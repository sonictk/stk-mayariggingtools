#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module userSetup.py: This module is run on Maya startup when the plugin is loaded.
"""
import logging
import maya.utils as utils
import rigTools.setup as rig_tools_setup
from rigTools.lib.exceptions import register_custom_exceptions
from rigTools.lib.plugin import load_rig_tools_plugins


class RigToolsInitialization(object):
    """
    This class initializes the Rigging Tools.
    """

    def __init__(self):
        """The constructor."""
        # TODO: Need to change to use custom logging

        # Register custom exceptions before logging is set up so that if problems
        # occur in logging it is immediately recorded in the traceback
        register_custom_exceptions()

        self.logger = logging.getLogger(__name__)
        self.logger.info('Launching Rigging Tools (sonictk)...')

        self.rigToolsLoader()

    def rigToolsLoader(self):
        """This method acts as the entry point for the Rigging Tools plugin."""
        rig_tools_setup.CreateRigToolsMenu()
        load_rig_tools_plugins()


#: Run at lowest priority in order to allow Maya to build
#: the main window (MayaWindow) first along with the menubar (gMainFileMenu)
utils.executeDeferred('RigToolsInitialization()')
