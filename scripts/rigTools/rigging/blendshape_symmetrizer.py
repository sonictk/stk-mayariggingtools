#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module blendshape_symmetrizer: This module ontains methods to symmetrizing
blendshapes in Maya.
"""

import logging
import rigTools.rigging.ui.blendshape_symmetrizer as blendshape_symmetrizer
from rigTools.lib.ui import MayaDockWidget
from rigTools.lib.ui import MayaShelfTool


class BlendshapeSymmetrizer(MayaDockWidget, MayaShelfTool):
    """
    This class contains methods for creating control shapes for rigs.
    """

    # Define shelf information
    shelf_tool_icon_name = 'blendshape_symmetrizer'
    shelf_tool_title = 'Blendshape Symmetrizer'


    def __init__(
            self,
            window_name='blendshape_symmetrizer',
            title='Symmetrize Blendshapes',
            sizeable=False
    ):

        self.logger = logging.getLogger(__name__)

        super(BlendshapeSymmetrizer, self).__init__(
            ui_class=blendshape_symmetrizer.Ui_Form_blendshape_sym,
            window_name=window_name,
            title=title,
            sizeable=sizeable
        )


    def populateData(self, *args, **kwargs ):
        """
        This method populates the widget with all necessary data.
        """

        pass


    def makeConnections(self, *args, **kwargs):
        """
        This method handles connecting UI widgets to their actions.
        """

        self.ui.btn_close.clicked.connect(self.cancelAction)