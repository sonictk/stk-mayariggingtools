#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module change_node_color: This module contains methods for changing the colour
of a Maya node.
"""
import logging
import pymel.core as pm
from PySide.QtGui import QWidget, QColor, QColorDialog
from rigTools.lib.undo import UndoManager
from rigTools.lib.ui import MayaWidget, MayaShelfTool
from rigTools.rigging.ui import change_node_color_ui as color_ui
from rigTools.lib.node import NodeColor


class Swatch(QWidget):
    """
    This creates a colour swatch widget that can be clicked on to set the
    colour of an object in Maya.
    """

    def __init__(self, width=20, height=20, color=(0,0,0)):
        """
        The constructor.

        :param width: ``int`` width of the swatch
        :param height: ``int`` height of the swatch
        :param color: ``list`` of RGB values for the background colour of the
            swatch.
        :return: ``QWidget``
        """

        self.logger = logging.getLogger(__name__)

        super(Swatch, self).__init__()

        self.setAutoFillBackground(True)
        self.setFixedSize(width, height)

        self.color = color

        palette = self.palette()
        palette.setColor(
            self.backgroundRole(),  QColor(*self.color)
        )

        self.setPalette(palette)


    def mousePressEvent(self, event):
        """
        This sets the current selected nodes' colours when the swatch is clicked
        to the current swatch's color.
        """

        self.logger.debug('Setting colours of objects to: {0}'.format(self.color))

        # Convert colour RGB to float values
        color = [c / 255 for c in self.color]

        NodeColor().set_nodes_color(color=color)


class ChangeColorDialog(MayaWidget, MayaShelfTool):
    """
    This class will bring up a colour dialog that allows the user to
    interactively change the colour of Maya node(s) in the scene.
    """

    # Define shelf information
    shelf_tool_title = 'Change Color'
    shelf_tool_icon_name = 'change_color'

    def __init__(
        self,
        window_name='changeColorDialog',
        title='Select Colour',
        nodes=None
    ):
        """
        The constructor.

        :param window_name: ``str`` that will the object name of the new window.
        :param title: ``str`` that will act as the title of the new window created.
        :param nodes: ``list`` of ``str`` that are the names of Maya objects.
        :return: ``None``
        """
        super(ChangeColorDialog, self).__init__(
            ui_class=color_ui.Ui_Form, window_name=window_name, title=title
        )

    def setup_ui_widgets(self, ui_class=None):
        """
        This method creates all the necessary UI items.

        :param ui_class: ``object`` that is a class generated from a Qt
            Designer .ui compiled .py file. Should contain the setupUi()
            method.

        :return: ``Ui_Form`` class generated by Qt Designer.
        """

        # Get reference to the widgets from the Qt Designer form class
        ui = super(ChangeColorDialog, self).setup_ui_widgets(ui_class=ui_class)

        ui.layout_color_buttons.setSpacing(0)

        # Create additional swatches for each Maya default colour index
        for index, color in NodeColor().color_mappings.items():

            # Get the corresponding RGB value from the colour index
            rgb_color = [round(c * 255) for c in pm.colorIndex(index, q=True)]

            swatch_widget = Swatch(color=rgb_color)

            ui.layout_color_buttons.addWidget(swatch_widget)

        return ui

    def makeConnections(self):
        """This method conencts the UI widgets to callbacks."""
        self.ui.tb_chooseCustomColor.clicked.connect(self.get_custom_color)
        self.ui.btn_close.clicked.connect(self.cancelAction)

    @UndoManager.undoable
    def get_custom_color(self):
        """
        This method opens a colour picker for the user to choose a specific
        colour value for.

        :return: ``list`` containing RGB colour values to set.
        """
        selected_color = QColorDialog.getColor(parent=self)

        # Convert colour back to float values
        if selected_color:
            selected_color = [
                selected_color.red() / 255,
                selected_color.green() / 255,
                selected_color.blue() /255
            ]

        NodeColor().set_nodes_color(color=selected_color)

