#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module tension_deformer: This module contains objects for setting up and working
with a tension deformer setup on meshes in Maya.
"""
import logging
import pymel.core as pm
from rigTools.lib.ui import MayaShelfTool
from rigTools.lib.ui import display_message

class TensionSetup(MayaShelfTool):
    """
    This class is a tool that aids in setting up a tension deformer on meshes
    in Maya.
    """

    shelf_tool_icon_name = 'out_calculateTension'
    shelf_tool_title = 'Setup Tension'

    def __init__(self):
        """The constructor."""
        self.logger = logging.getLogger(__name__)
        self.required_plugin_name = 'tensionDeformer'

        self.applyTension()

    def applyTension(self):
        """
        This method runs the necessary command to setup the tension on a given
        set of meshes.
        """
        if pm.pluginInfo(self.required_plugin_name, q=True, loaded=True) is True:
            try: pm.applyTension()
            except RuntimeError as err:
                display_message('Failed to setup tension!\n{0}'.format(err),
                                message_type='error',
                                show_prompt=True,
                                log=True)
        else:
            raise RuntimeError('The required plug-in: {0} is not available!'
                              .format(self.required_plugin_name))