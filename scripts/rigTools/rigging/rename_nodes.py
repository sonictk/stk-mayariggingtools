#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module rename_nodes: This module contains methods for helping to batch rename
nodes in Maya.
"""
import logging
import os
import pymel.core as pm
import re
import traceback
from PySide.QtCore import Qt
from rigTools.lib.callbacks import SelectionChangedEvent
from rigTools.lib.jsonutils import JSONUtils
from rigTools.lib.node import rename_nodes
from rigTools.lib.selections import get_current_selection
from rigTools.lib.undo import UndoManager
from rigTools.lib.ui import MayaDockWidget, MayaShelfTool
from rigTools.rigging.ui.rename_nodes_ui import Ui_Form


class RenameNodes(MayaDockWidget, MayaShelfTool):
    """
    This class creates a UI that allows users to batch rename node(s) in Maya.
    """

    # Define shelf information
    shelf_tool_title = 'Rename object(s)'
    shelf_tool_icon_name = 'renamer'

    def __init__(
            self,
            window_name='renameNodesDialog',
            title='Renamer',
            window_type=Qt.Dialog
    ):
        """
        The constructor.

        :return:
        """

        self.logger = logging.getLogger(__name__)

        # Get path to renamer conventions JSON file
        prefs_path = os.path.join(
            os.path.dirname(
                os.path.dirname(
                    os.path.abspath(__file__)
                )
            ),
            'prefs', 'renamer_conventions.json'
        )

        if not os.path.isfile(prefs_path):
            self.logger.error('### Could not find preferences file: {0} for '
                              'Renamer!!!'.format(prefs_path))
            return

        prefs_data = JSONUtils.read(prefs_path)

        self.prefixes = prefs_data['prefixes']
        self.suffixes = prefs_data['suffixes']
        self.separators = prefs_data['separators']
        self.numbering_types = prefs_data['numbering']

        # Register a callback and monkey patch it to get the current
        # selection whenever it changes, then refresh the UI with the currently
        # selected object's name
        self.selection_changed_callback = SelectionChangedEvent
        self.selection_changed_callback.emit = self.refresh_ui

        self.callback_fn = SelectionChangedEvent()

        super(RenameNodes, self).__init__(
            ui_class=Ui_Form,
            window_name=window_name,
            title=title,
            window_type=window_type
        )


    def populateData(self, *args, **kwargs):
        """
        This method populates all necessary data to the Renamer UI.

        :param args:
        :param kwargs:
        :return:
        """

        # Place currently selected object name as default name
        current_selection = pm.ls(sl=True)

        if current_selection:
            suggest_name = True
            default_name = current_selection[0].name()

        else:
            suggest_name = False
            default_name = 'Enter new name for object...'

        if suggest_name:
            self.ui.txt_affix.setText(default_name)
        else:
            self.ui.txt_affix.setPlaceholderText(default_name)

        self.ui.txt_find.setPlaceholderText('Enter expression to search for...')
        self.ui.txt_replace.setPlaceholderText('Enter text to replace with...')
        self.ui.txt_numbering.setPlaceholderText('Enter numbering expression...')

        # Fill up prefix/suffix comboboxes from prefs file
        for prefix, object_type in sorted(self.prefixes.items()):
            self.ui.cmbBox_rename_prefix.addItem(prefix, object_type)

        for suffix, long_name in sorted(self.suffixes.items()):
            self.ui.cmbBox_rename_suffix.addItem(suffix, long_name)

        for separator, long_name in sorted(self.separators.items()):
            self.ui.cmbBox_separator.addItem(long_name, separator)

        for long_name, numbering_type in sorted(self.numbering_types.items()):
            self.ui.cmbBox_numbering_affix_type.addItem(long_name, numbering_type)

        # Set default state of UI items
        self.ui.chk_add_prefix.setChecked(True)
        self.ui.chk_add_suffix.setChecked(False)
        self.ui.cmbBox_rename_suffix.setEnabled(False)


    def makeConnections(self, *args, **kwargs):
        """
        This method connects UI widgets to callbacks.

        :param args:
        :param kwargs:
        :return:
        """

        # Connect UI toggle elements
        self.ui.chk_add_prefix.stateChanged.connect(
            lambda: self.refresh_ui(event=None, refresh_object_name=False)
        )
        self.ui.chk_add_suffix.stateChanged.connect(
            lambda: self.refresh_ui(event=None, refresh_object_name=False)
        )

        # Connect UI buttons
        self.ui.btn_rename.clicked.connect(self.rename)

        self.ui.btn_replace_text.clicked.connect(self.search_and_replace)

        self.ui.btn_number_items.clicked.connect(self.renumber)

        self.ui.btn_close.clicked.connect(self.cancelAction)


    def closeEvent(self, event):
        """
        Un-registers the custom callback when this widget is closed.

        :param event:
        :return:
        """
        if hasattr(self, 'callback_fn'):
            self.callback_fn.remove_callback()
            self.callback_fn.emit=None
            del self.callback_fn

        return super(RenameNodes, self).closeEvent(event)


    def refresh_ui(self, event, refresh_object_name=True):
        """
        This method is run whenever the current selection changes. It is
        responsible for refreshing the current appearance of the UI.

        :param event:

        :param refresh_object_name: ``bool`` indicating if the object name
            should be refreshed whenever the UI is updated.

        :return:
        """

        if refresh_object_name:
            # Update current object text field
            current_selection = pm.ls(sl=True)
            self.logger.debug('Selected: {0}'.format(current_selection))

            if current_selection:
                self.ui.txt_affix.setText(current_selection[0].name())

        # Update appearance of Prefix/Suffix comboboxes
        self.ui.cmbBox_rename_prefix.setEnabled(self.ui.chk_add_prefix.isChecked())
        self.ui.cmbBox_rename_suffix.setEnabled(self.ui.chk_add_suffix.isChecked())


    @UndoManager.repeatable
    @UndoManager.undoable
    def rename(self,
               nodes=None,
               base_name='',
               prefix=None,
               suffix=None,
               separator=None
               ):
        """
        This method applies the renaming operation.

        :param base_name: ``str`` that will act as the new base name for the
            renamed objects.

        :param prefix: ``str`` prefix to append to object name.

        :param suffix: ``str`` suffix to suffix to object name.

        :param separator: ``str`` used to separate elements.

        :param nodes: ``list`` of ``nt.Transform`` objects to be renamed.

        :return: ``None``
        """

        if not nodes:
            nodes = get_current_selection()

        if self.ui.chk_add_prefix.isChecked() and not prefix:
                prefix = self.ui.cmbBox_rename_prefix.currentText()

        if self.ui.chk_add_suffix.isChecked() and not suffix:
            suffix = self.ui.cmbBox_rename_suffix.currentText()

        if not separator:
            separator = self.ui.cmbBox_separator.itemData(
                self.ui.cmbBox_separator.currentIndex()
            )

        if not base_name:
            base_name = self.ui.txt_affix.text()

        rename_nodes(
            nodes=nodes,
            base_name=base_name,
            prefix=prefix,
            suffix=suffix,
            separator=separator,
            renumber=False
        )


    @UndoManager.repeatable
    @UndoManager.undoable
    def search_and_replace(self, nodes=None, search_string='',
                           replace_string='', regex=False):
        """
        This method searches for and replaces instances of the search string
        with the replace expression in the given nodes' names.

        :param regex: ``bool`` indicating if search string is to be treated as a
            regex expression.

        :param nodes: ``list`` of ``nt.Transform`` objects to be renamed.

        :param search_string: ``str`` expression to search for in nodes' names.

        :param replace_string: ``str`` expression to replace in nodes' names.

        :return: ``None``
        """

        if not nodes:
            nodes = get_current_selection()

        if not search_string:
            search_string = self.ui.txt_find.text()

            if not search_string:
                pm.warning('You need to enter a string to search for!')
                return

        if not replace_string:
            replace_string = self.ui.txt_replace.text()

        regex = self.ui.chk_is_regex.isChecked()

        for node in nodes:

            if regex:
                try:
                    new_node_name = re.sub(
                        search_string, replace_string, node.name()
                    )
                except Exception:
                    self.logger.error('### Failed to execute regex expression!!!\n{0}'
                                      .format(traceback.print_exc))
                    pm.error('Regular Expression was invalid!')
                    return

            else:
                new_node_name = node.name().replace(search_string, replace_string)

            pm.rename(node, new_node_name)

        self.logger.info('Successfully renamed objects!')


    @UndoManager.repeatable
    @UndoManager.undoable
    def renumber(
            self,
            nodes=None,
            expression='',
            sort_nodes=None,
            reverse_sort=None,
            name_identifier='{name}',
            padding_identifier='#',
            start_from=None
    ):
        """
        This method re-numbers the nodes.

        :param name_identifier: ``str`` that is a special expression indicating
            what the object's original name will be replaced with when this
            identifier appears in the expression. Defaults to ``{name}``.

        :param padding_identifier: ``str`` that is a special expression
            indicating what the number will be replaced with when this
            identifier appears in the expression. Defaults to ``#``.

        :param start_from: ``int`` that determines where to start the
            numbering operation from.

        :param sort_nodes: ``bool`` determining if nodes are sorted according
            to their name. Defaults to taking the value from the UI.

        :param reverse_sort: ``bool`` determining if nodes are reverse sorted.
            Defaults to taking the value from the UI.

        :param nodes: ``list`` of ``nt.Transform`` objects to be re-numbered.

        :param expression: ``str`` expression determining how the objects
            should be re-numbered.

        :return: ``None``
        """

        if not nodes:
            nodes = get_current_selection()

        # Get current user input for starting number
        if not start_from:
            start_from = int(self.ui.txt_number_start.text())

        # Form default re-numbering expression
        if not expression:
            expression = self.ui.txt_numbering.text()
            if not expression:
                pm.warning('You need to enter an expression to re-number the objects!')
                return

        if not reverse_sort:
            reverse_sort = self.ui.chk_numbering_reverse_sort.isChecked()

        if not sort_nodes:
            sort_nodes = self.ui.chk_numbering_sort_names.isChecked()

            if sort_nodes:
                nodes = sorted(nodes, reverse=reverse_sort)

        affix_type = self.ui.cmbBox_numbering_affix_type.itemData(
            self.ui.cmbBox_numbering_affix_type.currentIndex()
        )

        for node_idx, node in enumerate(nodes):

            node_name = node.name()

            # Add any setting for starting re-numbering from a non-zero value
            node_idx += start_from

            # Check if the expression had the name identifier in it
            if expression.find(name_identifier) == -1:
                # If cannot find identifier, format default base name
                # e.g. 'nameOfNode_###'
                if affix_type == 'prefix':
                    new_name = '_{0}_{1}'.format(expression, node_name)
                elif affix_type == 'suffix':
                    new_name = '{0}_{1}'.format(node_name, expression)
            else:
                new_name = expression.replace(name_identifier, node_name)

            regex_pattern = re.compile(r'({0}+)+'.format(padding_identifier))

            # Get list of found padding identifiers
            found_matches = re.findall(regex_pattern, new_name)

            if not found_matches:
                if affix_type == 'prefix':
                    new_name = '{0}{1}'.format(node_idx, node_name)
                elif affix_type == 'suffix':
                    new_name = '{0}{1}'.format(node_name, node_idx)

            else:
                # Substitute the base name padding identifiers with the node count
                for match in found_matches:
                    pad_length = len(match)

                    new_name = re.sub(
                        regex_pattern,
                        str(node_idx).zfill(pad_length),
                        new_name,
                        count=1
                    )

            pm.rename(node, new_name)

        self.logger.info('Successfully performed re-numbering operation!')