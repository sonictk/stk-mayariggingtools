# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'F:/Programs/stk-mayariggingtools/scripts/rigTools/rigging/ui/change_node_color_ui.ui'
#
# Created: Tue Sep 29 16:10:23 2015
#      by: pyside-uic 0.2.14 running on PySide 1.2.0
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(298, 149)
        self.mainLayout = QtGui.QVBoxLayout(Form)
        self.mainLayout.setObjectName("mainLayout")
        self.grp_selectColor = QtGui.QGroupBox(Form)
        self.grp_selectColor.setObjectName("grp_selectColor")
        self.layout_color_buttons = QtGui.QHBoxLayout(self.grp_selectColor)
        self.layout_color_buttons.setObjectName("layout_color_buttons")
        self.tb_chooseCustomColor = QtGui.QToolButton(self.grp_selectColor)
        self.tb_chooseCustomColor.setObjectName("tb_chooseCustomColor")
        self.layout_color_buttons.addWidget(self.tb_chooseCustomColor)
        self.mainLayout.addWidget(self.grp_selectColor)
        self.btn_close = QtGui.QPushButton(Form)
        self.btn_close.setObjectName("btn_close")
        self.mainLayout.addWidget(self.btn_close)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.grp_selectColor.setTitle(QtGui.QApplication.translate("Form", "Select Colour", None, QtGui.QApplication.UnicodeUTF8))
        self.tb_chooseCustomColor.setText(QtGui.QApplication.translate("Form", "...", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_close.setText(QtGui.QApplication.translate("Form", "Close", None, QtGui.QApplication.UnicodeUTF8))

