#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module control_shape_creator: This module allows for creating custom control
shapes to be used as rig controls.
"""
import inspect
import logging
import pymel.core as pm
import maya.api.OpenMaya as om2
from PySide.QtCore import Slot
from PySide.QtGui import QColor
from rigTools.lib.node import NodeColor
from rigTools.lib.undo import UndoManager
from rigTools.lib.ui import MayaDockWidget, MayaShelfTool
from rigTools.rigging.lib.shapes import Shapes
from rigTools.rigging.ui import control_shape_creator_ui
from rigTools.lib.math import MathConstants


def create_controls(
        nodes=None,
        shape='null',
        color=(0,0,0),
        width=1,
        height=1,
        depth=1,
        parent=None,
        base_name='',
        orientation=None,
        chain=False
):
    """
    This method creates control objects.
    Returns a ``list`` of control objects that were created.

    :param orientation: ``tuple`` or ``list`` of 3 floats indicating a
        rotation offset for the shape.

    :param shape: ``str`` indicating the type of shape to create.

    :param width: ``float`` indicating the width of the shape.

    :param height: ``float`` indicating the height of the shape.

    :param depth: ``float`` indicating the depth of the shape.

    :param color: ``list`` of normalized 0-1 RGB colours to set.

    :param parent: ``nt.Transform`` object to parent all created objects to.

    :param base_name: ``str`` that will be prefixed to all controls that are
        created.

    :param chain: ``bool`` indicating if the objects created should be parented
        to each other in a hierarchical fashion.

    :param nodes: ``list`` of ``nt.Transform`` objects to create control objects
        for. The controls will be created at their positions. If ``None`` is
        specified, a single control will be created at the world origin.

    :return: ``list`` of ``MFnNurbsCurve`` objects that were created.
    """

    logger = logging.getLogger(__name__)

    result = []
    temporary_origin_transform = None

    base_name =  'ctrl_' + base_name

    if not nodes:
        nodes = pm.ls(orderedSelection=True, typ='transform')

        # If nothing is selected, create the controls at the origin.
        if not nodes:
            temporary_origin_transform = pm.nt.Transform()
            nodes = [temporary_origin_transform]

    # Convert PyMEL object to OpenMaya MObject
    if parent:
        parent = om2.MGlobal.getSelectionListByName(parent.name()).getDependNode(0)

    # Account for specified rotational offset
    if orientation:
        orientation = om2.MVector(*orientation)

    for node in nodes:
        # If a parent is specified, use identity transform for the ctrl,
        # otherwise use the parent's transform
        if parent:
            node_transformation = om2.MTransformationMatrix()
        else:
            node_transformation = om2.MTransformationMatrix(
                om2.MMatrix(node.getMatrix(worldSpace=True))
            )

        if shape == 'cube':
            ctrl = Shapes.cube(
                parent=parent,
                name=base_name,
                width=width,
                height=height,
                depth=depth,
                color=color,
                transform_matrix=node_transformation,
                rot_offset=orientation
            )
            result.append(ctrl)

        elif shape == 'pyramid':
            ctrl = Shapes.pyramid(
                parent=parent,
                name=base_name,
                width=width,
                height=height,
                depth=depth,
                color=color,
                transform_matrix=node_transformation,
                rot_offset=orientation
            )
            result.append(ctrl)

        elif shape == 'square':
            ctrl = Shapes.square(
                parent=parent,
                name=base_name,
                width=width,
                depth=depth,
                color=color,
                transform_matrix=node_transformation,
                rot_offset=orientation
            )
            result.append(ctrl)

        elif shape == 'star':
            ctrl = Shapes.star(
                parent=parent,
                name=base_name,
                width=width,
                color=color,
                transform_matrix=node_transformation,
                rot_offset=orientation
            )
            result.append(ctrl)

        elif shape == 'circle':
            ctrl = Shapes.circle(
                parent=parent,
                name=base_name,
                width=width,
                color=color,
                transform_matrix=node_transformation,
                rot_offset=orientation
            )
            result.append(ctrl)

        elif shape == 'clover':
            ctrl = Shapes.clover(
                parent=parent,
                name=base_name,
                width=width,
                color=color,
                transform_matrix=node_transformation,
                rot_offset=orientation
            )
            result.append(ctrl)

        elif shape == 'cylinder':
            ctrl = Shapes.cylinder(
                parent=parent,
                name=base_name,
                width=width,
                height=height,
                color=color,
                transform_matrix=node_transformation,
                rot_offset=orientation
            )
            result.append(ctrl)

        elif shape == 'diamond':
            ctrl = Shapes.diamond(
                parent=parent,
                name=base_name,
                width=width,
                color=color,
                transform_matrix=node_transformation,
                rot_offset=orientation
            )
            result.append(ctrl)

        elif shape == 'peaked_cube':
            ctrl = Shapes.peaked_cube(
                parent=parent,
                name=base_name,
                width=width,
                color=color,
                transform_matrix=node_transformation,
                rot_offset=orientation
            )
            result.append(ctrl)

        elif shape == 'arrow':
            ctrl = Shapes.arrow(
                parent=parent,
                name=base_name,
                width=width,
                color=color,
                transform_matrix=node_transformation,
                rot_offset=orientation
            )
            result.append(ctrl)

        elif shape == 'cross_arrow':
            ctrl = Shapes.cross_arrow(
                parent=parent,
                name=base_name,
                width=width,
                color=color,
                transform_matrix=node_transformation,
                rot_offset=orientation
            )
            result.append(ctrl)

        elif shape == 'cross':
            ctrl = Shapes.cross(
                parent=parent,
                name=base_name,
                width=width,
                color=color,
                transform_matrix=node_transformation,
                rot_offset=orientation
            )
            result.append(ctrl)

        else:
            logger.error('### Unknown type of shape specified!!!'.format(shape))
            break

    if chain:
        # Parent each object in sequence to each other
        fn_dag_modifier = om2.MDagModifier()

        def parent_objects(node_list):
            """
            This function parents the ``MObject`` items in the given ``list``
            recursively to each other.
            """
            if len(node_list) > 1:

                fn_dag_modifier.reparentNode(node_list[0], node_list[1])

                parent_objects(node_list[1:])

        parent_objects(result)

        # Commit the changes
        fn_dag_modifier.doIt()

    if temporary_origin_transform:
        pm.delete(temporary_origin_transform)

    return result


class ControlShapeCreator(MayaDockWidget, MayaShelfTool):
    """
    This class contains methods for creating control shapes for rigs.
    """

    # Define shelf information
    shelf_tool_icon_name = 'control_creator'
    shelf_tool_title = 'Create Control Shapes'

    def __init__(
            self,
            window_name='control_shape_creator',
            title='Create Control Shapes',
            sizeable=False
    ):

        self.logger = logging.getLogger(__name__)

        super(ControlShapeCreator, self).__init__(
            ui_class=control_shape_creator_ui.Ui_Form,
            window_name=window_name,
            title=title,
            sizeable=sizeable
        )


    def populateData(self, *args, **kwargs ):
        """
        This method populates the widget with all necessary data.
        """

        self.ui.txtFld_controls_baseName.setPlaceholderText(
            'Enter the name for the control(s)...'
        )

        color_mappings = self.register_colors()

        for idx, color_name in color_mappings.items():
            self.ui.cmbBox_control_colour.addItem(color_name, idx)

        shapes_members_list = self.register_control_shape_types()

        for member in shapes_members_list:
            self.ui.cmbBox_control_shape.addItem(member[0], member[1])

        # Set orientation checkbox initial state
        self.ui.rb_orient_x_pos.setChecked(True)


    def makeConnections(self, *args, **kwargs):
        """
        This method handles connecting UI widgets to their actions.
        """

        self.ui.btn_control_createControls.clicked.connect(
            self.create_controls_action
        )
        self.ui.cmbBox_control_colour.currentIndexChanged.connect(
            self.update_color_swatch
        )
        self.ui.btn_control_selParent.clicked.connect(
            self.set_parent_from_selected
        )
        self.ui.chkBox_control_parent.stateChanged.connect(
            self.toggle_parent_ui_state
        )
        self.ui.swatch_color.colorSelected.connect(
            self.color_selected
        )
        self.ui.btn_close.clicked.connect(self.cancelAction)


    @Slot()
    def color_selected(self, selected_color):
        """
        This method updates the ui when a custom colour has been selected.

        :param selected_color: ``QColor`` that is the custom colour selected.
        """

        self.ui.cmbBox_control_colour.setCurrentIndex(
            self.ui.cmbBox_control_colour.findText('Custom')
        )


    def toggle_parent_ui_state(self):
        """
        This callback will update the state of the UI widgets for selecting
        the parent item to parent the controls to.

        :return: ``None``
        """
        if self.ui.chkBox_control_parent.isChecked():
            self.ui.txtFld_control_parent.setEnabled(True)
            self.ui.btn_control_selParent.setEnabled(True)
        else:
            self.ui.txtFld_control_parent.setEnabled(False)
            self.ui.btn_control_selParent.setEnabled(False)


    def register_control_shape_types(self):
        """
        This method registers all available types of control shapes.
        """
        # Get list of tuples of (name, class) shape methods in order
        # to propagate the UI combobox
        return inspect.getmembers(Shapes, inspect.isfunction)


    def register_colors(self):
        """
        This method registers all available types of basic colours.
        """
        mappings = NodeColor().color_mappings
        mappings[0] = 'Custom'

        return mappings


    def get_selected_preset_color(self):
        """
        This method returns the currently selected colour in the combobox UI
        widget dropdown.

        :return: ``list`` of ``int`` RGB values.
        """
        current_colour_index = self.ui.cmbBox_control_colour.currentIndex()

        # If 'Custom' is selected, do nothing
        if not current_colour_index == 0:
            color_index = self.ui.cmbBox_control_colour.itemData(
                self.ui.cmbBox_control_colour.currentIndex()
            )
            rgb_color = [round(c * 255) for c in pm.colorIndex(color_index, q=True)]

            return rgb_color


    def update_color_swatch(self):
        """
        This callback updates the UI colour swatch with the currently
        selected colour.
        """
        rgb_color = self.get_selected_preset_color()

        if rgb_color:
            self.ui.swatch_color.current_color = QColor(*rgb_color)


    def set_parent_from_selected(self):
        """
        This method sets the currently selected object as the parent item in
        the UI.

        :return: ``str`` name of the parent set.
        """
        current_selection = pm.ls(sl=True)

        if not len(current_selection) == 1:
            pm.warning('Please select a single object in the scene!')
            return

        else:
            parent_name = current_selection[0].name()
            self.ui.txtFld_control_parent.setText(parent_name)
            return parent_name


    @UndoManager.undoable
    @UndoManager.repeatable
    def create_controls_action(self):
        """
        This method creates the controls for the selected objects.
        """
        _math_constants = MathConstants()
        _half_pi = _math_constants.half_pi
        _pi = _math_constants.pi

        shape = self.ui.cmbBox_control_shape.currentText()
        size = self.ui.spinBox_control_size.value()

        rgb_color = [
            float(self.ui.swatch_color.current_color.red()) / 255,
            float(self.ui.swatch_color.current_color.green()) / 255,
            float(self.ui.swatch_color.current_color.blue()) / 255
        ]

        if self.ui.chkBox_control_parent.isChecked():
            parent_name = self.ui.txtFld_control_parent.text()
            parent = pm.ls(parent_name)[0]
        else:
            parent = None

        if self.ui.rb_orient_x_pos.isChecked():
            rot_offset = (0, 0, -_half_pi)
        elif self.ui.rb_orient_x_neg.isChecked():
            rot_offset = (0, 0, _half_pi)
        elif self.ui.rb_orient_y_pos.isChecked():
            rot_offset = (0, 0, 0)
        elif self.ui.rb_orient_y_neg.isChecked():
            rot_offset = (0, 0, _pi)
        elif self.ui.rb_orient_z_pos.isChecked():
            rot_offset = (_half_pi, 0, 0)
        elif self.ui.rb_orient_z_neg.isChecked():
            rot_offset = (-_half_pi, 0, 0)
        else:
            rot_offset = None

        base_name = self.ui.txtFld_controls_baseName.text()

        self.logger.debug(
            'Shape for controls selected: {0}\n'
            'Color for controls selected: {1}\n'
            'Creating with orientation: {2}\n'
            'Creating with size: {3}\n'
            'Creating with parent: {4}\n'
                .format(shape, rgb_color, rot_offset, size, parent)
        )

        nodes = pm.ls(orderedSelection=True, typ='transform')

        controls = create_controls(
            nodes=nodes,
            shape=shape,
            color=rgb_color,
            width=size, height=size, depth=size,
            parent=parent,
            base_name=base_name,
            chain=self.ui.chkBox_controls_chainFlag.isChecked(),
            orientation=rot_offset
        )

        return controls
