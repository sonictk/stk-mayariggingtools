#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module group_freeze: This module contains methods for creating an intermediate
transform for the selected object(s).
"""
import logging
import pymel.core as pm
from rigTools.lib.undo import UndoManager
from rigTools.lib.ui import MayaShelfTool


class GroupFreeze(MayaShelfTool):
    """
    This class contains methods for creating intermediate transformations for
    objects in Maya.
    """

    shelf_tool_icon_name = 'group_freeze'
    shelf_tool_title = 'Group Freeze'


    def __init__(self):
        """The constructor."""
        self.groupFreeze()

    @staticmethod
    @UndoManager.undoable
    def groupFreeze(name=''):
        """
        Creates empty intermediate group transform to hold the selected object(s)
        existing transformations.

        :param name: ``str`` of object to have intermediate group made for.
        :rtype: ``list`` containing new groups made.
        """

        logger = logging.getLogger(__name__)

        zeros = []

        if not name:
            selection = pm.ls(selection=True)
        else:
            selection = name

        # Check for empty selection
        if not selection:
            pm.warning('# Please select at least one object!')
            return

        for sel in selection:

            # Capitalize first letter of node name
            new_name = 'grp_frz_' + sel.name()[0].upper() + sel.name()[1:]
            counter = 1

            # Increment-name duplicate groups
            while (pm.objExists(new_name)) == True:
                new_name = new_name[0:7] + str(counter) + new_name[7:]
                counter += 1

            # Create empty group
            grp = pm.group(empty=True, w=True, name=new_name)

            # Find parent of node and parent new group to parent
            par = pm.listRelatives( sel, parent=True )
            if par:
                pm.parent(grp, par)

            # Create a duplicate of original here
            orig_duplicate = pm.duplicate(sel, returnRootsOnly=True, renameChildren=True)
            orig_duplicate = pm.rename(orig_duplicate, ('grp_frz_' + sel))

            # Snap the grp to the dupe
            translation_val= pm.getAttr( orig_duplicate + '.translate')
            rotation_val= pm.getAttr( orig_duplicate + '.rotate')
            rot_val_x = rotation_val[0]
            rot_val_y = rotation_val[1]
            rot_val_z = rotation_val[2]
            scale_val = pm.getAttr(orig_duplicate + '.scale')

            #check if transform is of type joint and then find orientation
            if pm.ls( sel, type='joint'):
                orient_val_x = pm.getAttr( sel + '.jointOrientX' )
                orient_val_y = pm.getAttr( sel + '.jointOrientY' )
                orient_val_z = pm.getAttr( sel + '.jointOrientZ' )
                rot_val_x += orient_val_x
                rot_val_y += orient_val_y
                rot_val_z += orient_val_z
                pm.setAttr(sel + '.rotateX', 0)
                pm.setAttr(sel + '.rotateY', 0)
                pm.setAttr(sel + '.rotateZ', 0)

            pm.setAttr(grp + '.scaleX',     scale_val[0])
            pm.setAttr(grp + '.scaleY',     scale_val[1])
            pm.setAttr(grp + '.scaleZ',     scale_val[2])
            pm.setAttr(grp + '.rotateX',    rot_val_x)
            pm.setAttr(grp + '.rotateY',    rot_val_y)
            pm.setAttr(grp + '.rotateZ',    rot_val_z)
            pm.setAttr(grp + '.translateX', translation_val[0])
            pm.setAttr(grp + '.translateY', translation_val[1])
            pm.setAttr(grp + '.translateZ', translation_val[2])

            pm.delete(orig_duplicate)

            pm.parent(sel , grp)
            zeros.append(grp)

        logger.info('Group Freeze successful!')

        return zeros