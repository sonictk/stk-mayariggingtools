#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module helpers: This module contains methods for dealing with helper objects
that act as aids in rigging.
"""
import logging

import maya.OpenMaya as om
from rigTools.lib.metadata import MetadataConstants


def remove_all_helpers_from_scene():
    """
    This method removes any helper DAG objects from the scene.

    :return: ``int`` that is number of objects deleted.
    """

    logger = logging.getLogger(__name__)

    _helper_metadata_name = MetadataConstants().helper_attribute_name

    # Traverse the entire DAG and find objects with the helper metadata attribute
    fn_iterator_dag = om.MItDag(om.MItDag.kDepthFirst, om.MFn.kTransform)
    fn_dag_node = om.MFnDagNode()

    fn_dag_modifier = om.MDagModifier()

    helper_objects_count = 0

    while not fn_iterator_dag.isDone():

        dag_node = fn_iterator_dag.currentItem()
        fn_dag_node.setObject(dag_node)

        if fn_dag_node.hasAttribute(_helper_metadata_name):
            plug = fn_dag_node.findPlug(_helper_metadata_name)

            if plug.asBool():
                helper_objects_count += 1

                # Delete the helper objects found
                fn_dag_modifier.deleteNode(dag_node)

        fn_iterator_dag.next()

    # Commit any delete changes to the DAG
    fn_dag_modifier.doIt()

    logger.info('Deleted helper objects: {0}'.format(helper_objects_count))

    return helper_objects_count



