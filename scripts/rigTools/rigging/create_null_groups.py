#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module create_null_groups: This module contains functions for creating null
groups from the selected items.
"""

import logging
import pymel.core as pm
from PySide.QtGui import QInputDialog, QLineEdit
from rigTools.lib.ui import get_maya_window

from rigTools.lib.undo import UndoManager

logger = logging.getLogger(__name__)


def get_user_input(parent):
    """
    This method displays a prompt that returns the user input.

    :return: ``str`` that is user input.
    """

    prompt = QInputDialog.getText(
        parent,
        'Enter prefix...',
        'Enter custom prefix for null groups to be created:',
        QLineEdit.Normal,
        'grp_null_'
    )[0]

    if not prompt:
        return

    return prompt


@UndoManager.undoable
def create_null_groups( objects=None, prefix='grp_null_' ):
    """
    Creates null group objects at the selected objects' locations.

    :param objects: ``list`` of ``str`` objects to create null groups for.
    :param prefix: ``str`` to append to the new name of the null group.
    :return: ``list`` of group objects created.
    """

    groups =[]

    if objects is None:
        objects = pm.ls(sl=True, type='transform')

    if len(objects) == 0:
        pm.warning('Please select at least one object!!!')

    else:
        for o in objects:
            transform = pm.datatypes.TransformationMatrix(
                o.getMatrix(worldSpace=True)
            )

            # Create null group and set its transformation to match
            group = pm.group( n=prefix+o.name(), empty=True )
            group.setTransformation(transform)

            groups.append(group)

        logger.info('Null groups created!')

    return groups


@UndoManager.undoable
def create_null_groups_with_prefix():
    """
    This method creates null group objects at the selected objects' locations.
    Accepts custom user input to set a prefix for the created null groups' names.

    :return: ``list`` of group objects created.
    """

    prefix = get_user_input(get_maya_window())

    if not prefix:
        return

    create_null_groups(prefix=prefix)