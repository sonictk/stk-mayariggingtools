#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module shapes: This module allows for creating various types of
curve shapes that can be used for character controls in Maya.
"""

import maya.api.OpenMaya as om2

import rigTools.lib.curves as curves


def get_point_array_with_offset(
        point_positions,
        position_offset=None,
        rotation_offset=None
):
    """
    This method adds position and rotation offsets to a ``list`` of ``MVector``
    objects. Returns the new list of ``MVector`` objects.

    :param point_positions: ``list`` of ``MVector`` objects containing the
        point positions.

    :param position_offset: ``MVector`` containing the position offset of
        the object from its center origin.

    :param rotation_offset: ``MVector`` containing the rotation offset of
        the object from its center origin.

    :return: ``MPointArray`` containing ``MPoint`` objects.
    """

    points = om2.MPointArray()

    for pos in point_positions:

        # Add rotation offset if specified
        if rotation_offset:

            added_rot_vector = om2.MVector(pos.x, pos.y, pos.z)\
                .rotateBy(
                om2.MEulerRotation(
                    rotation_offset.x,
                    rotation_offset.y,
                    rotation_offset.z,
                    om2.MEulerRotation.kXYZ
                )
            )

            pos = om2.MVector(
                added_rot_vector.x,
                added_rot_vector.y,
                added_rot_vector.z
            )

        # Add position offset if specified
        if position_offset:
            pos += position_offset

        pos = om2.MPoint(pos)
        points.append(pos)

    return points


class Shapes(object):
    """
    This class contains various methods for creating various types of shapes.
    """

    @staticmethod
    def cube(
        parent=None,
        name='cube',
        width=1,
        height=1,
        depth=1,
        color=(0,0,0),
        transform_matrix=None,
        pos_offset=None,
        rot_offset=None
    ):
        """
        This method creates a NURBS cube curve shape.

        :param parent: ``MObject`` Parent object to parent the curve as a
            transform to.

        :param name: ``str`` of the new object's name.

        :param width: ``float`` determining the width of the shape.

        :param height: ``float`` determining the height of the shape.

        :param depth: ``float`` determining the depth of the shape.

        :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

        :param transform_matrix: ``MTransformationMatrix`` specifying an initial
            transform for the created shape.

        :param pos_offset: ``MVector`` specifying a position offset for the
            created shape from its center origin.

        :param rot_offset: ``MVector`` specifying a rotation offset for the
            created shape from its center origin.

        :return: ``MFnNurbsCurve`` shape.
        """

        if not transform_matrix:
            transform_matrix = om2.MTransformationMatrix()

        len_x = width * 0.5
        len_y = height * 0.5
        len_z = depth * 0.5

        # Set coordinates for the points of the cube;
        # Note: p is positive, N is negative
        ppp = om2.MVector(len_x,     len_y,      len_z)
        ppN = om2.MVector(len_x,     len_y,      -len_z)
        pNp = om2.MVector(len_x,     -len_y,     len_z)
        Npp = om2.MVector(-len_x,    len_y,      len_z)
        pNN = om2.MVector(len_x,     -len_y,     -len_z)
        NNp = om2.MVector(-len_x,    -len_y,     len_z)
        NpN = om2.MVector(-len_x,    len_y,      -len_z)
        NNN = om2.MVector(-len_x,    -len_y,     -len_z)

        # Add any offsets required
        points = get_point_array_with_offset(
            [
                ppp, ppN, NpN, NNN,
                NNp, Npp, NpN, Npp,
                ppp, pNp, NNp, pNp,
                pNN, ppN, pNN, NNN
            ],
            pos_offset,
            rot_offset
        )

        # Create the curve object, set its attributes, and return it
        node = curves.add_curve_node(
            parent_object=parent,
            points=points,
            name=name,
            degree=1,
            matrix=transform_matrix,
            color=color
        )

        return node


    @staticmethod
    def pyramid(
            parent=None,
            name='pyramid',
            width=1,
            height=1,
            depth=1,
            color=(0, 0, 0),
            transform_matrix=None,
            pos_offset=None,
            rot_offset=None
    ):
        """
        This method creates a NURBS pyramid curve shape.

        :param parent: ``MObject`` Parent object to parent the curve as a
            transform to.

        :param name: ``str`` of the new object's name.

        :param width: ``float`` determining the width of the shape.

        :param height: ``float`` determining the height of the shape.

        :param depth: ``float`` determining the depth of the shape.

        :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

        :param transform_matrix: ``MTransformationMatrix`` specifying an initial
        transform for the created shape.

        :param pos_offset: ``MVector`` specifying a position offset for the
            created shape from its center origin.

        :param rot_offset: ``MVector`` specifying a rotation offset for the
            created shape from its center origin.

        :return: ``MFnNurbsCurve`` shape.
        """

        if not transform_matrix:
            transform_matrix = om2.MTransformationMatrix()

        len_x = width * 0.5
        len_y = height
        len_z = depth * 0.5

        # Set coordinates for the points of the pyramid;
        # Note: p is positive, N is negative
        top = om2.MVector(0,         len_y,      0)
        pp  = om2.MVector(len_x,     0,          len_z)
        pN  = om2.MVector(len_x,     0,          -len_z)
        Np  = om2.MVector(-len_x,    0,           len_z)
        NN  = om2.MVector(-len_x,    0,          -len_z)

        # Add any offsets required
        points = get_point_array_with_offset(
            [
                pp, top, pN, pp,
                Np, top, NN, Np,
                NN, pN
            ],
            pos_offset,
            rot_offset
        )

        # Create the curve object, set its attributes, and return it
        node = curves.add_curve_node(
            parent_object=parent,
            name=name,
            points=points,
            degree=1,
            matrix=transform_matrix,
            color=color
        )

        return node


    @staticmethod
    def square(
            parent=None,
            name='square',
            width=1,
            depth=1,
            color=(0, 0, 0),
            transform_matrix=None,
            pos_offset=None,
            rot_offset=None
    ):
        """
        This method creates a NURBS square curve shape.

        :param parent: ``MObject`` Parent object to parent the curve as a
            transform to.

        :param name: ``str`` of the new object's name.

        :param width: ``float`` determining the width of the shape.

        :param depth: ``float`` determining the depth of the shape.

        :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

        :param transform_matrix: ``MTransformationMatrix`` specifying an initial
        transform for the created shape.

        :param pos_offset: ``MVector`` specifying a position offset for the
            created shape from its center origin.

        :param rot_offset: ``MVector`` specifying a rotation offset for the
            created shape from its center origin.

        :return: ``MFnNurbsCurve`` shape.
        """

        if not transform_matrix:
            transform_matrix = om2.MTransformationMatrix()

        len_x = width * 0.5
        len_z = depth * 0.5

        # Set coordinates for the points of the square;
        # Note: p is positive, N is negative
        pp = om2.MVector(len_x,      0,      len_z)
        pN  = om2.MVector(len_x,     0,      -len_z)
        Np  = om2.MVector(-len_x,    0,      len_z)
        NN  = om2.MVector(-len_x,    0,      -len_z)

        # Add any offsets required
        points = get_point_array_with_offset(
            [
                pp, pN, NN, Np
            ],
            pos_offset,
            rot_offset
        )

        # Create the curve object, set its attributes, and return it
        node = curves.add_curve_node(
            parent_object=parent,
            name=name,
            points=points,
            closed=True,
            degree=1,
            matrix=transform_matrix,
            color=color
        )

        return node


    @staticmethod
    def star(
            parent=None,
            name='star',
            width=1,
            color=(0, 0, 0),
            transform_matrix=None,
            pos_offset=None,
            rot_offset=None
    ):
        """
        This method creates a NURBS star curve shape.

        :param parent: ``MObject`` Parent object to parent the curve as a
            transform to.

        :param name: ``str`` of the new object's name.

        :param width: ``float`` determining the width of the shape.

        :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

        :param transform_matrix: ``MTransformationMatrix`` specifying an initial
        transform for the created shape.

        :param pos_offset: ``MVector`` specifying a position offset for the
            created shape from its center origin.

        :param rot_offset: ``MVector`` specifying a rotation offset for the
            created shape from its center origin.

        :return: ``MFnNurbsCurve`` shape.
        """

        width *= 0.5

        if not transform_matrix:
            transform_matrix = om2.MTransformationMatrix()

        # Set coordinates for the points of the star
        p1 = om2.MVector(0,            -width,              0)
        p2 = om2.MVector(-width * 0.4,  width * 0.4,        0)
        p3 = om2.MVector(width,         0,                  0)
        p4 = om2.MVector(-width * 0.4, -width * 0.4,        0)
        p5 = om2.MVector(0,            width,               0)
        p6 = om2.MVector(width * 0.4,  -width * 0.4,        0)
        p7 = om2.MVector(-width,       0,                   0)
        p8 = om2.MVector(width * 0.4,  width * 0.4,         0)

        # Add any offsets required
        points = get_point_array_with_offset(
            [
                p1, p2, p3, p4, p5, p6, p7, p8
            ],
            pos_offset,
            rot_offset
        )

        # Create the curve object, set its attributes, and return it
        node = curves.add_curve_node(
            parent_object=parent,
            name=name,
            points=points,
            closed=True,
            degree=1,
            matrix=transform_matrix,
            color=color
        )

        return node


    @staticmethod
    def clover(
            parent=None,
            name='clover',
            width=1,
            color=(0, 0, 0),
            transform_matrix=None,
            pos_offset=None,
            rot_offset=None
    ):
        """
        This method creates a NURBS clover curve shape.

        :param parent: ``MObject`` Parent object to parent the curve as a
            transform to.

        :param name: ``str`` of the new object's name.

        :param width: ``float`` determining the width of the shape.

        :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

        :param transform_matrix: ``MTransformationMatrix`` specifying an initial
        transform for the created shape.

        :param pos_offset: ``MVector`` specifying a position offset for the
            created shape from its center origin.

        :param rot_offset: ``MVector`` specifying a rotation offset for the
            created shape from its center origin.

        :return: ``MFnNurbsCurve`` shape.
        """

        if not transform_matrix:
            transform_matrix = om2.MTransformationMatrix()

        # Set coordinates for the points of the star
        p1 = om2.MVector(0,            -width,              0)
        p2 = om2.MVector(-width * 0.4,  width * 0.4,        0)
        p3 = om2.MVector(width,         0,                  0)
        p4 = om2.MVector(-width * 0.4, -width * 0.4,        0)
        p5 = om2.MVector(0,            width,               0)
        p6 = om2.MVector(width * 0.4,  -width * 0.4,        0)
        p7 = om2.MVector(-width,       0,                   0)
        p8 = om2.MVector(width * 0.4,  width * 0.4,         0)

        # Add any offsets required
        points = get_point_array_with_offset(
            [
                p1, p2, p3, p4, p5, p6, p7, p8
            ],
            pos_offset,
            rot_offset
        )

        # Create the curve object, set its attributes, and return it
        node = curves.add_curve_node(
            parent_object=parent,
            name=name,
            points=points,
            closed=True,
            degree=3,
            matrix=transform_matrix,
            color=color
        )

        return node


    @staticmethod
    def circle(
            parent=None,
            name='circle',
            width=1,
            color=(0, 0, 0),
            transform_matrix=None,
            pos_offset=None,
            rot_offset=None
    ):
        """
        This method creates a NURBS circle curve shape.

        :param parent: ``MObject`` Parent object to parent the curve as a
            transform to.

        :param name: ``str`` of the new object's name.

        :param width: ``float`` determining the width of the shape.

        :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

        :param transform_matrix: ``MTransformationMatrix`` specifying an initial
        transform for the created shape.

        :param pos_offset: ``MVector`` specifying a position offset for the
            created shape from its center origin.

        :param rot_offset: ``MVector`` specifying a rotation offset for the
            created shape from its center origin.

        :return: ``MFnNurbsCurve`` shape.
        """

        if not transform_matrix:
            transform_matrix = om2.MTransformationMatrix()

        radius = width * 0.5

        # Set coordinates for the points of the shape
        p0 = om2.MVector(radius,             0,      0)
        p1 = om2.MVector(radius * 0.707,     0,      radius * -0.707)
        p2 = om2.MVector(0,                  0,      -radius)
        p3 = om2.MVector(radius * -0.707,    0,      radius * -0.707)
        p4 = om2.MVector(-radius,            0,      0)
        p5 = om2.MVector(radius * -0.707,    0,      radius * 0.707)
        p6 = om2.MVector(0,                  0,      radius)
        p7 = om2.MVector(radius * 0.707,     0,      radius * 0.707)

        # Add any offsets required
        points = get_point_array_with_offset(
            [
                p0, p1, p2, p3, p4, p5, p6, p7
            ],
            pos_offset,
            rot_offset
        )

        # Create the curve object, set its attributes, and return it
        node = curves.add_curve_node(
            parent_object=parent,
            name=name,
            points=points,
            closed=True,
            degree=3,
            matrix=transform_matrix,
            color=color
        )

        return node


    @staticmethod
    def cylinder(
            parent=None,
            name='circle',
            width=1,
            height=1,
            color=(0, 0, 0),
            transform_matrix=None,
            pos_offset=None,
            rot_offset=None
    ):
        """
        This method creates a NURBS cylinder curve shape.

        :param parent: ``MObject`` Parent object to parent the curve as a
            transform to.

        :param name: ``str`` of the new object's name.

        :param height: ``float`` determining the height of the shape.

        :param width: ``float`` determining the width of the shape.

        :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

        :param transform_matrix: ``MTransformationMatrix`` specifying an initial
        transform for the created shape.

        :param pos_offset: ``MVector`` specifying a position offset for the
            created shape from its center origin.

        :param rot_offset: ``MVector`` specifying a rotation offset for the
            created shape from its center origin.

        :return: ``MFnNurbsCurve`` shape.
        """

        if not transform_matrix:
            transform_matrix = om2.MTransformationMatrix()

        radius = width * 0.5
        height *= 0.5

        # Set coordinates for the points of the shape
        # Circle upper
        p0 = om2.MVector(radius,             height,      0)
        p1 = om2.MVector(radius * 0.707,     height,      radius * -0.707)
        p2 = om2.MVector(0,                  height,      -radius)
        p3 = om2.MVector(radius * -0.707,    height,      radius * -0.707)
        p4 = om2.MVector(-radius,            height,      0)
        p5 = om2.MVector(radius * -0.707,    height,      radius * 0.707)
        p6 = om2.MVector(0,                  height,      radius)
        p7 = om2.MVector(radius * 0.707,     height,      radius * 0.707)

        # Circle lower
        N0 = om2.MVector(radius,             -height,      0)
        N1 = om2.MVector(radius * 0.707,     -height,      radius * -0.707)
        N2 = om2.MVector(0,                  -height,      -radius)
        N3 = om2.MVector(radius * -0.707,    -height,      radius * -0.707)
        N4 = om2.MVector(-radius,            -height,      0)
        N5 = om2.MVector(radius * -0.707,    -height,      radius * 0.707)
        N6 = om2.MVector(0,                  -height,      radius)
        N7 = om2.MVector(radius * 0.707,     -height,      radius * 0.707)

        curve = get_point_array_with_offset(
            [
                p0, p1, p2, p3, p4, p5, p6, p7, p0,
                N0, N1, N2, p2, N2, N3, N4, p4, N4, N5, N6, p6, N6, N7, N0
            ],
            pos_offset,
            rot_offset
        )

        node = curves.add_curve_node(
            parent_object=parent,
            name=name,
            points=curve,
            closed=True,
            degree=1,
            matrix=transform_matrix,
            color=color
        )

        # Add any offsets required
        # curve_upper_points = get_point_array_with_offset(
        #     [
        #         p0, p1, p2, p3, p4, p5, p6, p7
        #     ],
        #     pos_offset,
        #     rot_offset
        # )
        #
        # curve_lower_points = get_point_array_with_offset(
        #     [
        #         N0, N1, N2, N3, N4, N5, N6, N7
        #     ],
        #     pos_offset,
        #     rot_offset
        # )
        #
        # # Create the curve object, set its attributes, and return it
        # curve_upper_node = curves.add_curve_node(
        #     parent_object=parent,
        #     name=name,
        #     points=curve_upper_points,
        #     closed=True,
        #     degree=3,
        #     matrix=transform_matrix,
        #     color=color
        # )
        #
        # curve_lower_node = curves.add_curve_node(
        #     parent_object=parent,
        #     name=name,
        #     points=curve_lower_points,
        #     closed=True,
        #     degree=3,
        #     matrix=transform_matrix,
        #     color=color
        # )
        #
        # # todo: fix issue where subcurves cannot be created
        # curve_fn = om2.MFnNurbsCurve()
        # node = curve_fn.create([curve_upper_node, curve_lower_node])

        return node


    @staticmethod
    def diamond(
            parent=None,
            name='diamond',
            width=1,
            color=(0, 0, 0),
            transform_matrix=None,
            pos_offset=None,
            rot_offset=None
    ):
        """
        This method creates a NURBS circle curve shape.

        :param parent: ``MObject`` Parent object to parent the curve as a
            transform to.

        :param name: ``str`` of the new object's name.

        :param width: ``float`` determining the width of the shape.

        :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

        :param transform_matrix: ``MTransformationMatrix`` specifying an initial
        transform for the created shape.

        :param pos_offset: ``MVector`` specifying a position offset for the
            created shape from its center origin.

        :param rot_offset: ``MVector`` specifying a rotation offset for the
            created shape from its center origin.

        :return: ``MFnNurbsCurve`` shape.
        """

        if not transform_matrix:
            transform_matrix = om2.MTransformationMatrix()

        width *= 0.5

        # Set coordinates for the points of the shape
        # Note: p is positive, N is negative
        top     = om2.MVector(0,            width,      0)
        pp      = om2.MVector(width,        0,          width)
        pN      = om2.MVector(width,        0,          -width)
        Np      = om2.MVector(-width,       0,          width)
        NN      = om2.MVector(-width,       0,          -width)
        bottom  = om2.MVector(0,            -width,     0)

        # Add any offsets required
        # Form closed diamond shape by drawing around all the lines and
        # closing the start/end cvs
        points = get_point_array_with_offset(
            [
                pp, top, pN, pp,
                Np, top, NN, Np,
                NN, pN, bottom, NN,
                bottom, Np, bottom, pp
            ],
            pos_offset,
            rot_offset
        )

        # Create the curve object, set its attributes, and return it
        node = curves.add_curve_node(
            parent_object=parent,
            name=name,
            points=points,
            closed=True,
            degree=1,
            matrix=transform_matrix,
            color=color
        )

        return node


    @staticmethod
    def peaked_cube(
        parent=None,
        name='peaked cube',
        width=1,
        color=(0,0,0),
        transform_matrix=None,
        pos_offset=None,
        rot_offset=None
    ):
        """
        This method creates a NURBS peaked cube curve shape.

        :param parent: ``MObject`` Parent object to parent the curve as a
            transform to.

        :param name: ``str`` of the new object's name.

        :param width: ``float`` determining the width of the shape.

        :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

        :param transform_matrix: ``MTransformationMatrix`` specifying an initial
            transform for the created shape.

        :param pos_offset: ``MVector`` specifying a position offset for the
            created shape from its center origin.

        :param rot_offset: ``MVector`` specifying a rotation offset for the
            created shape from its center origin.

        :return: ``MFnNurbsCurve`` shape.
        """

        if not transform_matrix:
            transform_matrix = om2.MTransformationMatrix()

        width *= 0.5

        # Set coordinates for the points of the peaked cube;
        # Note: p is positive, N is negative
        peak = om2.MVector(0,         width,         0)
        ppp  = om2.MVector(width,     width * 0.5,   width)
        ppN  = om2.MVector(width,     width * 0.5,  -width)
        pNp  = om2.MVector(width,     -width * 0.5,  width)
        Npp  = om2.MVector(-width,    width * 0.5,   width)
        pNN  = om2.MVector(width,     -width * 0.5, -width)
        NNp  = om2.MVector(-width,    -width * 0.5,  width)
        NpN  = om2.MVector(-width,    width * 0.5,  -width)
        NNN  = om2.MVector(-width,    -width * 0.5, -width)

        # Add any offsets required
        points = get_point_array_with_offset(
            [
                NNN, pNN, pNp, NNp, NNN,
                NpN, ppN, pNN, ppN, ppp, pNp, ppp, Npp, NNp, Npp, NpN,
                peak, Npp, peak, ppp, peak, ppN
            ],
            pos_offset,
            rot_offset
        )

        # Create the curve object, set its attributes, and return it
        node = curves.add_curve_node(
            parent_object=parent,
            points=points,
            name=name,
            degree=1,
            matrix=transform_matrix,
            color=color
        )

        return node


    @staticmethod
    def arrow(
        parent=None,
        name='arrow',
        width=1,
        color=(0,0,0),
        transform_matrix=None,
        pos_offset=None,
        rot_offset=None
    ):
        """
        This method creates a NURBS arrow curve shape.

        :param parent: ``MObject`` Parent object to parent the curve as a
            transform to.

        :param name: ``str`` of the new object's name.

        :param width: ``float`` determining the width of the shape.

        :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

        :param transform_matrix: ``MTransformationMatrix`` specifying an initial
            transform for the created shape.

        :param pos_offset: ``MVector`` specifying a position offset for the
            created shape from its center origin.

        :param rot_offset: ``MVector`` specifying a rotation offset for the
            created shape from its center origin.

        :return: ``MFnNurbsCurve`` shape.
        """

        if not transform_matrix:
            transform_matrix = om2.MTransformationMatrix()

        width *= 0.5

        # Set coordinates for the points of the peaked cube;
        v0 = om2.MVector(0,     0.3*width,      -width)
        v1 = om2.MVector(0,     0.3*width,      0.3*width)
        v2 = om2.MVector(0,     0.6*width,      0.3*width)
        v3 = om2.MVector(0,     0,              width)
        v4 = om2.MVector(0,     -0.6*width,     0.3*width)
        v5 = om2.MVector(0,     -0.3*width,     0.3*width)
        v6 = om2.MVector(0,     -0.3*width,     -width)

        # Add any offsets required
        points = get_point_array_with_offset(
            [
                v0, v1, v2, v3, v4, v5, v6
            ],
            pos_offset,
            rot_offset
        )

        # Create the curve object, set its attributes, and return it
        node = curves.add_curve_node(
            parent_object=parent,
            points=points,
            name=name,
            degree=1,
            closed=True,
            matrix=transform_matrix,
            color=color
        )

        return node


    @staticmethod
    def cross_arrow(
        parent=None,
        name='cross arrow',
        width=1,
        color=(0,0,0),
        transform_matrix=None,
        pos_offset=None,
        rot_offset=None
    ):
        """
        This method creates a NURBS cross arrow curve shape.

        :param parent: ``MObject`` Parent object to parent the curve as a
            transform to.

        :param name: ``str`` of the new object's name.

        :param width: ``float`` determining the width of the shape.

        :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

        :param transform_matrix: ``MTransformationMatrix`` specifying an initial
            transform for the created shape.

        :param pos_offset: ``MVector`` specifying a position offset for the
            created shape from its center origin.

        :param rot_offset: ``MVector`` specifying a rotation offset for the
            created shape from its center origin.

        :return: ``MFnNurbsCurve`` shape.
        """

        if not transform_matrix:
            transform_matrix = om2.MTransformationMatrix()

        width *= 0.5

        # Set coordinates for the points of the shape;
        v0  = om2.MVector(0.2*width,    0,      0.2*width)
        v1  = om2.MVector(0.2*width,    0,      0.6*width)
        v2  = om2.MVector(0.4*width,    0,      0.6*width)
        v3  = om2.MVector(0,            0,      width)
        v4  = om2.MVector(-0.4*width,   0,      0.6*width)
        v5  = om2.MVector(-0.2*width,   0,      0.6*width)
        v6  = om2.MVector(-0.2*width,   0,      0.2*width)
        v7  = om2.MVector(-0.6*width,   0,      0.2*width)
        v8  = om2.MVector(-0.6*width,   0,      0.4*width)
        v9  = om2.MVector(-width,       0,      0)
        v10 = om2.MVector(-0.6*width,   0,      -0.4*width)
        v11 = om2.MVector(-0.6*width,   0,      -0.2*width)
        v12 = om2.MVector(-0.2*width,   0,      -0.2*width)
        v13 = om2.MVector(-0.2*width,   0,      -0.6*width)
        v14 = om2.MVector(-0.4*width,   0,      -0.6*width)
        v15 = om2.MVector(0,            0,      -width)
        v16 = om2.MVector(0.4*width,    0,      -0.6*width)
        v17 = om2.MVector(0.2*width,    0,      -0.6*width)
        v18 = om2.MVector(0.2*width,    0,      -0.2*width)
        v19 = om2.MVector(0.6*width,    0,      -0.2*width)
        v20 = om2.MVector(0.6*width,    0,      -0.4*width)
        v21 = om2.MVector(width,        0,      0)
        v22 = om2.MVector(0.6*width,    0,      0.4*width)
        v23 = om2.MVector(0.6*width,    0,      0.2*width)

        # Add any offsets required
        points = get_point_array_with_offset(
            [
                v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,
                v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23
            ],
            pos_offset,
            rot_offset
        )

        # Create the curve object, set its attributes, and return it
        node = curves.add_curve_node(
            parent_object=parent,
            points=points,
            name=name,
            degree=1,
            closed=True,
            matrix=transform_matrix,
            color=color
        )

        return node


    @staticmethod
    def cross(
        parent=None,
        name='cross',
        width=1,
        color=(0,0,0),
        transform_matrix=None,
        pos_offset=None,
        rot_offset=None
    ):
        """
        This method creates a NURBS cross curve shape.

        :param parent: ``MObject`` Parent object to parent the curve as a
            transform to.

        :param name: ``str`` of the new object's name.

        :param width: ``float`` determining the width of the shape.

        :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

        :param transform_matrix: ``MTransformationMatrix`` specifying an initial
            transform for the created shape.

        :param pos_offset: ``MVector`` specifying a position offset for the
            created shape from its center origin.

        :param rot_offset: ``MVector`` specifying a rotation offset for the
            created shape from its center origin.

        :return: ``MFnNurbsCurve`` shape.
        """

        if not transform_matrix:
            transform_matrix = om2.MTransformationMatrix()

        width *= 0.5

        offset1 = width * 0.5
        offset2 = width * 1.5

        # Set coordinates for the points of the shape;
        v0  = om2.MVector(width,        offset2,      0)
        v1  = om2.MVector(offset2,      width,        0)
        v2  = om2.MVector(offset1,      0,            0)
        v3  = om2.MVector(offset2,      -width,       0)
        v4  = om2.MVector(width,        -offset2,     0)
        v5  = om2.MVector(0,            -offset1,     0)
        v6  = om2.MVector(-width,       -offset2,     0)
        v7  = om2.MVector(-offset2,     -width,       0)
        v8  = om2.MVector(-offset1,     0,            0)
        v9  = om2.MVector(-offset2,     width,        0)
        v10 = om2.MVector(-width,       offset2,      0)
        v11 = om2.MVector(0,            offset1,      0)

        # Add any offsets required
        points = get_point_array_with_offset(
            [
                v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11
            ],
            pos_offset,
            rot_offset
        )

        # Create the curve object, set its attributes, and return it
        node = curves.add_curve_node(
            parent_object=parent,
            points=points,
            name=name,
            degree=1,
            closed=True,
            matrix=transform_matrix,
            color=color
        )

        return node



