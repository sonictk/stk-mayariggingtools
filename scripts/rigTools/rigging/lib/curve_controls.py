#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module curve_controls: This module contains methods for creating curve control
objects for usage in Maya.
"""


def create_shape_control(

    shape,
    parent,
    name,
    transform_matrix,
    color,
    **kwargs
 ):
    """
    This method creates a control shape for use in Maya.

    :param shape:
    :param parent:
    :param name:
    :param transform_matrix:
    :param color:
    :param kwargs:
    :return:
    """

    pass
