#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module deformer_creator: This module contains methods to create deformer
objects in Maya.
"""
import logging

import pymel.core as pm
from PySide.QtGui import QButtonGroup

import rigTools.rigging.ui.deformer_creator as deformer_creator
from rigTools.lib import deformers
from rigTools.lib.api.globals import APIGlobal
from rigTools.lib.callbacks import SelectionChangedEvent
from rigTools.lib.deformers import Deformers
from rigTools.lib.selections import get_current_selection
from rigTools.lib.undo import UndoManager
from rigTools.lib.ui import MayaDockWidget, MayaShelfTool


class DeformerCreator(MayaDockWidget, MayaShelfTool):
    """
    This class contains methods for creating control shapes for rigs.
    """

    # Define shelf information
    shelf_tool_icon_name = 'deformer_creator'
    shelf_tool_title = 'Deformer Creator'


    def __init__(
            self,
            window_name='deformer_creator',
            title='Deformer Creator',
            sizeable=False
    ):
        # Read conventions and preferences data
        prefs_data = deformers.get_deformer_conventions()

        self.prefixes = prefs_data['prefixes']
        self.suffixes = prefs_data['suffixes']
        self.separators = prefs_data['separators']
        self.deformer_types = prefs_data['deformer_types']

        # Define enum for axes definitions from the UI to use for internal
        # functions
        self.enum_axes = {
            -2 : 'x',
            -3 : 'y',
            -4 : 'z',
            -1 : None
        }

        # Register a callback and monkey patch it to get the current
        # selection whenever it changes, then refresh the UI with the currently
        # selected object's name
        self.selection_changed_callback = SelectionChangedEvent
        self.selection_changed_callback.emit = self.refresh_ui
        self.callback_fn = SelectionChangedEvent()

        super(DeformerCreator, self).__init__(
            ui_class=deformer_creator.Ui_Form_deformer_creator,
            window_name=window_name,
            title=title,
            sizeable=sizeable
        )

        self.logger = logging.getLogger(__name__)


    def populateData(self, *args, **kwargs ):
        """
        This method populates the widget with all necessary data.
        """

        # Populate the UI comboboxes
        for prefix, object_type in sorted(self.prefixes.items()):
            self.ui.cmbBox_prefix.addItem(prefix, object_type)

        for suffix, long_name in sorted(self.suffixes.items()):
            self.ui.cmbBox_suffix.addItem(suffix, long_name)

        for separator, long_name in sorted(self.separators.items()):
            self.ui.cmbBox_separator.addItem(long_name, separator)

        for name, node_type in sorted(self.deformer_types.items()):
            self.ui.cmbBox_deformer_type.addItem(name, node_type)

        self.ui.txt_basename.setPlaceholderText(
            'Enter a name for the deformer(s)...'
        )

        # Set defaults for UI widget states
        self.ui.chk_name_prefix.setChecked(True)
        self.ui.chk_name_suffix.setChecked(True)
        self.ui.chk_parent_in_sequence.setChecked(True)

        # Get the current scene world up axis as a vector and update the UI
        model_up_axis = APIGlobal.upAxis()

        # Create groups for the radio buttons
        self.ui.rb_grp_axis_aim = QButtonGroup(self)
        self.ui.rb_grp_axis_up = QButtonGroup(self)
        self.ui.rb_grp_axis_up_world_orient = QButtonGroup(self)

        self.ui.rb_grp_axis_aim.addButton(self.ui.rb_axis_aim_x)
        self.ui.rb_grp_axis_aim.addButton(self.ui.rb_axis_aim_y)
        self.ui.rb_grp_axis_aim.addButton(self.ui.rb_axis_aim_z)

        self.ui.rb_grp_axis_up.addButton(self.ui.rb_axis_up_x)
        self.ui.rb_grp_axis_up.addButton(self.ui.rb_axis_up_y)
        self.ui.rb_grp_axis_up.addButton(self.ui.rb_axis_up_z)

        self.ui.rb_grp_axis_up_world_orient.addButton(
            self.ui.rb_up_axis_world_orient_x
        )
        self.ui.rb_grp_axis_up_world_orient.addButton(
            self.ui.rb_up_axis_world_orient_y
        )
        self.ui.rb_grp_axis_up_world_orient.addButton(
            self.ui.rb_up_axis_world_orient_z
        )

        # By default, set prefix to 'jnt'
        self.ui.cmbBox_prefix.setCurrentIndex(
            self.ui.cmbBox_prefix.findText('jnt')
        )

        # By default, delete the original guides
        self.ui.chk_delete_orig_guides.setChecked(True)

        # By default, set separator to underscore
        self.ui.cmbBox_separator.setCurrentIndex(
            self.ui.cmbBox_separator.findText('Underscore')
        )

        # By default, set Y-axis as aim axis and X-axis as up axis
        self.ui.rb_axis_aim_y.setChecked(True)
        self.ui.rb_axis_up_x.setChecked(True)

        # Set the default orient up axis based on the current world up axis
        if model_up_axis.x:
            self.ui.rb_up_axis_world_orient_x.setChecked(True)
        elif model_up_axis.y:
            self.ui.rb_up_axis_world_orient_y.setChecked(True)
        else:
            self.ui.rb_up_axis_world_orient_z.setChecked(True)

        # Set Z-axis as default mirror axis
        self.ui.rb_mirror_z.setChecked(True)


    def makeConnections(self, *args, **kwargs):
        """
        This method handles connecting UI widgets to their actions.
        """

        self.ui.chk_name_prefix.stateChanged.connect(
           lambda _: self.ui.cmbBox_prefix.setEnabled(
               self.ui.chk_name_prefix.isChecked()
           )
        )

        self.ui.chk_name_suffix.stateChanged.connect(
           lambda _: self.ui.cmbBox_suffix.setEnabled(
               self.ui.chk_name_suffix.isChecked()
           )
        )

        self.ui.chk_orient_to_world.stateChanged.connect(
            lambda _: self.ui.grp_axes.setEnabled(
                not self.ui.chk_orient_to_world.isChecked()
            )
        )

        # Connect UI buttons
        self.ui.btn_rename.clicked.connect(
            lambda: self.rename_selected()
        )

        self.ui.btn_relabel.clicked.connect(
            lambda: self.relabel_selected()
        )

        self.ui.btn_create_deformers.clicked.connect(
            self.create_deformers_from_selected_guides
        )

        # todo: implement insert deformers function
        # self.ui.btn_insert_deformers.clicked.connect(
        #
        # )

        self.ui.btn_close.clicked.connect(self.cancelAction)


    def closeEvent(self, event):
        """
        Un-registers the custom callback when this widget is closed.

        :param event:
        :return:
        """
        if hasattr(self, 'callback_fn'):
            self.callback_fn.remove_callback()
            self.callback_fn.emit=None
            del self.callback_fn

        return super(DeformerCreator, self).closeEvent(event)


    def refresh_ui(self, event, refresh_base_name=True):
        """
        This method is run whenever the current selection changes. It is
        responsible for refreshing the current appearance of the UI.

        :param event:

        :param refresh_base_name: ``bool`` indicating if the object name
            should be refreshed whenever the UI is updated.

        :return:
        """
        if refresh_base_name:
            # Update the base name text field
            current_selection = get_current_selection()

            if current_selection:
                self.ui.txt_basename.setText(current_selection[0].name())


    @UndoManager.undoable
    def create_deformers_from_selected_guides(self):
        """
        This callback creates defomers for each of the selected guide objects
        and places them at the objects' positions.
        """

        # Get user input from the UI for forming the names of the new deformers
        base_name = self.ui.txt_basename.text()

        if not base_name:
            APIGlobal.displayWarning('You must enter a name for the deformers!')
            return

        deformer_type = self.ui.cmbBox_deformer_type.itemData(
            self.ui.cmbBox_deformer_type.currentIndex()
        )

        if self.ui.chk_name_suffix.isChecked():
            side = self.ui.cmbBox_suffix.itemData(
                self.ui.cmbBox_suffix.currentIndex()
            )
            suffix = self.ui.cmbBox_suffix.currentText()
        else:
            side = None
            suffix = None

        if self.ui.chk_name_prefix.isChecked():
            prefix = self.ui.cmbBox_prefix.currentText()
        else:
            prefix = None

        name_separator = self.ui.cmbBox_separator.itemData(
            self.ui.cmbBox_separator.currentIndex()
        )

        # Account for user-specified orientation of joint axes
        negate_aim_axis = self.ui.chk_negate_aim_axis.isChecked()
        negate_up_axis = self.ui.chk_negate_up_axis.isChecked()
        negate_orient_to_world_axis = self.ui.chk_negate_up_world_orient_axis\
            .isChecked()

        aim_axis = self.enum_axes[self.ui.rb_grp_axis_aim.checkedId()]
        if negate_aim_axis:
            aim_axis = '-{0}'.format(aim_axis)

        up_axis = self.enum_axes[self.ui.rb_grp_axis_up.checkedId()]
        if negate_up_axis:
            up_axis = '-{0}'.format(up_axis)

        orient_to_world_axis = self.enum_axes[
            self.ui.rb_grp_axis_up_world_orient.checkedId()
        ]
        if negate_orient_to_world_axis:
            orient_to_world_axis = '-{0}'.format(orient_to_world_axis)

        guides = get_current_selection(filter_type='transform')

        if not guides:
            APIGlobal.displayError('You need to select some guide objects!!!')
            return

        if self.ui.chk_reverse_sequence.isChecked():
            guides.reverse()

        new_deformers = Deformers().create_deformers(
            guide_objects=guides,
            deformer_type=deformer_type,
            side=side,
            aim_axis=aim_axis, up_axis=up_axis, world_up_axis=orient_to_world_axis,
            base_name=base_name,
            parent_in_sequence=self.ui.chk_parent_in_sequence.isChecked()
        )

        Deformers.rename_deformers(
            nodes=new_deformers,
            base_name=base_name,
            deformer_type=deformer_type,
            prefix=prefix,
            suffix=suffix,
            separator=name_separator
        )

        # todo: implement orienting deformers when they are created
        # Deformers.orient_deformers(
        #     nodes=new_deformers
        # )

        if self.ui.chk_delete_orig_guides.isChecked():
            pm.delete(guides)


    def rename_selected(self, prefix='', suffix='', separator=None):
        """
        This method is executed when the user clicks the Rename Selected
        button in the UI.

        Renames the currently selected deformers based on the type chosen in
        the UI.

        :param prefix: ``str`` that will be appended to the name of each
            deformer.

        :param suffix: ``str`` that will be added to the end of the name of
            each deformer.

        :param separator: ``str`` that will be used to separate each element of
            the name in the deformer.

        :return: ``None``
        """

        # Get user input from the UI
        base_name = self.ui.txt_basename.text()

        if not base_name:
            APIGlobal.displayWarning('You must enter a name for the deformers!')
            return

        deformer_type = self.ui.cmbBox_deformer_type.itemData(
            self.ui.cmbBox_deformer_type.currentIndex()
        )

        if self.ui.chk_name_prefix.isChecked():
            prefix = self.ui.cmbBox_prefix.currentText()

        if self.ui.chk_name_suffix.isChecked():
            suffix = self.ui.cmbBox_suffix.currentText()

        if not separator:
            separator = self.ui.cmbBox_separator.itemData(
                self.ui.cmbBox_separator.currentIndex()
            )

        Deformers.rename_deformers(
            base_name=base_name,
            deformer_type=deformer_type,
            prefix=prefix,
            suffix=suffix,
            separator=separator
        )


    def relabel_selected(self, side='', basename=''):
        """
        This method is executed when the user clicks the Relabel Selected
        button in the UI.

        Re-labels the selected deformers based on the side and base name given.

        :param side: ``str`` that will be set as the side label of the deformer.

        :param basename: ``str`` that will act as the base name for the
            deformers to be renamed as.

        :return: ``None``
        """

        if not basename:
            # Get user input from the UI
            basename = self.ui.txt_basename.text()

        if not basename:
            APIGlobal.displayWarning('You must enter a name for the label!')
            return

        deformer_type = self.ui.cmbBox_deformer_type.itemData(
            self.ui.cmbBox_deformer_type.currentIndex()
        )

        if self.ui.chk_name_suffix.isChecked():
            side = self.ui.cmbBox_suffix.itemData(
                self.ui.cmbBox_suffix.currentIndex()
            )

        deformers_to_relabel = pm.ls(sl=True, typ=deformer_type)

        deformers = Deformers().relabel_deformers(
            deformers=deformers_to_relabel,
            label_base_name=basename,
            side=side
        )

        return deformers