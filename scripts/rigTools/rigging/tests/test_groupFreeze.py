#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module test_groupFreeze: This is a unit test."""
import pymel.core as pm
from rigTools.rigging.group_freeze import GroupFreeze
from rigTools.lib.test import MayaTestCase


class TestGroupFreeze(MayaTestCase):
    def setUp(self):
        self.test_transform = pm.createNode('transform')
        self.offset = (5, 5, 5)
        pm.move(self.test_transform, self.offset)
        pm.select(self.test_transform, replace=True)

    def test_groupFreeze(self):
        new_grp = GroupFreeze.groupFreeze()
        self.assertIsInstance(new_grp, list)
        new_grp_pos = pm.xform(new_grp[0], q=True, t=True)
        self.assertEqual(
            [i == self.offset[idx] for idx, i in enumerate(new_grp_pos)],
            [True, True, True],
            'The group freeze operation did not complete successfully!\n'
            'The new position of the group is: {0} while it should '
            'be: {1}!'.format(new_grp_pos, self.offset)
        )
