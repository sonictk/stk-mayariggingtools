#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module deformer_orienter: This module contains methods to orient deformer
objects in Maya.
"""
import logging
import maya.OpenMaya as om
from rigTools.lib.deformers import Deformers
from rigTools.lib.node import toggle_local_axes
from rigTools.lib.selections import get_current_selection
import rigTools.rigging.ui.deformer_orienter as deformer_orienter

from PySide.QtGui import QButtonGroup
from rigTools.lib.ui import MayaDockWidget, MayaShelfTool


class DeformerOrienter(MayaDockWidget, MayaShelfTool):
    """
    This class contains methods for creating control shapes for rigs.
    """

    # Define shelf information
    shelf_tool_icon_name = 'deformer_orienter'
    shelf_tool_title = 'Deformer Orienter'


    def __init__(
            self,
            window_name='deformer_orienter',
            title='Deformer Orienter',
            sizeable=False
    ):

        self.logger = logging.getLogger(__name__)

        super(DeformerOrienter, self).__init__(
            ui_class=deformer_orienter.Ui_Form_deformer_orienter,
            window_name=window_name,
            title=title,
            sizeable=sizeable
        )


    def populateData(self, *args, **kwargs ):
        """
        This method populates the widget with all necessary data.
        """

        # Create groups for radio buttons
        self.ui.rb_grp_axis_aim = QButtonGroup(self)
        self.ui.rb_grp_axis_up = QButtonGroup(self)

        self.ui.rb_grp_axis_aim.addButton(self.ui.rb_axis_aim_x)
        self.ui.rb_grp_axis_aim.addButton(self.ui.rb_axis_aim_y)
        self.ui.rb_grp_axis_aim.addButton(self.ui.rb_axis_aim_z)

        self.ui.rb_grp_axis_up.addButton(self.ui.rb_axis_up_x)
        self.ui.rb_grp_axis_up.addButton(self.ui.rb_axis_up_y)
        self.ui.rb_grp_axis_up.addButton(self.ui.rb_axis_up_z)

        # By default, set Y-axis as aim axis and X-axis as up axis
        self.ui.rb_axis_aim_y.setChecked(True)
        self.ui.rb_axis_up_x.setChecked(True)

        # By default, set X-axis as tweak orientation axis
        self.ui.rb_tweak_axis_x.setChecked(True)

        # Get the current maya world up axis as a vector and update the UI
        model_up_axis = om.MGlobal.upAxis()

        self.ui.txt_world_up_axis_x.setValue(model_up_axis.x)
        self.ui.txt_world_up_axis_y.setValue(model_up_axis.y)
        self.ui.txt_world_up_axis_z.setValue(model_up_axis.z)

        self.ui.txt_world_up_axis_x.setSingleStep(0.1)
        self.ui.txt_world_up_axis_y.setSingleStep(0.1)
        self.ui.txt_world_up_axis_z.setSingleStep(0.1)

        self.ui.txt_world_up_axis_x.setMaximum(1.0)
        self.ui.txt_world_up_axis_y.setMaximum(1.0)
        self.ui.txt_world_up_axis_z.setMaximum(1.0)

        self.ui.txt_world_up_axis_x.setMinimum(-1.0)
        self.ui.txt_world_up_axis_y.setMinimum(-1.0)
        self.ui.txt_world_up_axis_z.setMinimum(-1.0)

        self.ui.txt_tweak_degrees.setSingleStep(90.0)


    def makeConnections(self, *args, **kwargs):
        """
        This method handles connecting UI widgets to their actions.
        """

        self.ui.btn_show_axes.clicked.connect(
            lambda: self.toggle_axes_display()
        )

        self.ui.btn_hide_axes.clicked.connect(
            lambda: self.toggle_axes_display(toggle=False, state=False)
        )

        self.ui.btn_show_labels.clicked.connect(
            lambda: self.toggle_labels_display()
        )

        self.ui.btn_hide_labels.clicked.connect(
            lambda: self.toggle_labels_display(toggle=False, state=False)
        )

        self.ui.chk_auto_guess_world_up_axis.stateChanged.connect(
            self.refresh_ui
        )

        self.ui.btn_world_up_x.clicked.connect(
            lambda: self.set_ui_world_axis_value(
                {
                    'x' : 1.0,
                    'y' : 0.0,
                    'z' : 0.0
                }
            )
        )

        self.ui.btn_world_up_y.clicked.connect(
            lambda: self.set_ui_world_axis_value(
                {
                    'x' : 0.0,
                    'y' : 1.0,
                    'z' : 0.0
                }
            )
        )

        self.ui.btn_world_up_z.clicked.connect(
            lambda: self.set_ui_world_axis_value(
                {
                    'x' : 0.0,
                    'y' : 0.0,
                    'z' : 1.0
                }
            )
        )

        self.ui.btn_close.clicked.connect(self.cancelAction)


    def refresh_ui(self):
        """
        This callback is executed whenever the UI needs to be updated.

        :return: ``None``
        """

        auto_guess = not self.ui.chk_auto_guess_world_up_axis.isChecked()

        # Enable/Disable the world up axis widgets
        self.ui.lbl_world_up_axis.setEnabled(auto_guess)
        self.ui.txt_world_up_axis_x.setEnabled(auto_guess)
        self.ui.txt_world_up_axis_y.setEnabled(auto_guess)
        self.ui.txt_world_up_axis_z.setEnabled(auto_guess)
        self.ui.btn_world_up_x.setEnabled(auto_guess)
        self.ui.btn_world_up_y.setEnabled(auto_guess)
        self.ui.btn_world_up_z.setEnabled(auto_guess)


    def set_ui_world_axis_value(self, axes):
        """
        This callback sets the value of the UI world up axis fields.

        :param axes: ``dict`` indicating the axes chosen and the values to set.
        :return: ``None``
        """

        self.ui.txt_world_up_axis_x.setValue(axes.get('x'))
        self.ui.txt_world_up_axis_y.setValue(axes.get('y'))
        self.ui.txt_world_up_axis_z.setValue(axes.get('z'))


    def toggle_axes_display(self, toggle=True, state=True):
        """
        This callback toggles displaying the selected deformers' local
        rotation axes.

        :param state:  ``bool`` indicating what the state should be set to.
            Ignored if ``toggle`` is set to ``True``.
        :param toggle: ``bool`` indicating if the state should be toggled or not.
        """

        toggle_local_axes(
            get_current_selection(filter_type='transform'), toggle, state
        )


    def toggle_labels_display(self, toggle=True, state=True):
        """
        This callback toggles displaying the selected deformers' labels.

        :param state:  ``bool`` indicating what the state should be set to.
            Ignored if ``toggle`` is set to ``True``.
        :param toggle: ``bool`` indicating if the state should be toggled or not.
        """

        Deformers.toggle_label_display(
            get_current_selection(filter_type='transform'), toggle, state
        )


    def create_mirrored_from_selected(self, mirror_axis):
        """
        This callback creates a mirrored set of joints about the specified axis.

        :param mirror_axis: ``str`` indicating the axis to mirror across.
        :return:
        """

        pass