#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module reset_skinned_joints: This module contains functions for resetting
skin deformation from joints.
"""

import logging

import pymel.core as pm

from rigTools.lib.api.globals import APIObject, APIFnDGNode, APIItSelectionList
from rigTools.lib.selections import get_active_selection_list, get_current_selection
from rigTools.lib.undo import UndoManager

logger = logging.getLogger(__name__)


@UndoManager.repeatable
@UndoManager.undoable
def resetSkinnedJoints(joints=None):
    """
    Reset skin deformation for selected joint(s)

    :param joints: ``list`` of ``nt.Transform`` joint objects.
    """

    if not joints:
        joints = get_current_selection(filter_type='joint')

    for joint in joints:

        # Get connected skinCluster nodes
        skinCluster_plugs = pm.listConnections(
            joint + '.worldMatrix[0]',
            type='skinCluster',
            plugs=1
        )

        if skinCluster_plugs:
            for plug in skinCluster_plugs:

                # Get the name of the skinCluster
                skincluster_name = plug.nodeName()
                index = plug.index()

                jnt_inverse_matrix = pm.getAttr(joint + '.worldInverseMatrix')

                # Flatten the nested lists
                jnt_inverse_matrix = [j for i in jnt_inverse_matrix for j in i]

                pm.setAttr(
                    skincluster_name + '.bindPreMatrix[{0}]'.format(index),
                    type='matrix',
                    *jnt_inverse_matrix
                )

            logger.info('Successfully reset skinned joints!')

        else:
            logger.error('### No skinCluster attached to {0}!!!'.format(joint))


@UndoManager.repeatable
@UndoManager.undoable
def reset_skinned_joints(joints_list=None):
    """
    Resets the skin deformation for the selected joint(s).

    :param joints_list: ``MSelectionList`` of the joint nodes.
    :return:
    """
    # todo: finish implmementation

    if not joints_list:
        joints_list = get_active_selection_list(filter_type='joint')

    # Create iterator to go over the selection list
    fn_iterator = APIItSelectionList(joints_list)

    while not fn_iterator.isDone():
        # Get connected skinCluster plugs

        node = APIObject()
        fn_iterator.getDependNode(node)

        dg_node = APIFnDGNode(node)

        plugs = dg_node.get_connections()



        fn_iterator.next()

