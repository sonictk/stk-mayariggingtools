#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module HelpMenuActions: This module contains callbacks that are executed from
menu items in the Help menu.
"""
import logging
import os
import webbrowser
import maya.api.OpenMaya as om2
from PySide.QtGui import QMessageBox, QFileDialog
from rigTools.lib.system import RigToolsSettings
from rigTools.lib.ui import get_maya_window

from rigTools.lib.system import Constants


def view_about_info():
    """
    This method opens a dialog displaying information about the plugin.

    :return: ``None``
    """
    _constants_data = Constants()

    info = 'Name of plugin: {0}\n' \
           'Version: {1}\n' \
           'Language: {2}\n' \
           'Description: {3}\n' \
           'Organization: {4}\n'\
        .format(
        _constants_data.plugin_name,
        _constants_data.plugin_version,
        _constants_data.plugin_language,
        _constants_data.application_description,
        _constants_data.organization_name
    )

    QMessageBox.information(
        get_maya_window(),
        'About',
        info
    )


def view_documentation(docs_path=''):
    """
    This method opens a web browser pointing to the in-built documentation.
    If no path is specified, one will attempt to be formatted.

    :param docs_path: ``str`` that is path to the documentation.
    :return: ``None``
    """

    logger = logging.getLogger(__name__)

    if not docs_path:
        docs_path = os.path.join(
            os.path.dirname(
                os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(os.path.abspath(__file__))
                    )
                )
            ),
            'docs', 'build', 'html', 'index.html'
        )

    if not os.path.isfile(docs_path) and docs_path[0:4] != 'http':
        logger.error('### Could not find documentation at the following '
                     'location: {0}!!!'.format(docs_path))

        om2.MGlobal.displayError('Unable to display documentation! Please view '
                                 'the script editor for details!')
    else:
        logger.debug('Opening {0}'.format(docs_path))
        webbrowser.open(docs_path)


def save_maya_developer_documentation_path(docs_path):
    """
    This method stores the preference of where the Maya developer documentation
    is installed on the local workstation.

    :param docs_path: ``str`` that is file path to the Maya developer
        documentation.
    :return: ``None``
    """

    logger = logging.getLogger(__name__)

    if not os.path.isfile(docs_path):
        logger.error('### Could not find documentation at the following '
                     'location: {0}!!!'.format(docs_path))

        om2.MGlobal.displayError('Invalid location for documentation was '
                                 'specified!')

    else:
        # Store to the global application preferences
        settings = RigToolsSettings()

        settings.createSetting(
            'help',
            'maya_developer_help_location',
            docs_path
        )


def set_maya_developer_documentation_path():
    """
    This method opens a dialog for allowing the user to specify a custom
    location for the Maya developer documentation on the local workstation.

    If a valid directory is returned, saves the chosen directory to the
    global tool settings.

    :return: ``None``
    """

    # Open user prompt
    help_directory = QFileDialog.getExistingDirectory(
        get_maya_window(),
        'Select directory of Maya developer documentation...'
    )

    if help_directory:

        help_directory = os.path.join(help_directory, 'index.html')

        save_maya_developer_documentation_path(help_directory)


def open_maya_developer_documentation():
    """
    This method opens the Maya Developer documentation.

    :return: ``None``
    """

    settings = RigToolsSettings()

    docs_path = settings.readSetting(
        'help', 'maya_developer_help_location'
    )

    if not docs_path:
        docs_path = 'http://www.autodesk.com/maya-sdkdoc-2016-enu'

    view_documentation(docs_path)
