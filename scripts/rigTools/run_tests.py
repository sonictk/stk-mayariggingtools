#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module run_tests: This module is a command-line utility for running all
unit tests in this toolkit."""

import argparse
import os
import sys
import warnings
from subprocess import check_call


def get_maya_location(maya_version='2016'):
    """
    This function attempts to find an existing installation of Maya on the
    filesystem and returns the path.

    :param maya_version: ``str`` The version of Maya to attempt to locate.

        NOTE:
            For Maya versions before 2016, the architecture should be be also
            specified in the name i.e. ``2015-x64``.

    :return: ``str`` If found, the the path to the Maya installation directory,
        or ``None`` if the location cannot be determined.
    """
    if os.environ.get('MAYA_LOCATION'):
        maya_location = os.environ['MAYA_LOCATION']
    else:
        # Look for Maya default installation in platform-speciifc locations
        if sys.platform == 'win32':
            maya_location = os.path.join(
                os.environ.get('PROGRAMFILES'),
                'Autodesk',
                'Maya{0}'.format(maya_version)
            )
        elif sys.platform == 'darwin':
            maya_location = os.path.join(
                'Applications',
                'Autodesk',
                'maya{0}'.format(maya_version),
                'Maya.app',
                'Contents'
            )
        elif sys.platform == 'linux2':
            maya_location = os.path.join('/usr',
                                         'autodesk',
                                         'maya{0}'.format(maya_version))
        else:
            raise OSError('Platform not supported: {0}!'.format(sys.platform))

    if os.path.isdir(maya_location):
        return maya_location
    else:
        return None


def get_mayapy_executable_path(maya_version='2016'):
    """
    This function find the executable path to the ``mayapy`` Python interpreter.

    :param maya_version: ``str`` The version of Maya to attempt to locate.

        NOTE:
            For Maya versions before 2016, the architecture should be be also
            specified in the name i.e. ``2015-x64``.

    :return: ``str`` If found, the the path to the ``mayapy`` executable,
        or ``None`` if the location cannot be determined.
    """
    maya_location = get_maya_location(maya_version=maya_version)
    if not maya_location:
        raise RuntimeError('Could not find an installation of Maya on this system!')
    mayapy_location = os.path.join(get_maya_location(maya_version), 'bin', 'mayapy')
    if sys.platform == 'win32':
        mayapy_location += '.exe'
    if os.path.isfile(mayapy_location):
        return mayapy_location



# This is the entry point for running unit tests on this toolkit.
if __name__ == '__main__':
    import __main__
    parser = argparse.ArgumentParser(
        description='Runs the unit tests for this toolkit.'
    )
    parser.add_argument('-d', '--directories',
                        help='(list) A list of directories to use for finding '
                             'the tests. If not specified, all tests for the '
                             'toolkit are run.',
                        type=list,
                        default=None)

    parser.add_argument('-t', '--test',
                        help='(str) The name of a specific test to run for '
                             'this toolkit. This can be specified together '
                             'with the --directories flag as well. The name '
                             'should be the name of the test module itself, without '
                             'the .py extension.',
                        type=str,
                        default='')

    parser.add_argument('-mv', '--maya_version',
                        help='(str) The version of Maya to use for running the tests. '
                             '(e.g. 2015-x64, 2016)',
                        type=str,
                        default='2016')

    parser.add_argument('-v', '--verbosity',
                        help='(int) Specifies the verbosity level to use for '
                             'logging information output.\n'
                             '\t0 : Errors only.\n'
                             '\t1 : Show warnings.\n'
                             '\t2 : Show informational messages.\n'
                             '\t3 : Show debug messages.\n',
                        type=int,
                        default=3)

    parsed_arguments = parser.parse_args()
    maya_version = parsed_arguments.maya_version

    # Check whether being run from a mayapy interpreter session and start a new
    # one in a subprocess if necessary to run the tests
    try:
        import maya.OpenMaya as om
        if om.MGlobal.mayaState() in (om.MGlobal.kBatch, om.MGlobal.kInteractive):
            subprocess_required = False
        else:
            subprocess_required = True
    except (ImportError, TypeError):
        subprocess_required = True

    if subprocess_required is True:
        # Find the mayapy interpreter on the system
        mayapy_location = get_mayapy_executable_path(maya_version)
        if not os.path.isfile(mayapy_location):
            raise RuntimeError('Could not determine location of mayapy Python '
                               'interpreter location! Please '
                               'set the MAYA_LOCATION environment '
                               'variable to point to your Maya installation '
                               'directory on the filesystem!')
        # Format the command that will be executed for running the unit tests
        cmd = [mayapy_location,
               os.path.join(
                   os.path.dirname(__main__.__file__),
                   __main__.__file__
               )]
    else:
        cmd = None

    # Set up the ``PYTHONPATH`` if necessary, since the interpreter could have
    # been run in a different environment
    # Ensure that the toolkit sources are in the PATH before attempting to
    # import any modules from the toolkit
    toolkit_py_sources_path = os.path.normpath(os.path.dirname(
        os.path.dirname(__main__.__file__)
    ))
    if not toolkit_py_sources_path in sys.path:
        sys.path.insert(0, toolkit_py_sources_path)
    toolkit_root = os.path.dirname(toolkit_py_sources_path)

    # Add the plugin binaries to the Maya plugin path
    maya_plugin_path = os.path.join(toolkit_root, 'plug-ins', 'bin')

    if not os.path.isdir(maya_plugin_path):
        warnings.warn('The plugin binaries directory: {0} could not be found!'
                             'Tests that rely on compiled plugins in this toolkit '
                             'will not be able to run successfully!'
                      .format(maya_plugin_path))
    else:
        os.environ['MAYA_PLUG_IN_PATH'] = maya_plugin_path

    # Setup a clean barebones Maya environment for testing with default preferences
    os.environ['MAYA_SCRIPT_PATH'] = ''
    os.environ['MAYA_MODULE_PATH'] = toolkit_root
    temp_clean_prefs_dir = os.path.normpath(os.path.join(
        os.path.dirname(
            os.path.dirname(
                os.path.dirname(__main__.__file__))),
        'resource',
        'default_maya_prefs',
        '{0}'.format(maya_version)
    ))
    if os.path.isdir(temp_clean_prefs_dir):
        os.environ['MAYA_APP_DIR'] = temp_clean_prefs_dir

    # If not using the mayapy interpreter, start a subprocess that runs it instead
    if subprocess_required is True and cmd:
        sys.stdout.write('Starting mayapy subprocess for running tests..\n')
        check_call(cmd)
    else:
        from rigTools.lib.test import run_toolkit_tests
        run_toolkit_tests(directories=parsed_arguments.directories,
                          test=parsed_arguments.test,
                          verbosity=parsed_arguments.verbosity)
