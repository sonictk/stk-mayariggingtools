#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module rigTools_shelf: This module contains methods for creating a
custom shelf toolbar with readily accessible rigging tools in Maya.
"""
import importlib
import inspect
import logging
import os
from PySide.QtCore import Qt
from PySide.QtGui import QIcon
from rigTools.help.menu_actions import view_documentation

from rigTools.lib.ui import MayaToolbar, MayaShelfTool
from rigTools.lib.system import Constants


class RigToolsShelf(MayaToolbar):
    """
    This class creates a custom shelf in Maya that allows easy access to
    common rigging tools and utilities.
    """

    def __init__(
            self,
            title='Rig Tools Shelf',
            insert_before='Tool Box',
            icon_display_style=Qt.ToolButtonIconOnly
    ):
        """
        The constructor.

        :return:
        """

        self.logger = logging.getLogger(__name__)

        # Get all available actions from inspecting modules
        actions = []

        folders_to_register = [
            'rigging',
            'skinning'
        ]

        base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        for folder_name in folders_to_register:
            folder_path = os.path.join(base_path, folder_name)

            if os.path.isdir(folder_path):
                actions.append(
                    self.register_shelf_actions(folder_path, Constants().plugin_name)
                )

        super(RigToolsShelf, self).__init__(
            title,
            actions[0],
            insert_before=insert_before,
            icon_display_style=icon_display_style
        )


    def register_shelf_actions(self, folder_path, base_namespace):
        """
        This method will inspect all submodules from the given folder and
        search for valid shelf tool items, then attempt to register them.

        :param folder_path: ``str`` that is the directory to search through

        :param base_namespace: ``str`` that is the base namespace to import
            submodules from.

        :return: ``list`` of actions to register.
        """

        modules = []
        actions = []

        for root, dirs, filenames in os.walk(folder_path):

            for filename in filenames:

                # Only grab .py files and not files like __init__.py
                if filename.endswith('.py') and not filename.startswith('__'):

                    # Filter out broken symlinks and directories called 'dir.py/'
                    if os.path.isfile(os.path.join(root, filename)):
                        modules.append((filename, os.path.join(root, filename)))

        for module in modules:

            # Format the module namespace to be absolute
            module_namespace = module[1][module[1].find(base_namespace):-3]\
                .replace(os.path.sep, '.')

            # Import the module and inspect it
            loaded_submodule = importlib.import_module(name=module_namespace)

            class_members = inspect.getmembers(loaded_submodule, inspect.isclass)

            # Filter out class members which are not defined within the module itself
            class_members = filter(
                lambda x:inspect.getmodule(x[1])==loaded_submodule,
                class_members
            )

            # Now inspect each class for a valid shelf subclass
            for class_member in class_members:

                # Skip class if it is not a valid subclass
                if not issubclass(class_member[1], MayaShelfTool):
                    continue

                # Get the MRO and check if it matches the valid subclass
                if inspect.getmro(class_member[1])[-2] == MayaShelfTool:

                    tool_title = class_member[1].get_shelf_tool_title()
                    tool_icon = class_member[1].get_shelf_tool_icon()

                    actions.append([tool_icon, tool_title, class_member[1]])

        # Add the documentation help actions to the end of the shelf
        icons_path = os.path.join(
            os.path.dirname(
                os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(os.path.abspath(__file__))
                    )
                )
            ), 'icons'
        )
        docs_icon_path = os.path.join(icons_path, 'help.png')

        if os.path.isfile(docs_icon_path):
            actions.append(
                [QIcon(docs_icon_path), 'View Help', lambda: view_documentation()]
            )

        return actions

