#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module setup: This module contains methods for setting up the Rigging Tools
environment in Maya.
"""
import logging
import pymel.core as pm
import rigTools.completion.generate_om2_autocompletions as generate_om2
import rigTools.completion.generate_om_autocompletions as generate_om
import rigTools.help.menu_actions as help_menu
import rigTools.lib.compile_qt_resources as compileQt
import rigTools.lib.debug as debug
import rigTools.lib.reload as reloader
import rigTools.lib.selections as selections
import rigTools.rigging.blendshape_symmetrizer as blendshape_symmetrizer
import rigTools.rigging.change_node_color as change_color
import rigTools.rigging.control_shape_creator as control_shapes_creator
import rigTools.rigging.create_null_groups as create_nulls
import rigTools.rigging.deformer_creator as deformer_creator
import rigTools.rigging.deformer_orienter as deformer_orienter
import rigTools.rigging.group_freeze as group_freeze
import rigTools.rigging.helpers as helpers
import rigTools.rigging.rename_nodes as renamer
import rigTools.shelf.rigTools_shelf as shelf
import rigTools.skinning.reset_skinned_joints as reset_jnts
from rigTools.lib.system import Constants
from rigTools.rigging.tension_deformer import TensionSetup
from rigTools.lib.plugin import reload_all_plugins, unload_rig_tools_plugins


# TODO: Shift this to use the inspect class type method of creating the menus automatically
class CreateRigToolsMenu(object):
    """
    This class creates the menu entry in Maya for the Rigging Tools.
    """

    def __init__(self):
        """
        The constructor.
        """

        self.logger = logging.getLogger(__name__)

        self._constants_data = Constants()

        self._organization_name = self._constants_data.organization_name
        self._application_name = self._constants_data.application_name
        self._menu_name = self._constants_data.menu_name
        self._applciation_description = self._constants_data.application_description

        menu_exists = self.check_for_existing_menu(
            self._organization_name, docTag=self._organization_name
        )

        # If the menu exists but docTag does not match correct one, rebuild
        # the menu
        if not menu_exists:
            if pm.menu(self._organization_name, q=True, exists=True):
                pm.deleteUI(self._organization_name)

            self.top_level_menu = pm.menu(
                self._organization_name,
                p='MayaWindow',
                docTag=self._organization_name,
                tearOff=True,
                allowOptionBoxes=True,
                label=self._organization_name
            )

        pm.setParent(self._organization_name, menu=True)

        self.main_menu = pm.menuItem(
            parent=self._organization_name,
            subMenu=True,
            tearOff=True,
            docTag=self._application_name,
            allowOptionBoxes=True,
            label=self._menu_name,
            annotation=self._applciation_description
        )

        pm.setParent(self.main_menu, menu=True)

        # Create sub-menus
        self.create_rigging_submenu()
        self.create_skinning_submenu()

        pm.setParent(self.main_menu, menu=True)
        pm.menuItem(divider=True)

        self.create_misc_submenu()

        pm.setParent(self.main_menu, menu=True)
        pm.menuItem(divider=True)

        # Add Shelf menu item
        self.create_shelf_menu()

        pm.setParent(self.main_menu, menu=True)
        pm.menuItem(divider=True)

        # Add Help menu item
        self.create_help_menu()


    def check_for_existing_menu(self, menu_name, docTag=''):
        """
        This method checks if the specified menu already exists.

        :param menu_name: ``str`` that is the name of the menu to check for.
        :param docTag: ``str`` to check if the menu has the docTag for.
        :return: ``bool`` indicating if the menu already exists.
        """

        menu_exists = pm.menu(menu_name, exists=True)

        if menu_exists and docTag and \
            pm.menu(menu_name, q=True, docTag=True) == docTag:
                return True
        else:
            return False


    def create_rigging_submenu(self):
        """This method creates the Rigging sub-menu entry."""

        rigging_submenu = pm.menuItem(
            parent=self.main_menu,
            subMenu=True,
            tearOff=True,
            label='Rigging',
            annotation='Rigging utilities'
        )

        pm.setParent(rigging_submenu, menu=True)
        pm.menuItem(divider=True)

        pm.menuItem(
            'Rename object(s)',
            command=lambda _: renamer.RenameNodes(),
            annotation='Search/Replace/Prefix/Suffix selected object(s) names.'
        )

        pm.menuItem(
            'Create intermediate parent',
            command=lambda _: group_freeze.GroupFreeze.groupFreeze(),
            annotation='Creates an intermediate parent object that holds the'
                       'current object\'s transformations.'
        )

        pm.menuItem(
            'Create null group(s)',
            command=lambda _: create_nulls.create_null_groups(),
            annotation='Creates empty group(s) at the selected object(s) '
                       'locations.'
        )
        pm.menuItem(
            'Create null group(s) with custom prefix',
            command=lambda _: create_nulls.create_null_groups_with_prefix(),
            optionBox=True,
            annotation='Enter a custom prefix for the empty group(s) to be created.'
        )

        pm.menuItem(
            'Find selection center',
            command=lambda _: selections.get_selection_center()
        )

        pm.menuItem(divider=True, dividerLabel='Controls')

        pm.menuItem(
            'Create Control Shapes',
            command=lambda _: control_shapes_creator.ControlShapeCreator(),
            annotation='Create custom control shapes to be used for rigs.'
        )

        pm.menuItem(
            'Change colour',
            command=lambda _: change_color.ChangeColorDialog(),
            annotation='Change colours of the selected object(s)'
        )

        pm.menuItem(divider=True, dividerLabel='Deformers')

        pm.menuItem(
            'Create Deformers',
            command=lambda _: deformer_creator.DeformerCreator(),
            annotation='Create deformer objects (e.g. bones) to be used for rigs.'
        )

        pm.menuItem(
            'Orient Deformers',
            command=lambda _: deformer_orienter.DeformerOrienter(),
            annotation='Orient deformer objects (e.g. bones).'
        )

        pm.menuItem(
            'Symmetrize Blendshapes',
            command=lambda _: blendshape_symmetrizer.BlendshapeSymmetrizer(),
            annotation='Perform symmetrical operations on blendshapes.'
        )

        pm.menuItem(
            'Apply Tension',
            command=lambda _: TensionSetup().applyTension(),
            annotation='Applies a tension deformer setup to the selected mesh(es).'
        )


    def create_skinning_submenu(self):
        """This method creates the Skinning sub-menu entry."""

        skinning_submenu = pm.menuItem(
            parent=self.main_menu,
            subMenu=True,
            tearOff=True,
            label='Skinning',
            annotation='Skinning utilities'
        )

        pm.setParent(skinning_submenu, menu=True)
        pm.menuItem(divider=True)

        pm.menuItem(
            'Reset skinned joints',
            command=lambda _: reset_jnts.resetSkinnedJoints(),
            annotation='Resets the skinCluster deformations for the selected '
                       'joint(s).'
        )


    def create_misc_submenu(self):
        """This method creates the Miscellaneous sub-menu entry."""

        misc_submenu = pm.menuItem(
            parent=self.main_menu,
            subMenu=True,
            tearOff=True,
            label='Misc.',
            annotation='Misc. utilities for developers.'
        )

        pm.setParent(misc_submenu, menu=True)
        pm.menuItem(divider=True)

        pm.menuItem(
            'Reload toolkit from source',
            command=lambda _: reloader.Reloader(),
            annotation='Reload all Python source files in the toolkit.'
        )

        pm.menuItem(
            'Reload all toolkit plugins',
            command=lambda _: reload_all_plugins(),
            annotation='Unloads and then reloads all compiled plug-ins in the toolkit.'
        )

        pm.menuItem(
            'Unload all toolkit plugins',
            command=lambda _: unload_rig_tools_plugins(),
            annotation='Unloads all compiled plug-ins in the toolkit.'
        )

        pm.menuItem(
            'Connect pydevd Debugger',
            command=lambda _: debug.setup_debugger(),
            annotation='Open a port for pydevd to connect to for remote debugging purposes.'
        )

        pm.menuItem(
            'Compile .ui file to .py',
            command=lambda _: compileQt.ConvertUiToPy(),
            annotation='Convert a Qt Designer .ui file to a .py file.'
        )

        pm.menuItem(
            'Remove all helper objects',
            command=lambda _: helpers.remove_all_helpers_from_scene(),
            annotation='Deletes all rigging helper objects in the scene.'
        )

        generate_completions_submenu = pm.menuItem(
            parent=misc_submenu,
            subMenu=True,
            tearOff=True,
            label='Generation utilities',
            annotation='Misc. utilties for generating Maya API auto-completions.'
        )

        pm.setParent(generate_completions_submenu, menu=True)
        pm.menuItem(divider=True)

        pm.menuItem(
            'Generate OM2 completion data',
            command=lambda _: generate_om2.generate_autocompletions(),
            annotation='Generate Maya Python API 2.0 autocompletion data for use in IDEs.'
        )

        pm.menuItem(
            'Generate OM1 completion data',
            command=lambda _: generate_om.generate_autocompletions(),
            annotation='Generate Maya Python API 1.0 autocompletion data for use in IDEs.'
        )


    def create_shelf_menu(self):
        """This method creates the Shelf menu entry."""

        pm.menuItem(
            parent=self.main_menu,
            label='Display Shelf',
            annotation='Show tools shelf',
            command=lambda _: shelf.RigToolsShelf()
        )


    def create_help_menu(self):
        """This method creates the Help menu entry."""
        help_submenu = pm.menuItem(
            parent=self.main_menu,
            subMenu=True,
            tearOff=True,
            label='Help',
            annotation='Documentation and other help utilities.'
        )

        pm.setParent(help_submenu, menu=True)
        pm.menuItem(divider=True)

        pm.menuItem(
            'About',
            command=lambda _: help_menu.view_about_info(),
            annotation='View basic information about the Rigging toolkit.'
        )

        pm.menuItem(
            'View documentation',
            command=lambda _: help_menu.view_documentation(),
            annotation='View the documentation for the Rigging toolkit.'
        )

        pm.menuItem(
            'Open Maya Developer documentation',
            command=lambda _: help_menu.open_maya_developer_documentation(),
            annotation='Quick shortcut to the Maya developer documentation.'
        )
        pm.menuItem(
            'Set location of Maya developer documentation locally',
            command=lambda _: help_menu.set_maya_developer_documentation_path(),
            optionBox=True,
            annotation='Allows for setting shortcut to a local copy of the '
                       'developer documentation.'
        )