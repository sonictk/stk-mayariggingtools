#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module plugin: This module contains utility functions for working with plugins
from this toolkit.
"""
import logging
import os
import pymel.core as pm


def get_plugin_binary_directory():
    """
    This function find the directory containing binaries for plug-ins related to
    this toolkit.
    """
    logger = logging.getLogger(__name__)

    plugins_directory = os.path.join(
        os.path.dirname(
            os.path.dirname(
                os.path.dirname(
                    os.path.dirname(__file__)
                )
            )
        ),
        'plug-ins', 'bin'
    )
    if not os.path.isdir(plugins_directory):
        logger.error(
            'Unable to find the plug-ins directory: {0}! Plugins will '
            'not be loaded and some functionality might be missing!!'
            .format(plugins_directory))
        return

    return plugins_directory


def load_rig_tools_plugins():
    """
    This function loads all available compiled plug-ins that are associated with
    this toolkit.
    """
    logger = logging.getLogger(__name__)

    plugins_directory = get_plugin_binary_directory()

    [pm.loadPlugin(os.path.join(plugins_directory, plugin_filename))
     for plugin_filename in os.listdir(plugins_directory)]

    logger.info('Plugins successfully loaded!')


def unload_rig_tools_plugins():
    """
    This function calls the un-registration functions for all rigTools-related
    plugins and handles cleanup for all of them.
    """
    logger = logging.getLogger(__name__)

    plugins_directory = get_plugin_binary_directory()
    try:
        [pm.unloadPlugin(os.path.splitext(plugin_filename)[0])
         for plugin_filename in os.listdir(plugins_directory)]
    except RuntimeError as err:
        if 'cannot be unloaded because it is still in use' in err.message:
            logger.warning('Plug-ins detected still in use! Creating new scene...')
            pm.newFile(force=True)
            [pm.unloadPlugin(os.path.splitext(plugin_filename)[0])
             for plugin_filename in os.listdir(plugins_directory)]
        else:
            raise RuntimeError(err)
    logger.info('Plugins successfully unloaded!')


def reload_all_plugins():
    """This convenience function reloads all compiled plugins in this toolkit."""
    unload_rig_tools_plugins()
    load_rig_tools_plugins()