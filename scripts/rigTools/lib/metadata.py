#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module metadata: This module contains methods for dealing with metadata on
objects in Maya.
"""
import logging

import maya.OpenMaya as om

from rigTools.lib.api.globals import APIPlug
from rigTools.lib.system import Singleton


def add_metadata_to_node(
        dg_node,
        attribute_name,
        attribute_value,
        fn_dg_node=None,
        writable=False,
        hidden=True,
        keyable=False
):
    """
    This function adds an item of metadata to a given dependency graph node.

    :param dg_node: ``MObject`` that is the object to add the metadata info to.

    :param attribute_name: ``str`` that is the name of the metadata attribute to
        add to the object.

    :param attribute_value: ``str``or ``int`` or ``float`` or ``bool`` value to
        be added to the metadata value.

    :param fn_dg_node: ``MFnDependencyNode``

    :param writable: ``bool`` determining if the attribute can be written to.

    :param hidden: ``bool`` determining if the attribute should be hidden from
        the UI. **Currently not used**

    :param keyable: ``bool`` determining if the attribute is keyable.

    :return:
    """

    logger = logging.getLogger(__name__)


    def get_attribute_type(value):
        """
        This method returns the type of the attribute given.

        :param value:
        :return:
        """

        # Check the type of attribute value to set
        if isinstance(attribute_value, int):
            attribute_type = om.MFnNumericData.kInt

        elif isinstance(attribute_value, float):
            attribute_type = om.MFnNumericData.kFloat

        elif isinstance(attribute_value, bool):
            attribute_type = om.MFnNumericData.kBoolean

        elif isinstance(attribute_value, str) or \
                isinstance(attribute_value, unicode):
            attribute_type = om.MFnData.kString

        else:
            logger.error('### Attribute: {0} of type: {1} is not supported by'
                         'this command!!!'
                         .format(attribute_value, type(attribute_value)))
            raise AttributeError

        return attribute_type


    def set_plug_value(dg_modifier, plug, value):
        """
        This method sets the given plug to a new value after automatically
        determining the value's type.

        :param dg_modifier:
        :param value:
        :return:
        """

        attribute_type = get_attribute_type(value)

        if attribute_type == om.MFnNumericData.kInt:
            dg_modifier.newPlugValueInt(plug, attribute_value)

        elif attribute_type == om.MFnNumericData.kFloat:
            dg_modifier.newPlugValueFloat(plug, attribute_value)

        elif attribute_type == om.MFnNumericData.kBoolean:
            dg_modifier.newPlugValueBool(plug, attribute_value)

        elif attribute_type == om.MFnData.kString:
            dg_modifier.newPlugValueString(plug, attribute_value)

        else:
            logger.error('### Attribute: {0} of type: {1} is not supported by'
                         'this command!!!'
                         .format(attribute_value, type(attribute_value)))
            raise AttributeError

        logger.debug('Successfully set attribute: {0} of type: {1} to: {2}!'
                     .format(plug.name(), attribute_type, attribute_value))


    if not fn_dg_node:
        fn_dg_node = om.MFnDagNode(dg_node)

    dg_modifier = om.MDGModifier()

    # Check if the attribute already exists and set the value directly instead
    if fn_dg_node.hasAttribute(attribute_name):
        plug = APIPlug(dg_node, fn_dg_node.attribute(attribute_name))

    else:
        # Create the new attribute if it does not exist
        fn_attribute = om.MFnTypedAttribute()

        attribute_type = get_attribute_type(attribute_value)

        new_attribute = fn_attribute.create(
            attribute_name,
            attribute_name,
            attribute_type
        )

        dg_modifier.addAttribute(dg_node, new_attribute)
        plug = APIPlug(dg_node, new_attribute)

    set_plug_value(dg_modifier, plug, attribute_value)

    # Commit changes to attribute values
    dg_modifier.doIt()

    if not writable:
        plug.setLocked(True)

    if not keyable:
        plug.setKeyable(False)

    if hidden:
        plug.setChannelBox(False)


class MetadataConstants(object):
    """
    This class contains methods and objects related to Maya metadata for this
    plugin that are intended to remain globally constant throughout any
    invocation of this entire package.
    """
    __metaclass__ = Singleton

    def __init__(self):
        """The constructor."""
        self._helper_attribute_name = 'isRigToolsHelper'


    @property
    def helper_attribute_name(self):
        """
        This is the name of the metadata attribute that all helpers will have.

        :return: ``str`` name.
        """
        return self._helper_attribute_name