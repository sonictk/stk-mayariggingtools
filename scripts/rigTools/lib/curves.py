#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module curves: This module contains methods for working with NURBS Curve
objects in Maya.
"""
import logging
import traceback
import maya.api.OpenMaya as om2
from rigTools.lib.node import NodeColor


def add_curve_node(
    points,
    name='',
    parent=None,
    parent_object=None,
    closed=False,
    degree=3,
    matrix=None,
    color=(0,0,0)
):
    """
    This method creates a NURBS curve object in Maya.

    :param color: ``tuple`` of RGB colour values that will be the color of
            the created shape.

    :param name: ``str`` that will be the name of the curve node.

    :param parent_object: ``MObject`` Parent object to parent the curve as a
        transform to.

    :param parent: ``MObject`` Parent object for curve shape.

    :param points: ``MPointArray`` of positions in a single-dimension array.

    :param closed: ``bool`` indicating if the curve is closed.

    :param degree: ``int`` indicating the NURBS degree of the curve.

    :param matrix: ``MTransformationMatrix`` to set the global transformation
        of the curve.

    :return: ``MFnNurbsCurve`` curve object.

    :rtype: ``MFnNurbsCurve``
    """

    logger = logging.getLogger(__name__)

    knots = range(len(points) + degree - 1)

    # Get and set arguments to pass to the curve constructor.
    if not matrix:
        matrix = om2.MTransformationMatrix()

    # Account for closed/open curves
    if closed:
        for pt in points[:degree]:
            points.append(pt)

        knots = range(len(points) + degree -1)

         # kClosed
        form = om2.MFnNurbsCurve.kPeriodic
    else:
        # kOpen
        form = om2.MFnNurbsCurve.kOpen

    if not parent:
        parent = om2.MObject()

    # Create the curve object
    fn_curve = om2.MFnNurbsCurve()

    try:
        curve_object = fn_curve.create(
            points,
            knots,
            degree,
            form,
            False,
            True,
            parent
        )

    except RuntimeError:
        logger.error('### Failed to create the curve!!!\n{0}'.format(traceback.print_exc()))
        raise RuntimeError

    # Rename the node
    dg_modifier_fn = om2.MDGModifier()
    dg_modifier_fn.renameNode(curve_object, name)
    dg_modifier_fn.doIt()

    # Set the colour of the node
    node_list = om2.MSelectionList().add(fn_curve.getPath())
    NodeColor().set_nodes_color(color=color, nodes=node_list)

    # Parent to DAG node if specified
    if parent_object:
        parent_object_dagnode = om2.MFnDagNode(parent_object)
        parent_object_dagnode.addChild(curve_object, om2.MFnDagNode.kNextPos, False)

    # Set the transformation for the new curve
    node_transform = om2.MFnTransform(curve_object)

    try:
        node_transform.setTransformation(matrix)
    except TypeError:
        logger.error('### {0} is not a valid MTransformationMatrix object!!!'
                     .format(matrix))
        raise TypeError

    return curve_object

