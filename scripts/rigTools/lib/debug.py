#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module debug: This module contains methods for working with debugging Maya."""
import logging
import os
import sys

from rigTools.lib.system import Constants


def setup_debugger(host='localhost', port=3320, suspend=False):
    """
    This method sets up Maya for remote debugging via pydevd.

    :param host: ``str`` that is the name of the local host to attempt debug
        connection with.

    :param port: ``int`` that the debugger will attempt to connect on. This port
        should be the currently open listening port in Maya.

    :param suspend: ``bool`` that determines if the remotely debugged application
        should be suspended on successful connection of the debugger.

    :return: ``None``
    """

    logger = logging.getLogger(__name__)
    logger.debug('Adding PyCharm helpers to the PYTHONPATH...')

    pycharm_helper_path = Constants().pydev_helper_path

    if not pycharm_helper_path:
        logger.error(
            '### Unable to determine the location of the pydevd helper files!!!'
            'Please ensure that the environment variable PYDEV_HELPERS_PATH is '
            'pointing to the correct location on disk!!!'
        )
        return

    elif not os.path.isdir(pycharm_helper_path):
        logger.error('# PyCharm helpers do not exist at: {0}!'
                            .format(pycharm_helper_path))
        return

    if pycharm_helper_path not in sys.path:
        sys.path.append(pycharm_helper_path)

    try: import pydevd
    except ImportError as err:
        raise ImportError('Unable to import debugging module!\n\n{0}'.format(err))
    pydevd.settrace(host, port=port, suspend=suspend)