#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module undo: For PyCharm"""
import logging
import traceback

from pymel import core as pm


class UndoManager(object):
    """
    This class contains methods for working with the undo queue in Maya.
    """

    logger = logging.getLogger(__name__)

    @classmethod
    def undoable(cls, function):
        """
        A decorator that will make commands undoable in Maya.

        :param function: ``object`` function that would be set to undoable.
        :return: ``object`` decorator.
        """

        def decorator(*args, **kwargs):
            """
            This method is returned as a wrapper for the function that is
            passed into it.
            """

            # Open the undo queue
            pm.undoInfo(openChunk=True)

            # Clear current item that will be returned
            functionReturn = None

            # See if method to be passed in works
            try:
                functionReturn = function(*args, **kwargs)
            except Exception:
                logging.error('### Failed to execute method!!!\n{0}'
                              .format(traceback.print_exc()))
            finally:
                # close the undo queue and return the function
                pm.undoInfo(closeChunk=True)
                return functionReturn

        return decorator


    @classmethod
    def repeatable(cls, function):
        """
        A decorator that will make functions repeatable.

        :param function: ``object`` function that would be set to repeatable.
        :return: ``object`` decorator.
        """

        def decorator(*args, **kwargs):
            """
            This method is returned as a wrapper for the function that is
            passed into it.
            """

            argString=''

            if args:
                for each in args:
                    argString+= str(each)+', '

            if kwargs:
                for key, item in kwargs.iteritems():
                    argString += str(key)+'='+str(item)+', '

            # Format MEL command: -ac is the command, -acl is a label for it
            #  python("awesomeTools.myAwesomeFunction(valueA=\"totalyWickedMode\"")
            # __name__ of this module and then __name__ of this function passed in
            commandStr = 'python("'+__name__+'.'+function.__name__+'('+argString+')")'

            functionReturn = function(*args, **kwargs)

            try: pm.repeatLast( ac=commandStr, acl=function.__name__ )
            except Exception: pass

            return functionReturn

        return decorator