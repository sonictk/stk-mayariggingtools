#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module test: This module contains useful objects and functions for working
with unit tests in this toolkit.
"""
import itertools
import logging
import os
import pymel.core as pm
import tempfile
import time
import unittest
import uuid
from rigTools.lib.system import Constants


def run_toolkit_tests(directories=None,
                      test='',
                      test_suite=None,
                      verbosity=3):
    """
    This function runs the unit tests for the toolkit.

    :param directories: ``list`` of ``str`` paths containing tests to run. If no
        paths are specified, will run all available tests in the toolkit.

    :param test: ``str`` name of a specific test to run. If specified, overrides
        all other tests to run and will *only* run this specific test.

    :param test_suite: ``TestSuite`` to use for running the tests. If not
        specified, a default ``TestSuite`` wil be generated.

    :param verbosity: ``int`` Determines the level of logging to be output during
        running of the tests. Levels are inclusive of previous levels (i.e.
        choosing to log warnings will also log errors as well)

            - ``0`` : Errors only.
            - ``1`` : Show warnings.
            - ``2`` : Show informational messages.
            - ``3`` : Show debug messages.
    """
    # Check if maya standalone session can be initialized and initialize it
    try:
        import maya.standalone
        maya.standalone.initialize()

    except ImportError:
        raise RuntimeError('You are currently not in a valid mayapy interpreter '
                           'session! Unable to initialize standalone Maya session!')

    if verbosity == 3:
        log_level = logging.DEBUG
    elif verbosity == 2:
        log_level = logging.INFO
    elif verbosity == 1:
        log_level = logging.WARNING
    elif verbosity == 0:
        log_level = logging.CRITICAL
    else:
        raise ValueError('Invalid verbosity level specified!')
    logger = logging.getLogger(__name__)
    logger.setLevel(log_level)

    loader = unittest.TestLoader()

    if not test_suite:
        test_suite = unittest.TestSuite()

    if test != '':
        # Attempt to locate the test that was specified either by absolute
        # filename or name and add it
        test_found = False
        for root, dirnames, filenames in os.walk(Constants().toolkit_path):
            for f in filenames:
                if test == f or test == os.path.splitext(f)[0]:
                    test_suite.addTest(
                        loader.discover(start_dir=root, pattern=f)
                    )
                    test_found = True
                    break

        if test_found is False:
            logger.error('No test could be found with the given name: {0}!'.format(test))
            return
    elif directories:
        for d in directories:
            if os.path.isdir(d):
                test_suite.addTests(loader.discover(d))
    else:
        # If no directories (or specific test) are specified explicitly,
        # add all tests recursively that can be found in the toolkit.
        test_suite.addTests(loader.discover(Constants().toolkit_path))

    test_runner = MayaTextTestRunner(verbosity=verbosity, resultclass=MayaTestResult)
    test_runner.run(test_suite)

    # NOTE: Calling ``unitialize()`` here prevents an exception from being raised
    # in 2016 onwards
    if pm.about(v=True) >= 2016.0:
        maya.standalone.uninitialize()


class MayaTestResult(unittest.TextTestResult):
    """
    Customize the test result so we can do things like do a file new between
    each test and suppress script editor output.
    """

    def __init__(self, stream, descriptions, verbosity):
        """
        The constructor.

        :param stream (sys.stderr):
        :param descriptions (bool): Defaults to ``True``.
        :param verbosity (int):
        """
        super(MayaTestResult, self).__init__(stream, descriptions, verbosity)
        self.test_numbers = itertools.count(1)
        self.test_start_time = 0.0
        self.verbosity = verbosity

    def startTestRun(self):
        """Called before any tests are run."""
        # Suppress the script editor logging (except for errors)
        pm.scriptEditorInfo(suppressInfo=True,
                            suppressResults=True,
                            suppressWarnings=True,
                            suppressErrors=True)
        logging.disable(logging.CRITICAL)
        super(MayaTestResult, self).startTestRun()

    def stopTestRun(self):
        """Called after all tests are run."""
        logging.disable(logging.NOTSET)
        # Restore script editor logging
        pm.scriptEditorInfo(suppressInfo=False,
                            suppressResults=False,
                            suppressWarnings=False,
                            suppressErrors=False)

        return super(MayaTestResult, self).stopTestRun()

    def startTest(self, test):
        """
        Called before an individual test is run. This writes the test number to
        the stream in order to indicate the progress.

        :param test: ``TestCase`` that is about to run.
        """
        self.test_start_time = time.time()
        if self.showAll and self.test_cases_count:
            progress = '[{0}/{1}] '.format(next(self.test_numbers), self.test_cases_count)
            self.stream.write(progress)

            # Also store the progress in the test itself, so that if it errors,
            # it can be written to the exception information by our overridden
            # _exec_info_to_string method:
            test.progress_index = progress

        return super(MayaTestResult, self).startTest(test)

    def _exc_info_to_string(self, err, test):
        """
        Get an exception info string and prepend the test number in the event
        of a test failure, along with printing useful information about the stack
        at the time of execution.

        :param err:
        :param test:
        """
        info = super(MayaTestResult, self)._exc_info_to_string(err, test)
        # TODO: print info about the stack and the locals/globals in the test
        if self.showAll:
            info = 'Test number: {idx}\n{info}'\
                .format(idx=test.progress_index, info=info)

        return info

    def stopTest(self, test):
        """
        Called after an individual test is run. Re-initializes Maya to a new
        scene file.

        :param test: ``TestCase`` that just ran.
        """
        self.stream.write('Execution time: {0:.3f}\n'.format(
            time.time() - self.test_start_time
        ))
        # TODO: Should also unregister any previously registered callbacks
        pm.newFile(force=True)
        return super(MayaTestResult, self).stopTest(test)


class MayaTextTestRunner(unittest.TextTestRunner):
    """
    This is a test runner that supports numbering of test cases for running
    Maya unit tests.
    """
    resultclass = MayaTestResult

    def run(self, test):
        """
        This method helps to store the total number of test cases before running.

        :param test:
        """
        self.test_cases_count = test.countTestCases()
        return super(MayaTextTestRunner, self).run(test)

    def _makeResult(self):
        """
        Creates and returns a result instance that has the count of total test cases.
        """
        result = super(MayaTextTestRunner, self)._makeResult()
        result.test_cases_count = self.test_cases_count
        return result


class MayaTestCase(unittest.TestCase):
    """
    Base class for unit test cases run in Maya.

    Tests do not have to inherit from this ``TestCase`` but this derived
    TestCase contains convenience functions to load/unload plug-ins and clean up
    temporary files.
    """
    # Keep track of all temporary files that were created so they can be cleaned
    # up after all tests have been run
    files_created = []

    # Keep track of which plugins were loaded so we can unload them after all
    # tests have been run
    plugins_loaded = set()

    @classmethod
    def tearDownClass(cls):
        super(MayaTestCase, cls).tearDownClass()
        cls.delete_temp_files()
        cls.unload_plugins()

    @classmethod
    def load_plugin(cls, plugin):
        """
        Load the given plug-in and saves it to be unloaded when the unit test
        is finished.

        @param plugin: Plug-in name.
        """
        pm.loadPlugin(plugin, qt=True)
        cls.plugins_loaded.add(plugin)

    @classmethod
    def unload_plugins(cls):
        # Unload any plugins that this test case loaded
        for plugin in cls.plugins_loaded:
            pm.unloadPlugin(plugin)
        cls.plugins_loaded = []

    @classmethod
    def delete_temp_files(cls):
        """Delete the temp files in the cache and clear the cache."""
        [os.remove(f) for f in cls.files_created if os.path.isfile(f)]
        cls.files_create = []

    @classmethod
    def get_temp_filename(cls, file_name):
        """
        Get a unique filepath name in the testing directory.

        The file will not be created, that is up to the caller. This file
        will be deleted when the tests are finished.

        @param file_name: A partial path ex: 'directory/somefile.txt'

        @return The full path to the temporary file.
        """
        temp_dir = os.path.join(tempfile.gettempdir(), 'mayaunittest', str(uuid.uuid4()))
        if not os.path.exists(temp_dir):
            os.makedirs(temp_dir)
        base_name, ext = os.path.splitext(file_name)
        path = os.path.join(temp_dir, base_name + '.' + ext)
        count = 0
        while os.path.exists(path):
            # If the file already exists, add an incremented number
            count += 1
            path = os.path.join(temp_dir, base_name + count + '.' + ext)
        cls.files_created.append(path)

        return path


