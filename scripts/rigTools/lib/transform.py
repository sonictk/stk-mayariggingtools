#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module transform: This module contains useful functions for working with
transformations and matrices in Maya.
"""
from rigTools.lib.api.datatypes import APIFnMatrixData
from rigTools.lib.api.globals import APIFnDGNode, APIPlug


def get_world_matrix_of_node(node):
    """
    This function retrieves the world-space transformation matrix of a given
    **node**.

    :param node: ``MObject`` The node to retrieve the transformation matrix for.

    :return: ``MMatrix`` The world-space transformation matrix of the object.
    """
    # TODO: should fallback to multiplying by parents' inverse object mat44s
    # if the 'worldMatrix' attribute cannot be found
    fn_node = APIFnDGNode(node)
    world_matrix_attr = fn_node.attribute('worldMatrix')

    matrix_node = APIPlug(node, world_matrix_attr).elementByLogicalIndex(0)\
        .asMObject()

    return APIFnMatrixData(matrix_node).transformation().asMatrix()


def get_inverse_world_matrix_of_node(node):
    """
    This function retrieves the world-space transformation matrix of a given
    **node**.

    :param node: ``MObject`` The node to retrieve the transformation matrix for.

    :return: ``MMatrix`` The world-space transformation matrix of the object.
    """
    # TODO: should fallback to multiplying by parents' inverse object mat44s
    # if the 'worldMatrix' attribute cannot be found
    fn_node = APIFnDGNode(node)
    world_matrix_attr = fn_node.attribute('worldInverseMatrix')

    matrix_node = APIPlug(node, world_matrix_attr).elementByLogicalIndex(0)\
        .asMObject()

    return APIFnMatrixData(matrix_node).transformation().asMatrix()