#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module test_get_api_object_from_name: This is a unit test."""
import maya.OpenMaya as om
import pymel.core as pm
from unittest import TestCase
from rigTools.lib.api.globals import get_api_object_from_name

class TestGet_api_object_from_name(TestCase):
    def setUp(self):
        self.test_node = pm.createNode('transform', name='test_node')

    def test_get_api_object_from_name(self):
        result = get_api_object_from_name(self.test_node.name(), as_dag_path=True)
        self.assertIsInstance(result, om.MDagPath)
        self.assertTrue(result.hasFn(om.MFn.kTransform),
                        'The returned object was of invalid type!')
