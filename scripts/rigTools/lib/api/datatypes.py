#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module datatypes: This module contains additional datatypes for use. """

import maya.OpenMaya as om


class APIPointArray(om.MPointArray):
    """
    This class implements an array of MPoints.
    """

    def __init__(self, *args):
        super(APIPointArray, self).__init__(*args)


class APIDoubleArray(om.MDoubleArray):
    def __init__(self, *args):
        super(APIDoubleArray, self).__init__(*args)


class APIFnMatrixData(om.MFnMatrixData):

    def __init__(self, *args):
        super(APIFnMatrixData, self).__init__(*args)
