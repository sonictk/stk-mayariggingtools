#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module api_anim: This module contains custom API bindings for the Maya Python API.
"""

import maya.OpenMayaAnim as om_anim

class APIFnSkinCluster(om_anim.MFnSkinCluster):
    def __init__(self, *args):
        super(APIFnSkinCluster, self).__init__(*args)


class APIIkJoint(om_anim.MFnIkJoint):
    def __init__(self, *args):
        super(APIIkJoint, self).__init__(*args)


