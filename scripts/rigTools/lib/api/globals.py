#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module api: This module contains custom API bindings for the Maya Python API.
"""

import maya.OpenMaya as om
import maya.api.OpenMaya as om2


class APIEventMessage(om2.MEventMessage):
    """
    Class used to register callbacks for event related messages.
    """

    def __init__(self, *args):
        super(APIEventMessage, self).__init__(*args)


class APIFileIO(om.MFileIO):
    """I/O operations on scene files. """

    def __init__(self, *args):
        super(APIFileIO, self).__init__(*args)


class APIDGModifier(om.MDGModifier):
    """
    Dependency graph modifier.

    An MDGModifier is used to change the structure of the dependency graph. This
    includes adding nodes, making new connections, and removing existing
    connections. To perform operations using an MDGModifier, register all of
    the changes that are to be made and then call the doIt method to make the
    changes. Undo is provided through the undoIt method.
    """

    def __init__(self, *args):
        super(APIDGModifier, self).__init__(*args)


class APIGlobal(om.MGlobal):
    """
    Static class providing common API global functions.
    """
    def __init__(self, *args):
        """The constructor."""
        super(APIGlobal, self).__init__(*args)


class APIFn(om.MFn):
    """
    MFn encapsulates all API Function Set type identifiers used for RTTI in
    the API.
    """

    def __init__(self, *args):
        super(APIFn, self).__init__(*args)


class APIFnNurbsCurve(om.MFnNurbsCurve):
    def __init__(self, *args):
        super(APIFnNurbsCurve, self).__init__(*args)


class APIFnMesh(om.MFnMesh):
    """
    Polygonal surface function set.

    This function set provides access to polygonal meshes. Objects of type
    MFn::kMesh, MFn::kMeshData, and MFn::kMeshGeom are supported.
    MFn::kMesh objects are shapes in the DAG, MFn::kMeshGeom objects are the
    raw geometry that the shapes use, and MFn::kMeshData objects are the data
    that is passed through dependency graph connections.
    """

    def __init__(self, *args):
        super(APIFnMesh, self).__init__(*args)


class APIFnTransform(om.MFnTransform):
    def __init__(self, *args):
        super(APIFnTransform, self).__init__(*args)


class APISelectionList(om.MSelectionList):
    """
    This class implements a list of objects.
    """

    def __init__(self, *args):
        super(APISelectionList, self).__init__(*args)


    @property
    def selection_strings(self):
        """
        This class property returns a ``list`` of ``str`` names of the items in
        the selection list.
        """
        string_list = []
        self.getSelectionStrings(string_list)

        return string_list


class APIItDependencyGraph(om.MItDependencyGraph):
    def __init__(self, *args):
        super(APIItDependencyGraph, self).__init__(*args)


class APIItSelectionList(om.MItSelectionList):
    """
    Class for iterating over the items in an MSelection list. A filter can be
    specified so that only those items of interest on a selection list can be
    obtained.
    """

    def __init__(self, *args):
        super(APIItSelectionList, self).__init__(*args)


class APIObject(om.MObject):
    """
    Generic Class for Accessing Internal Maya Objects.
    """
    def __init__(self, *args):
        """The constructor."""
        super(APIObject, self).__init__(*args)


class APIPlug(om.MPlug):
    """Create and Access dependency node plugs."""
    def __init__(self, *args):
        super(APIPlug, self).__init__(*args)


class APIDagPath(om.MDagPath):
    """
    Provides methods for obtaining one or all Paths to a specified DAG Node,
    determining if a Path is valid and if two Paths are equivalent, obtaining
    the length, transform, and inclusive and exclusive matrices of a Path, as
    well as performing Path to Path assignment.
    """

    def __init__(self, *args):
        super(APIDagPath, self).__init__(*args)


class APIDagPathArray(om.MDagPathArray):
    def __init__(self, *args):
        super(APIDagPathArray, self).__init__(*args)


class APIFnDagNode(om.MFnDagNode):
    """
    DAG Node Function Set.

    Provides methods for attaching Function Sets to, querying, and adding
    children to DAG Nodes. Particularly useful when used in conjunction with
    the DAG Iterator class (MItDag).
    """

    def __init__(self, *args):
        super(APIFnDagNode, self).__init__(*args)


class APIPlugArray(om.MPlugArray):
    """
    This class implements an array of MPlugs.
    """
    def __init__(self, *args):
        super(APIPlugArray, self).__init__(*args)


class APIFnSingleIndexedComponent(om.MFnSingleIndexedComponent):
    def __init__(self, *args):
        super(APIFnSingleIndexedComponent, self).__init__(*args)


class APIFnDGNode(om.MFnDependencyNode):
    """
    Dependency node function set.

    MFnDependencyNode allows the creation and manipulation of dependency graph
    nodes. Traversal of the dependency graph is possible using the
    getConnections method.
    """

    def __init__(self, *args):
        super(APIFnDGNode, self).__init__(*args)


    @classmethod
    def get_connections(self):
        """
        Get all of the current connections to this node as an array of plugs.

        :return: ``APIPlugArray``
        """
        plugs = APIPlugArray()
        self.getConnections(plugs)

        return plugs


class APITransformationMatrix(om.MTransformationMatrix):
    def __init__(self, *args):
        super(APITransformationMatrix, self).__init__(*args)


class APIQuaternion(om.MQuaternion):
    """
    Quaternion math.
    """
    def __init__(self, *args):
        super(APIQuaternion, self).__init__(*args)


    @staticmethod
    def slerp(p, q, t, spin=0):
        """
        This does spherical linear interpolation between two quaternions and
        returns the result as a new quaternion.

        :param p: ``APIQuaternion`` first to blend with
        :param q: ``APIQuaternion`` second to blend with
        :param t: ``float`` between 0.0 and 1.0 as time value to use for blending
        :param spin: ``int`` determining the number of revolutions around the axis
            occurs as p blends towards q.
        :return: ``APIQuaternion``
        """
        om2_p = om2.MQuaternion(*p)
        om2_q = om2.MQuaternion(*q)
        quat_slerped = om2.MQuaternion(om2_p, om2_q, t, spin)

        return APIQuaternion(*quat_slerped)


class APIVector(om.MVector):
    def __init__(self, *args):
        super(APIVector, self).__init__(*args)


def get_api_object_from_name(name, as_dag_path=False):
    """
    This function returns the Maya API object instance from a given **name**.

    :param name: ``str``name of the object to get the API object for.

    :param as_dag_path: ``bool`` If set to ``True``, returns the DAG path instead
        of the ``APIObject``.

    :return: ``MObject``|``MDagPath`` The API object.
    """
    selection_list = APISelectionList()
    selection_list.add(name)

    node = APIObject()
    selection_list.getDependNode(0, node)

    if as_dag_path:
        dag_path = om.MDagPath()
        APIFnDagNode(node).getPath(dag_path)
        return dag_path
    else:
        return node


