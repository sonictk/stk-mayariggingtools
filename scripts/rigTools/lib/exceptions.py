#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
Module exceptions: This module contains custom exceptions that are specifically
used for this toolkit.
"""

import sys

import maya.utils

from rigTools.lib.api.globals import APIGlobal, APIFileIO


def register_custom_exceptions():
    """This function registers the custom ``Exception`` object for this plugin."""
    RigToolsException()


class RigToolsException(Exception):
    """
    This is the base exception that should be raised whenever an error is
    encountered during normal program execution.
    """

    def __init__(self, *args, **kwargs):
        super(RigToolsException, self).__init__(*args, **kwargs)
        self.register()


    def register(self):
        """
        This method registers the custom exception and sets up how Maya will
        format stdout output when printing to the script editor.
        """
        # Monkey-patch stdout/err and Maya's output for how it handles GUI exceptions
        orig_maya_excepthook = maya.utils.formatGuiException
        orig_sys_excepthook = sys.__excepthook__ # Get the original state of excepthook

        # TODO: Filter exceptions only for errors that occur within the plugin

        sys.__excepthook__ = self.except_hook
        maya.utils.formatGuiException = self.except_hook


    def get_environment(self):
        """
        This method obtains information about the current operating environment
        that will be included in the exception message to provide additional
        diagnostic information.

        :return:
        """
        # TODO: Use this in the exception printout

        info = {
            'scene_name' : APIFileIO.currentFile(),
            'file_type' : APIFileIO.fileType(),
            'file_version' : APIFileIO.currentlyReadingFileVersion(),
            'maya_version' : APIGlobal.mayaVersion(),
            'maya_api_version' : APIGlobal.apiVersion(),
            'maya_state' : APIGlobal.mayaState()
        }


    def except_hook(self, exception_type, exception_value, traceback, detail=2):
        """
        This method overrides the default exception printing behaviour to
        format messages written to the ``stdout`` and ``stderr`` streams that
        will include more information by default.

        :param exception_type:
        :param exception_value:
        :param traceback:
        :param detail: ``int``
        :return: ``str`` that is the custom exception message to print.
        """
        environment = self.get_environment()

        exception_message = \
            '\n{separator}\n' \
            'An unhandled exception in the plugin has occurred. Some useful trace ' \
            'information may be found in the following console output.\n' \
            '{separator' \
            'Current scene: {scene_name} {file_type} {file_version}\n' \
            'Maya version: {maya_version} {maya_api_version}\n' \
            '{separator}' \
            '{maya_trace}\n' \
            '{separator\n\n' \
            '\n{separator}\n\n'\
                .format(
                    separator='#' * 79,
                    maya_trace=maya.utils._formatGuiException(
                        exception_type, exception_value, traceback, detail),
                    scene_name=environment.get('scene_name', ''),
                    file_type=environment.get('file_type', ''),
                    file_version=environment.get('file_version', ''),
                    maya_version=environment.get('maya_version', ''),
                    maya_api_version=environment.get('maya_api_version', '')
                )

        return exception_message


class FileNotFoundError(RigToolsException):
    """
    This exception should be raised whenever a filesystem resource fails to be
    found on disk.
    """

    def __init__(self, file_path, message='File not found!'):
        """
        The constructor.

        :param file_path: ``str`` indicating the location of the file that was
            attempted to be retrieved.

        :param message: ``str`` that is the error message to be output.
        """
        super(FileNotFoundError, self).__init__(file_path, message)

        sys.stderr.write('The file resource: {0} was not found on the filesystem!\n\n'
                         .format(file_path))