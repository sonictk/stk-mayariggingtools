#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module ui: This module contains base classes for creating UI widgets in Maya.
"""

import logging
import os
import traceback

import maya.OpenMayaUI as apiUi
import pymel.core as pm
import shiboken
from PySide.QtCore import Qt, QSize, Signal
from PySide.QtGui import QWidget, QToolBar, QMainWindow, QDockWidget, QColor, \
    QColorDialog, QIcon, QSizePolicy, QMessageBox
from maya.app.general.mayaMixin import MayaQWidgetBaseMixin, \
    MayaQWidgetDockableMixin

from rigTools.lib.api.globals import APIGlobal


class MessageTypes(object):
    """These are enums for various types of UI messages from the toolkit."""
    debug = 0
    info = 1
    warning = 2
    error = 3
    critical = 4


def get_maya_window(return_as_type=QWidget):
    """
    Gets the main Maya window as a QtGui.QWidget instance.

    :param return_as_type: ``QWidget`` or ``QMainWindow``, depending on type
        of object to be requested.

    :return: ``object`` instance of the top level Maya window.
    """

    # Create pointer to the Maya mainWindow object through SWIG
    ptr = apiUi.MQtUtil.mainWindow()

    # Return Maya window
    if ptr:
        # noinspection PyUnresolvedReferences
        return shiboken.wrapInstance(long(ptr), return_as_type)


def display_message(message, message_type='info', show_prompt=False, log=False):
    """
    This displays the given **message** within the Maya GUI.

    :param message: ``str`` Message that will be displayed to the user.

    :param message_type: ````str``|``QtGui.QMessagebox.type`` Type of message to
        be displayed.

    :param show_prompt: Setting this to ``True`` will also popup a dialog
            to show the user what the message was about.

    :param log: ``bool`` If set to ``True``, will also write to the logger handler.
            Defaults to ``False``.
    """

    logger = logging.getLogger(__name__)

    if message_type in ('warning', QMessageBox.Warning, MessageTypes.warning):
        APIGlobal.displayWarning(message)
        if show_prompt:
            QMessageBox.warning(get_maya_window(), 'Warning!', message)
        if log:
            logger.warning(message)

    elif message_type in ('error', QMessageBox.Critical, MessageTypes.critical, MessageTypes.error):
        APIGlobal.displayError(message)
        if show_prompt:
            QMessageBox.critical(get_maya_window(), 'Error occurred!', message)
        if log:
            logger.error(message)

    else:
        APIGlobal.displayInfo(message)
        if show_prompt:
            QMessageBox.info(get_maya_window(), 'Notice', message)
        if log:
            logger.info(message)


class _GCProtector(object):
    """
    This class acts as a reference manager to prevent Qt widgets from being
    garbage-collected automatically. It contains a shared state that is
    maintained throughout all instances of itself that are created.
    """
    __shared_state = {}

    def __init__(self):
        """The constructor."""
        self.__dict__ = self.__shared_state

        self.protected_widgets_key = 'protected_widgets'


    @property
    def state(self):
        """
        The current shared state data.

        :return: ``dict`` The shared state of the class.
        """
        return self.__shared_state


    @state.setter
    def state(self, state):
        """
        Set the new shared state for all garbage collection protection.

        :param state: ``dict`` The new shared state that the protector will
            have across all its instances.
        """
        self.__shared_state = state


    @property
    def widgets(self):
        """
        The current widgets that are under garbage collection protection.

        :return: ``list`` The widgets that are being tracked.
        """
        return self.__shared_state.get(self.protected_widgets_key, [])


    def add_widget(self, widget):
        """
        This method adds the given **widget** to garbage collection protection.

        :param widget: ``QWidget`` The widget to protect against garbage
            collection.
        """
        protected_widgets = self.widgets
        protected_widgets.append(widget)
        self.__shared_state[self.protected_widgets_key] = protected_widgets


    def remove_widget(self, widget):
        """
        This method removes the given **widget** from garbage collection
        protection.

        :param widget: ``QWidget`` The widget to remove from garbage collection
            protection.
        """
        protected_widgets = self.widgets

        if not protected_widgets:
            return
        else:
            protected_widgets.remove(widget)
            # Manually schedule the widget for garbage collection by Qt as well
            widget.deleteLater()
            self.__shared_state[self.protected_widgets_key] = protected_widgets


class MayaToolbar(QToolBar):
    """
    This base class creates a custom toolbar widget that is dock-able in Maya.
    """

    def __init__(
        self,
        title,
        actions,
        area=Qt.LeftToolBarArea,
        insert_before=None,
        icon_display_style=Qt.ToolButtonTextUnderIcon,
        parent=None
    ):
        """
        The constructor.

        :param icon_display_style: ``Qt.ToolButtonStyle`` that determines how
            the icons will be displayed in the toolbar.

        :param insert_before: ``str`` that is name of existing Maya toolbar or
            ``QToolBar`` determining which ``QToolBar`` the newly-created
            ``MayaToolBar`` will be inserted before.

        :param area: ``Qt.ToolBarArea`` determining where the toolbar will be
            initially docked. Ignored if ``insert_before`` is used.

        :param actions:  ``list`` of ``tuple`` objects containing either:

            - ``str``
            - ``QIcon``, ``str``
            - ``str``, ``QObject``
            - ``QIcon``, ``str``, ``QObject``

            that will be used to propagate the initial list of actions in the
            toolbar.

        :param title: ``str`` that will be the title of the toolbar.

        :param parent: ``QMainWindow`` object that will be the parent of
            the toolbar. Defaults to the Maya main window.

        :return: ``QToolBar``
        """

        self.logger = logging.getLogger(__name__)

        # Get reference to the Maya main window
        if not parent:
            parent = get_maya_window(QMainWindow)

        # Check if shelf already exists and closes it
        existing_shelf = parent.findChildren(QToolBar, title)

        if existing_shelf:
            for shelf in existing_shelf:
                self.logger.debug('Closing existing shelf {0}...'
                                  .format(shelf.objectName()))

                self.remove_toolbar(shelf)

        # Create the QToolBar instance
        super(MayaToolbar, self).__init__(title, parent)

        self.setObjectName(title)
        self.setToolButtonStyle(icon_display_style)
        self.default_icon_size = QSize(32, 32)
        self.setIconSize(self.default_icon_size)
        self.icon_scale_factor = 0.5
        self.mininum_icon_size = 32
        self.maximum_icon_size = 128
        self.minimumToolbarWidth = 200
        self.minimumToolbarHeight = 48

        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        # TODO: Should move this to global stylesheet
        self.setStyleSheet('QToolBar {background-color: rgb(35, 35, 35)}')

        self.add_actions(actions)

        if insert_before and isinstance(insert_before, str):
            maya_shelves = parent.findChildren(QToolBar)
            insert_before_shelf = None

            for shelf in maya_shelves:
                if insert_before == shelf.windowTitle():
                    insert_before_shelf = shelf
                    break

            if not insert_before_shelf:
                # Fallback to inserting shelf directly (Maya beta)
                self.logger.warning('The specified Maya shelf: {0} could '
                                  'not be found!!!'.format(insert_before))

                parent.addToolBar(area, self)
            else:
                parent.insertToolBar(insert_before_shelf, self)

        else:
            parent.addToolBar(area, self)

        self.logger.debug('Created toolbar: {0}!'.format(self.objectName()))


    def deleteLater(self, *args):
        """Schedules this toolbar for deletion."""
        super(MayaToolbar, self).deleteLater(*args)


    def resizeEvent(self, event):
        """
        This method is re-implemented to allow for custom behaviour of
        resizing the icons to fit the size of the toolbar.
        """
        #TODO: Need to fix min size/height not working
        #TODO: Need to make high-res icons
        super(MayaToolbar, self).resizeEvent(event)

        # Get the current width and height of the toolbar, and resize the tool
        # icons to match
        new_icon_dimension = min(self.width(), self.height()) * self.icon_scale_factor

        # Clamp the icon's size to be between
        new_icon_dimension = \
            self.mininum_icon_size if new_icon_dimension < self.mininum_icon_size \
            else self.maximum_icon_size if new_icon_dimension > self.maximum_icon_size \
            else new_icon_dimension

        new_icon_size = QSize(new_icon_dimension, new_icon_dimension)
        self.setIconSize(new_icon_size)

        # Set the minimum width/height of the toolbar based on the orientation
        # of the icons in the toolbar
        current_orientation = self.orientation()
        number_of_actions = len(self.actions()) + 1 # Account for the close button

        if current_orientation == Qt.Horizontal:
            self.setMinimumWidth(number_of_actions * self.mininum_icon_size *
                                 self.icon_scale_factor)
            self.setMinimumHeight(self.mininum_icon_size * self.icon_scale_factor)

        elif current_orientation == Qt.Vertical:
            self.setMinimumWidth(self.mininum_icon_size *  self.icon_scale_factor)
            self.setMinimumHeight(number_of_actions * self.mininum_icon_size *
                                  self.icon_scale_factor)

        else:
            self.logger.error('Unknown orientation type: {0} returned! Defaulting '
                              'to default minimum width/height values for the '
                              'toolbar!'.format(current_orientation))
            self.setMinimumSize(self.minimumToolbarWidth, self.minimumToolbarHeight)


    def add_actions(self, actions):
        """
        This method sets up the initial actions for the toolbar.

        :param actions:  ``list`` of ``list`` objects containing either:

            - ``str``
            - ``QIcon``, ``str``
            - ``str``, ``QObject``
            - ``QIcon``, ``str``, ``QObject``

            that will be used to propagate the initial list of actions in the
            toolbar.
        """
        # Add the actions to the toolbar
        for action in actions:
            if not action:
                continue

            action_instance = self.addAction(*action)
            action_instance.setStatusTip(action[1])

        self.addSeparator()

        # Add a close action at the end by default
        close_action = self.addAction('Close', lambda: self.remove_toolbar(self))
        close_action.setStatusTip('Closes the shelf.')


    def remove_toolbar(self, toolbar, parent=None):
        """
        This method removes the toolbar from the specified parent.
        """
        if not parent:
            parent = get_maya_window(QMainWindow)

        parent.removeToolBar(toolbar)

        # Schedule the QToolBar for garbage collection, otherwise reference ptr
        # will not actually be deleted
        toolbar.deleteLater()


class MayaDockWidget(MayaQWidgetDockableMixin, QWidget):
    """
    This base class creates a dockable widget that can be used for either
    attaching other widgets to, or importing a previously compiled .py file
    from Qt Designer to setup widgets with.
    """

    def __init__(self,
                 ui_class=None,
                 window_name=None,
                 title=None,
                 sizeable=True,
                 width=None,
                 height=None,
                 parent=None,
                 window_type=Qt.Dialog
                 ):
        """
        The constructor.

        :param ui_class: ``object`` that is a class generated from a Qt
            Designer .ui compiled .py file. Should contain the setupUi()
            method.

        :param window_name: ``str`` that will be the window's short name.

        :param window_type: ``Qt.WindowType`` that will determine the appearance/
            behaviour of the widget. Defaults to ``Qt.Window``.

            .. tip::

                Set this value to ``Qt.Window`` or ``Qt.Tool`` depending on the
                desired appearance of the tool.

        :param title: ``str`` that will be set as the window's visible title.

        :param sizeable: ``bool`` that determines if the widget can be
            interactively re-sized.

        :param width: ``int`` the width of the widget.

        :param height: ``int`` the height of the widget.

        :param parent: ``QObject`` that will act as the parent of the widget.
        """

        self.logger = logging.getLogger(__name__)

        # Call base constructor
        super(MayaDockWidget, self).__init__(parent)

        maya_main_window_ptr = get_maya_window(QMainWindow)

        if not window_name:
            window_name = os.path.dirname(os.path.abspath(__file__))

        if not title:
            title = window_name

        if not parent:
            parent = maya_main_window_ptr

        self.close_existing_instances(parent, window_name)

        # Setup the UI widgets and create a reference to the UI class returned
        self.ui = self.setup_ui_widgets(ui_class=ui_class)

        # Default to widget native width and height, else set default values
        if not width and hasattr(self, 'width'):
            width = self.width()
        else:
            width = 250

        if not height and hasattr(self, 'height'):
            height = self.height()
        else:
            height = 500

        # Set widget attributes
        self.setParent(parent)
        self.setWindowFlags(window_type)

        if sizeable:
            self.resize(width, height)
        else:
            self.setFixedSize(width, height)

        self.setWindowTitle(title)
        self.setObjectName(window_name)

        try: self.populateData()
        except Exception:
            self.logger.error('### Could not populate data to widget!!! \n {0}'
                         .format(traceback.print_exc()))
            raise Exception

        self.logger.debug('Data successfully populated to widget!')

        try: self.makeConnections()
        except Exception:
            self.logger.error('### Could not make connections to signals/slots!!! \n {0}'
                         .format(traceback.print_exc()))
            raise Exception

        self.logger.debug('Connections successfully made for widget!')

        self.show(dockable=True)


    def close_existing_instances(self, parent, window_name):
        """
        This method closes any existing instances of the tool that may be open.

        :param parent: ``QMainWindow`` parent instance that this dock widget
            is parented to.

        :param window_name: ``str`` name of the window object to check for
            and delete existing instances of.

        :return: ``None``
        """

        existing_dock_ptr = apiUi.MQtUtil.findControl(window_name)

        if existing_dock_ptr:
            # noinspection PyUnresolvedReferences
            existing_dock = shiboken.wrapInstance(
                long(existing_dock_ptr), QDockWidget
            )
            # Send closeEvent message to account for any callbacks that might
            # be hooked into it
            existing_dock.close()

            parent.removeDockWidget(existing_dock)

            # Schedule the QDockWidget for garbage collection, otherwise
            # reference ptr will not actually be deleted
            existing_dock.deleteLater()


    def setup_ui_widgets(self, ui_class=None):
        """
        This method creates all the necessary UI items.

        Should be re-implemented if you are choosing to create your own
        UI widgets directly.

        :param ui_class: ``object`` that is a class generated from a Qt
            Designer .ui compiled .py file. Should contain the setupUi()
            method.

        :return: ``Ui_Form`` class generated by Qt Designer.
        """

        # Setup UI widgets if custom UI class from Qt Designer compiled
        # file was provided or from a custom UI definition file
        if ui_class and hasattr(ui_class, 'setupUi'):
            # Setup UI widgets from class to this object
            uiClassReference = ui_class()
            uiClassReference.setupUi(self)

            # Protect the widgets from being garbage collected
            _GCProtector().add_widget(self)

            # Create reference to UI class
            return uiClassReference

        else:
            # Protect the widgets from being garbage collected
            _GCProtector().add_widget(self)
            return self

    def populateData(self, *args, **kwargs ):
        """
        This virtual method should be re-implemented to populate the
        widget with data.

        :return: ``None``
        """
        pass

    def makeConnections(self, *args, **kwargs):
        """
        This virtual method should be re-implemented to handle making
        connections from the Qt widgets to corresponding signals/slots.

        :return: ``None``
        """
        pass

    def closeEvent(self, event):
        """
        This overrides the base event in ``QDialog``.

        :param event: ``QEvent`` that is passed in.
        :return: ``None``
        """
        # Remove widget objects from GC protection
        if self in _GCProtector().widgets:
            _GCProtector().remove_widget(self)

        return super(MayaDockWidget, self).closeEvent(event)

    def cancelAction(self):
        """
        This method is run when the user chooses to close the widget via the UI.
        """
        self.close()


class MayaWidget(MayaQWidgetBaseMixin, QWidget):
    """
    This base class creates a widget that can be used for either attaching other
    widgets to, or importing a previously compiled .py file from Qt Designer
    to setup widgets with.
    """

    def __init__(self,
                 ui_class=None,
                 window_name=None,
                 title=None,
                 sizeable=True,
                 width=None,
                 height=None,
                 parent=None,
                 window_type=Qt.Dialog
                 ):
        """
        The constructor.

        :param ui_class: ``object`` that is a class generated from a Qt
            Designer .ui compiled .py file. Should contain the setupUi()
            method.

        :param window_name: ``str`` that will be the window's short name.

        :param window_type: ``Qt.WindowType`` that will determine the appearance/
            behaviour of the widget. Defaults to ``Qt.Window``.

            .. tip::

                Set this value to ``Qt.Window``, ``Qt.Tool`` or ``Qt.Dialog
                depending on the desired appearance of the tool.

        :param title: ``str`` that will be set as the window's visible title.

        :param sizeable: ``bool`` that determines if the widget can be
            interactively re-sized.

        :param width: ``int`` the width of the widget.

        :param height: ``int`` the height of the widget.

        :param parent: ``QObject`` that will act as the parent of the widget.

        :return: ``None``
        """

        self.logger = logging.getLogger(__name__)

        # Call base constructor
        super(MayaWidget, self).__init__(parent)

        maya_main_window_ptr = get_maya_window()

        if not window_name:
            window_name = os.path.dirname(os.path.abspath(__file__))

        if not title:
            title = window_name

        if not parent:
            parent = maya_main_window_ptr

        self.close_existing_instances(window_name)

        # Setup the UI widgets and create a reference to the UI class returned
        self.ui = self.setup_ui_widgets(ui_class=ui_class)

        # Default to widget native width and height, else set default values
        if not width and hasattr(self, 'width'):
            width = self.width()
        else:
            width = 250

        if not height and hasattr(self, 'height'):
            height = self.height()
        else:
            height = 500

        # Set widget attributes
        self.setParent(parent)
        self.setWindowFlags(window_type)

        if sizeable:
            self.resize(width, height)
        else:
            self.setFixedSize(width, height)

        self.setWindowTitle(title)
        self.setObjectName(window_name)

        try: self.populateData()
        except Exception:
            self.logger.error('### Could not populate data to widget!!! \n {0}'
                         .format(traceback.print_exc()))
            raise Exception

        self.logger.debug('Data successfully populated to widget!')

        try: self.makeConnections()
        except Exception:
            self.logger.error('### Could not make connections to signals/slots!!! \n {0}'
                         .format(traceback.print_exc()))
            raise Exception

        self.logger.debug('Connections successfully made for widget!')

        self.show()


    def close_existing_instances(self, window_name):
        """
        This method closes any existing instances of the tool that may be open.
        """

        if pm.window(window_name, exists=True):
            pm.deleteUI(window_name)


    def setup_ui_widgets(self, ui_class=None):
        """
        This method creates all the necessary UI items.

        Should be re-implemented if you are choosing to create your own
        UI widgets directly.

        :param ui_class: ``object`` that is a class generated from a Qt
            Designer .ui compiled .py file. Should contain the setupUi()
            method.

        :return: ``Ui_Form`` class generated by Qt Designer.
        """

        # Setup UI widgets if custom UI class from Qt Designer compiled
        # file was provided or from a custom UI definition file
        if ui_class and hasattr(ui_class, 'setupUi'):
            # Setup UI widgets from class to this object
            uiClassReference = ui_class()
            uiClassReference.setupUi(self)

            # Protect the widgets from being garbage collected
            _GCProtector().add_widget(self)

            # Create reference to UI class
            return uiClassReference

        else:
            # Protect the widgets from being garbage collected
            _GCProtector().add_widget(self)
            return self


    def populateData(self, *args, **kwargs ):
        """
        This virtual method should be re-implemented to populate the
        widget with data.
        """
        pass

    def makeConnections(self, *args, **kwargs):
        """
        This virtual method should be re-implemented to handle making
        connections from the Qt widgets to corresponding signals/slots.
        """
        pass

    def closeEvent(self, event):
        """
        This overrides the base event in ``QDialog``.

        :param event: ``QEvent`` that is passed to the base method
        :return: ``None``
        """
        # Remove widget objects from GC protection
        if self in _GCProtector().widgets:
            _GCProtector().remove_widget(self)

        return super(MayaWidget, self).closeEvent(event)


    def cancelAction(self):
        """
        This method is run when the user chooses to close the widget via the UI.
        """
        self.close()


class MayaShelfTool(object):
    """
    This class, when used as a subclass, will allow the object to be
    automatically registered as a shelf item in Maya.
    """

    #: Define shelf information. This will be used in the UI to both identify
    #: the shelf item by unique name, along with providing the tooltip information
    #: when the mouse hovers over the icon.
    #: The icon name is used to find the corresponding 32x32px *.png image that
    #: resides in the predefined /icons folder, and should not include the name
    #: of the extension itself: i.e. 'generic'.
    shelf_tool_icon_name = 'generic'
    shelf_tool_title = 'Shelf Tool'

    # Get the path to the base directory for icons
    icons_path = os.path.join(
        os.path.dirname(
            os.path.dirname(
                os.path.dirname(
                    os.path.dirname(os.path.abspath(__file__))
                )
            )
        ),
        'icons'
    )

    if not os.path.isdir(icons_path):
        raise RuntimeWarning('Could not find the icons directory located at: {0}!'.format(icons_path))


    @classmethod
    def get_shelf_tool_title(cls):
        """
        Returns the title of the shelf tool.

        :rtype: ``str``
        """
        return cls.shelf_tool_title


    @classmethod
    def get_shelf_tool_icon(cls):
        """
        Returns the icon for the shelf tool.

        :rtype: ``QIcon``
        """
        icon_path = os.path.join(
            cls.icons_path, cls.shelf_tool_icon_name + '.png'
        )

        if os.path.isfile(icon_path):
            return QIcon(icon_path)
        else:
            return QIcon()


class ColorSwatch(QWidget):
    """
    This creates a colour swatch widget that can be clicked on to return the
    selected colour.
    """
    # Define custom signal to be emitted when a user has selected a new color
    colorSelected = Signal(QColor)

    def __init__(self, parent, width=20, height=20, color=(0,0,0)):
        """
        The constructor.

        :param parent: ``QObject`` instance to act as the parent of this widget.

        :param width: ``int`` width of the swatch

        :param height: ``int`` height of the swatch

        :param color: ``list`` of RGB values for the background colour of the
            swatch.

        :return: ``QWidget``
        """

        self.logger = logging.getLogger(__name__)

        super(ColorSwatch, self).__init__(parent)

        # Define attributes for the colour swatch
        self.setAutoFillBackground(True)
        self.setFixedSize(width, height)

        self.color = color

        self.palette = self.palette()
        self.palette.setColor(
            self.backgroundRole(), QColor(*self.color)
        )

        self.setPalette(self.palette)


    @property
    def current_color(self):
        """
        Returns the current background colour of the widget.

        :return: ``QColor`` that is the current background colour of the widget.
        """

        self.__color = self.palette.color(self.backgroundRole())

        return self.__color


    @current_color.setter
    def current_color(self, color):
        """
        Sets the current background colour of the widget.

        :param color: ``QColor`` that is to be set for the widget.
        :return:
        """

        self.__color = color

        self.palette.setColor(
            self.backgroundRole(), self.__color
        )

        self.setPalette(self.palette)


    def mousePressEvent(self, event):
        """
        This opens a dialog that allows the user to pick a specific colour
        using the OS native selection tools.

        :param event:
        """
        color = self.get_custom_color()

        if color:
            self.current_color = color


    def get_custom_color(self):
        """
        This method opens a colour picker for the user to choose a specific
        colour value for.

        :return: ``QColor`` that was selected.
        """

        selected_color = QColorDialog.getColor(parent=self)

        # Emit signal if user has selected a color to notify other widgets
        # noinspection PyUnresolvedReferences
        self.colorSelected.emit(selected_color)

        return selected_color
