#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module math: This module contains useful math functions. """

from rigTools.lib.system import Singleton


class MathConstants(object):
    """
    This class contains math methods and objects that are intended to remain globally
    constant throughout any invocation of this entire package.
    """
    __metaclass__ = Singleton

    def __init__(self):
        self._pi = 3.142
        self._half_pi = 1.571


    @property
    def pi(self):
        """
        Mathematical constant.

        :rtype: ``int``
        """
        return self._pi


    @property
    def half_pi(self):
        """
        Mathematical constant.

        :rtype: ``int``
        """
        return self._half_pi