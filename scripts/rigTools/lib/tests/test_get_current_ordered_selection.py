#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module test_get_current_ordered_selection: This is a unit test."""
import pymel.core as pm
from unittest import TestCase
from rigTools.lib.selections import get_current_ordered_selection

class TestGet_current_ordered_selection(TestCase):
    def setUp(self):
        a = pm.createNode('transform', name='testNode1')
        b = pm.createNode('transform', name='testNode2')
        c = pm.createNode('mesh', name='testNode3')
        self.createdNodes = [a, b, c]
        pm.addAttr(a, ln='testAttr', at='double', defaultValue=1.0)
        pm.select(self.createdNodes, r=True)

    def test_get_current_ordered_selection(self):
        result = get_current_ordered_selection()
        self.assertEqual(result,
                         self.createdNodes,
                         'The returned selection was invalid!')
