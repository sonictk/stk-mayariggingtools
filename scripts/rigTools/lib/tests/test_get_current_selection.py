#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Module test_get_current_selection: This is a unit test."""
import pymel.core as pm
from rigTools.lib.test import MayaTestCase
from rigTools.lib.selections import get_current_selection


class TestGet_current_selection(MayaTestCase):
    def setUp(self):
        a = pm.createNode('transform', name='testNode1')
        b = pm.createNode('transform', name='testNode2')
        c = pm.createNode('mesh', name='testNode3')
        self.createdNodes = [a, b, c]
        pm.addAttr(a, ln='testAttr', at='double', defaultValue=1.0)
        pm.select(self.createdNodes, r=True)

    def test_get_current_selection(self):
        result = get_current_selection()
        self.assertEqual(result, self.createdNodes)

        pm.select(None, r=True)
        emptyResult = get_current_selection()
        self.assertEqual(emptyResult, [])

        pm.select(self.createdNodes, r=True)
        filteredResult = get_current_selection(filter_type='transform')
        self.assertEqual(filteredResult, [result[0], result[1]])

        filteredAttrResult = get_current_selection(filter_attribute='testAttr')
        self.assertEqual(filteredAttrResult, [result[0]])
