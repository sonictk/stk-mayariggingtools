#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module system: This module contains objects for dealing with Maya core systems.
"""

import logging
import os
import sys

from PySide.QtCore import QSettings, QFileInfo

from rigTools.lib.exceptions import FileNotFoundError
from rigTools.lib.jsonutils import JSONUtils


class Singleton(type):
    """
    This is a metaclass that will allow for a class that specifies use of this
    class to be a **singleton**, that is, this class can only ever have one
    instance running at any given time.

    .. tip::
        To use, specify the __metaclass__ attribute of the class to refer to this
        Singleton class.

    .. note::
        In Python 3, you can define a class with the metaclass in the constructor,
        like so:

            ```
            class TestClass(metaclass=Singleton):
                pass
            ```

    .. warning::
        This should only be used in specific use cases that call for it! The use
        of the singleton design pattern in Python is highly discouraged!
    """
    # Create holder for the instances of the singleton that are already running
    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        Re-implemented in order to return the ``Singleton`` object whenever
        it or any of its methods are called.

        :return: ``Singleton`` object.
        """
        # If no existing instance exists, create a new instance; otherwise
        # return the existing one.
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Borg(object):
    """
    This is a base class that allows for creating an object with a shared state
    among all its instances.
    """

    _shared_state = {}

    def __init__(self):
        """
        The constructor.
        """
        self.__dict__ = self._shared_state


    @property
    def shared_state(self):
        """
        The current state that is shared among all object instances.

        :return: ``dict`` state.
        """
        return self._shared_state


    @shared_state.setter
    def shared_state(self, state):
        """
        Set a new **state** that will be shared among all object instances.

        :param state: ``dict`` New state to set.
        """
        self._shared_state = state
        self.__dict__ = self._shared_state


class Constants(object):
    """
    This class contains methods and objects that are intended to remain globally
    constant throughout any invocation of this entire package.
    """
    __metaclass__ = Singleton

    def __init__(self):
        """
        The constructor. Loads all the initial constants necessary and checks
        that the configuration files are valid on disk.
        """
        # The plugin global configuration location should be a relative immutable
        # location on disk
        self.plugin_config_file = os.path.join(
            os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
            'cfg',
            'plugin_config.json'
        )

        if not os.path.isfile(self.plugin_config_file):
            raise FileNotFoundError('The plugin configuration file: {0} could '
                                    'not be found!'.format(self.plugin_config_file))
        else:
            self._plugin_config_data = JSONUtils.read(self.plugin_config_file)

        # TODO: Make the translations folder path configurable in the global
        # configuration file location and not immutable relative as it is now
        self.translations_folder_path = os.path.join(
            os.path.dirname(os.path.dirname(os.path.dirname(__file__))), 'translations'
        )

        if not os.path.isdir(self.translations_folder_path):
            raise FileNotFoundError('The translations configuration folder: {0} '
                                    'does not exist!'.format(self.translations_folder_path))
        else:
            self._translations_config_data = JSONUtils.read(self.translations_file_path)

        self._pydev_helpers_path = os.environ.get('PYDEV_HELPERS_PATH')

    @property
    def plugin_config_data(self):
        """
        This is the current configuration data for the plugin as loaded from the
        configuration file on disk.

        :return: ``dict`` of configuration settings.
        """
        return self._plugin_config_data

    @property
    def toolkit_path(self):
        """
        This returns the path to the sources for the toolkit.

        :return: ``str`` path to directory containing toolkit sources on disk.
        """
        return os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

    @property
    def plugin_path(self):
        """
        This returns the path to the plugin binaries for this toolkit.

        :return: ``str`` path to the directory containing the toolkit plugins.
        """
        return os.path.join(
            os.path.dirname(
                os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(__file__)
                    )
                )
            ), 'plug-ins', 'bin'
        )

    @plugin_config_data.setter
    def plugin_config_data(self, settings, store=False):
        """
        This sets the current plugin's configuration data to the ``dict`` of
        settings given.

        :param settings: ``dict`` of JSON-formatted settings.

        :param store: ``bool`` If set to ``True``, will write the given settings
            to disk as well for persistent storage.
        """
        for key, value in settings:
            self._plugin_config_data[key] = value

        if store:
            JSONUtils.write(self.plugin_config_file, self._plugin_config_data)

    @property
    def plugin_name(self):
        """
        This is the name of the plugin.

        :return: ``str`` name.
        """
        return self._plugin_config_data.get('plugin_config', {}).get('plugin_name')

    @property
    def plugin_version(self):
        """
        This is the current version of the plugin.

        :return: ``str`` semantic version.
        """
        return self._plugin_config_data.get('plugin_config', {}).get('version')

    @property
    def plugin_language(self):
        """
        This is the current language that the plugin is beign loaded with.

        :return: ``str`` language identifier setting.
        """
        return self._plugin_config_data.get('plugin_config', {}).get('language')

    @plugin_language.setter
    def plugin_language(self, language, store=False):
        """
        This sets the current plugin's language to the ``str`` identifier given.

        :param language: ``str`` that identifies the language to be used.

        :param store: ``bool`` If set to ``True``, will write the given settings
            to disk as well for persistent storage.
        """
        if not os.path.isfile(
            os.path.join(self.translations_folder_path, language + '.json')
        ):
            raise FileNotFoundError('Failed to set translation to: {0} as translation '
                                    'file does not exist in: {1}!'
                                    .format(language, self.translations_folder_path))
        else:
            self._plugin_config_data['plugin_config']['language'] = language

            if store:
                JSONUtils.write(self.plugin_config_file, self._plugin_config_data)

    @property
    def translations_file_path(self):
        """
        The path to the current translation file that is configured.

        :return: ``str`` path to the filename on disk.
        """
        return os.path.join(self.translations_folder_path,
                            self.plugin_language + '.json')

    @property
    def plugin_translation_data(self):
        """
        This is the current configuration data for the strings used in the
        plugin as loaded from the translations stringslist file on disk.

        :return: ``dict`` of configuration strings that are language-specific.
        """
        return self._translations_config_data

    @plugin_translation_data.setter
    def plugin_translation_data(self, settings, store=False):
        """
        This method sets the current ``dict`` of translation settings to the one
        specified.

        :param settings: ``dict`` of translation settings to apply.

        :param store: ``bool`` If set to ``True``, will write the given settings
            to disk as well for persistent storage.
        """
        for key, value in settings:
            self._translations_config_data[key] = value

        if store:
            JSONUtils.write(self.plugin_config_file, self._translations_config_data)

    @property
    def application_description(self):
        """
        This gets the description of the plugin.

        :return: ``str`` description.
        """
        return self.plugin_translation_data.get('constants', {})\
            .get('application_description')

    @property
    def application_name(self):
        """
        This gets the API name of the plugin.

        :return: ``str`` API name.
        """
        return self.plugin_translation_data.get('constants', {})\
            .get('application_name')

    @property
    def organization_name(self):
        """
        This gets the description of the plugin.

        :return: ``str`` description.
        :rtype: ``str``
        """
        return self.plugin_translation_data.get('constants', {})\
            .get('organization_name')

    @property
    def menu_name(self):
        """
        This is the name of the top-level menu to register for the plugin.

        :return: ``str`` menu name.
        """
        return self.plugin_translation_data.get('constants', {})\
            .get('menu_name')

    @property
    def logging_config_data(self):
        """
        This property is the current logging settings that have been specified
        for the plugin.

        :return: ``dict`` of logging settings.
        """
        return self.plugin_config_data.get('logging', {})

    @property
    def pydev_helper_path(self):
        """
        This is the path to the PyDev helper eggs.
        """
        return self._pydev_helpers_path


class RigToolsSettings(object):
    """
    This class contains methods for managing local user-side settings.
    """

    def __init__(self,
                 configFormat=QSettings.IniFormat,
                 scope=QSettings.UserScope,
                 organization=Constants().organization_name,
                 application=Constants().application_name,
                 *args):
        """
        The constructor.
        Creates a QSettings configuration instance that can be read/written from.

        :param configFormat: ``QSettings.Format`` determining the format of the config file stored.
                            Defaults to an INI file.

        :param scope: ``QSettings.Scope`` determining whether user/system-specific
                        settings should be saved. Defaults to user-scope.

        :param organization: ``str`` determining the organization name.
                            Prefixed to setting values.

        :param application: ``str`` determining the application name.
                            Prefixed to setting values after the *organization*.

        :param args:

        :return: ``RigToolsSettings`` QSettings instance
        """

        self.logger = logging.getLogger(__name__)

        self.__settings = QSettings(
            configFormat, scope, organization, application
        )

        # Store path to global QSettings file
        self.__settingsPath = \
            QFileInfo(self.settings.fileName()).absoluteFilePath()

        # Ensure that nothing is written to registry, only write physical files (Windows)
        if sys.platform == 'win32':
            self.settings.setFallbacksEnabled(False)


    @property
    def settings(self):
        return self.__settings

    @settings.setter
    def settings(self, settings_instance):

        if not isinstance(settings_instance, RigToolsSettings):
            self.logger.error('### {0} must be a {1} object!!!'
                              .format(settings_instance, self.__class__.__name__))
            return

        self.__settings = settings_instance


    @property
    def settingsPath(self):
        return self.__settingsPath

    @settingsPath.setter
    def settingsPath(self, path):

        if not isinstance(path, str):
            self.logger.error('### {0} must be a string object!!!'
                              .format(path))
            return

        self.__settingsPath = path


    def returnToRootGroup(self):
        """
        This method sets the current group back to the top-level root.

        :return: ``None``
        """

        while self.settings.group():
            self.settings.endGroup()


    def createSetting(self, prefix, key, value):
        """
        This method creates a new QSettings variable for storing data.

        :param prefix: ``str`` that will be added to all keys.
        :param key:``object`` key value for recalling values.
        :param value: ``object`` value to be associated with key.
        :return: ``None``
        """

        self.settings.beginGroup(prefix)
        self.settings.setValue(key, value)

        self.logger.debug('Successfully wrote to QSettings:{0}:{1} as {2}!'
                          .format(prefix, key, value))

        # Reset group to root
        self.returnToRootGroup()

        # Manually ensure that changes are saved to QSettings store
        self.settings.sync()


    def createSettings(self, prefix, data):
        """
        This method handles storing multiple QSettings variables at once.

        :param prefix: ``str`` that will be added to all keys.
        :param data: ``dict`` containing key/value pairs to be written to *QSettings*.
        :return: ``None``
        """

        assert isinstance(data, dict), \
            '### Data to be written to QSettings file must be in the form ' \
            'of a Python dict() object!'

        self.settings.beginGroup(prefix)

        # write to QSettings object
        for key, value in data.items():
            self.settings.setValue(key, value)

        self.returnToRootGroup()

        # Manually ensure that changes are saved to QSettings store
        self.settings.sync()

        self.logger.debug('Successfully wrote to QSettings:{0}:{1}!'
                          .format(prefix, data))


    def readSetting(self, prefix, key):
        """
        This method handles reading a single setting from a given *prefix*
        and *key*.

        :param key: ``str`` indicating which key to read the value from.
        :param prefix: ``str`` indicating which QSettings group to read from
        :return: ``str`` or ``bool`` containing the key/value pair being read from
        :rtype: ``str`` or ``bool``
        """

        # Check if the group exists
        if self.settings.allKeys():

            # Grab specific QSettings group
            if prefix in self.settings.childGroups():

                # set current group
                self.settings.beginGroup(prefix)

                if key in self.settings.allKeys():
                    value = self.settings.value(key)

                    # Check for bool strings and convert to ``bool`` types
                    if value == 'true':
                        value = True
                    elif value == 'false':
                        value = False
                    else:
                        value = self.settings.value(key)

                    # Manually ensure that changes are saved to QSettings store
                    self.settings.sync()

                    self.returnToRootGroup()

                    return value


    def readSettings(self, prefix):
        """
        This method handles reading settings from a given *prefix*.

        :param prefix: ``str`` indicating which QSettings group to read from
        :return: ``dict`` containing the key/value pairs from the QSettings config file
        :rtype: ``dict``
        """

        # Check if group exists
        if self.settings.allKeys():

            # Grab specific QSettings group
            if prefix in self.settings.childGroups():

                # Set current group
                self.settings.beginGroup(prefix)

                # Create dict for output data
                data = {}

                # Iterate over QSettings and store in data to be returned
                for key in self.settings.allKeys():

                    value = self.settings.value(key)

                    # Check for bool strings and convert to ``bool`` types
                    if value == 'true':
                        data[key] = True
                    elif value == 'false':
                        data[key] = False
                    else:
                        data[key] = value

                # Reset group to root
                self.returnToRootGroup()

                # Manually ensure that all unsaved changes are written to storage
                self.settings.sync()

                return data


    def deleteSetting(self, prefix, key):
        """
        This method removes a setting from a given *prefix*.

        :param prefix: ``str`` indicating which QSettings group to read from
        :param key: ``str`` indicating which key to delete.
        """

        self.settings.beginGroup(prefix)

        self.settings.remove(key)

        self.returnToRootGroup()

        # Manually ensure that all unsaved changes are written to storage
        self.settings.sync()
