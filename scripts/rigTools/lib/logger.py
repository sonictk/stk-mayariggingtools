#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module logger: This module contains a definition for a custom logger that
should be used for all logging purposes for the rigTools plugin.
"""
import logging
import logging.config
import sys
from rigTools.lib.system import Constants


class RigToolsLogger(logging.Handler):
    """
    This is a custom logger for the rigging tools that should be used for
    capturing all logging output across the toolset.

    Will also write to a logfile as events are handled.
    """
    _default_logging_level = logging.INFO

    def __init__(self, *args, **kwargs):
        """The constructor. """
        super(RigToolsLogger, self).__init__(*args, **kwargs)

        logging_settings = Constants().logging_config_data

        try: logging.config.dictConfig(logging_settings)
        except (ValueError, TypeError, AttributeError) as err:
            sys.stderr.write('Invalid logging settings specified!\n{0}'.format(err))
            logging.basicConfig(level=self._default_logging_level)
        except ImportError as err:
            sys.stderr.write('Failed to import logging settings!\n{0}'.format(err))
            logging.basicConfig(level=self._default_logging_level)


    def emit(self, record):
        """
        Re-implemented in order to also write to a specified logfile on disk
        as necessary.

        :param record: ``str`` that is the logging record to be emitted.
        :return:
        """
        try:
            msg = self.format(record=record)

        except (KeyboardInterrupt, SystemExit):
            raise RuntimeError('Failed to emit message to log stream!')


