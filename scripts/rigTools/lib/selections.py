#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module selections: This module contains custom classes for dealing with
selections inside of Maya.
"""
import logging

import maya.OpenMaya as om
import pymel.core as pm

from rigTools.lib.api.datatypes import APIPointArray
from rigTools.lib.api.globals import APISelectionList, APIObject, \
    APIDagPath, APIGlobal, APIFn, APIFnMesh, APIFnNurbsCurve
from rigTools.lib.metadata import add_metadata_to_node, MetadataConstants
from rigTools.lib.undo import UndoManager


def get_current_selection(filter_type='', filter_attribute=''):
    """
    This method returns the current user selection.

    :param filter_attribute: ``str`` indicating that only objects with the
        attribute present should be returned.

    :param filter_type: ``str`` indicating type of object to filter by.

    :return: ``list`` of selected nodes or empty ``list`` if selection is empty.
    """
    if filter_type:
        return pm.ls(sl=True, typ=filter_type)
    elif filter_attribute and filter_type:
        return [sel for sel in pm.ls(sl=True, typ=filter_type)
                if pm.hasAttr(sel, filter_attribute)]
    elif filter_attribute:
        return [sel for sel in pm.ls(sl=True)
                if pm.hasAttr(sel, filter_attribute)]
    else:
        return pm.ls(sl=True)


def get_current_ordered_selection(filter_type=''):
    """
    This method returns an ordered sequence of the current user selection.

    :param filter_type: ``str`` indicating type of object to filter by.

    :return: ``False`` or ``list`` of selected nodes depending on if selection
        is empty.
    """

    if filter_type:
        current_selection = pm.ls(orderedSelection=True, typ=filter_type)
    else:
        current_selection = pm.ls(orderedSelection=True)

    if not current_selection:
        pm.warning('You need to select an object in the scene!')
        return False
    else:
        return current_selection


def get_active_selection_list(ordered=False, filter_type=None, as_dag_paths=False):
    """
    This method returns a ``APISelectionList`` of the user selection.

    :param filter_type: ``str`` or ``MFn.filter`` type to specify for filtering
        the selection list. Can also specify as a ``list`` containing compound
        filters to use.

    :param ordered: ``bool`` determining if the order of the selection matters.

    :param as_dag_paths: ``bool`` If set to ``True``, the selection list returned
            will be a list of DAG paths to the current selected objects instead
            of the ``APIObject`` nodes.

    :return:``APISelectionList`` containing the active selection.

    :rtype : ``APISelectionList`` | ``None``
    """

    selection_list = APISelectionList()
    result_selection = APISelectionList()

    # Check if ordered selection flag is set via API and set it
    if ordered and not APIGlobal.trackSelectionOrderEnabled():
        APIGlobal.setTrackSelectionOrderEnabled(True)

    APIGlobal.getActiveSelectionList(selection_list)

    if selection_list.isEmpty():
        return selection_list

    for idx in xrange(selection_list.length()):
        if as_dag_paths:
            item = APIDagPath()
            selection_list.getDagPath(idx, item)
        else:
            item = APIObject()
            selection_list.getDependNode(idx, item)

        if not filter_type:
            result_selection.add(item)
        
        # Account for filter types specified as strings
        elif isinstance(filter_type, str) or (hasattr(item, 'apiTypeStr') and
                item.apiTypeStr() == filter_type):
            result_selection.add(item)
        # Account for filter types specified as enums
        elif isinstance(filter_type, int) or (hasattr(item, 'apiTypeStr') and
                item.apiType() == filter_type):
            result_selection.add(item)
        elif (isinstance(filter_type, list) or isinstance(filter_type, tuple)) and \
            (item.apiType() or (hasattr(item, 'apiTypeStr') and item.apiTypeStr())
                in filter_type):
            result_selection.add(item)
            
        # Account for no filter specified
        else:
            continue

    return result_selection


@UndoManager.undoable
def get_selection_center(nodes=None, create_helper=True, helper_name='center'):
    """
    This method returns the center of the current selection.
    Can optionally create a helper at the center location to act as a visual aid.

    :param nodes: ``list`` of objects or components to find the center for.

    :param create_helper: ``bool`` indicating if a visual aid DAG node should be
        created at the selection center

    :param helper_name: ``str`` that will be the name of the helper created.
        Is ignored if ``create_helper`` is ``False``.
    """

    logger = logging.getLogger(__name__)

    point_positions = []

    current_selection = APISelectionList()
    APIGlobal.getActiveSelectionList(current_selection)

    if current_selection.isEmpty():
        APIGlobal.displayWarning('Please select at least one object in the scene!')

    else:
        dag_path = APIDagPath()
        component = APIObject()

        for idx in xrange(current_selection.length()):
            current_selection.getDagPath(idx, dag_path, component)

            dag_node = om.MFnDagNode(dag_path)
            dag_node_type = dag_node.typeName()

            # Iterate over the shape node(s) from the transform DAG node and
            # determine what kind of shapes they are, then get the positions of
            # all their components
            if dag_node_type == 'transform':

                shape_dag_path = APIDagPath()
                shape_component = APIObject()

                current_selection.getDagPath(idx, shape_dag_path, shape_component)

                # Pass values by reference to Maya Python API
                num_shapes_util = om.MScriptUtil()
                num_shapes_util.createFromInt(0)
                num_shapes_ptr = num_shapes_util.asUintPtr()

                shape_dag_path.numberOfShapesDirectlyBelow(num_shapes_ptr)
                shapes_count = om.MScriptUtil(num_shapes_ptr).asUint()

                for shape_idx in range(shapes_count):
                    shape_dag_path.extendToShapeDirectlyBelow(shape_idx)

                    if shape_dag_path.apiType() == om.MFn.kMesh:
                        in_mesh_points = APIPointArray()

                        # Store vertex world positions in the array
                        fn_mesh = APIFnMesh(dag_path)
                        fn_mesh.getPoints(in_mesh_points, om.MSpace.kWorld)

                        for i in xrange(in_mesh_points.length()):
                            point_positions.append(
                                [
                                    in_mesh_points[i][0],
                                    in_mesh_points[i][1],
                                    in_mesh_points[i][2]
                                ]
                            )

                    elif shape_dag_path.apiType() == om.MFn.kNurbsCurve:
                        in_curve_points = APIPointArray()

                        # Store CV world positions in the array
                        fn_curve = APIFnNurbsCurve(dag_path)
                        fn_curve.getCVs(in_curve_points, om.MSpace.kWorld)

                        for i in xrange(in_curve_points.length()):
                            point_positions.append(
                                [
                                    in_curve_points[i][0],
                                    in_curve_points[i][1],
                                    in_curve_points[i][2]
                                ]
                            )

                    # todo: support NURBS surfaces as well

                    else:
                        logger.warning('# The following transform of type: {0} '
                                       'is not supported for calculating center '
                                       'positions: {1}'
                               .format(
                            shape_component.apiTypeStr(), shape_component)
                        )

            # Iterate over mesh component selections as well and get their
            # positions
            elif dag_node_type == 'mesh':
                fn_iterator_geo = om.MItGeometry(dag_path, component)

                while not fn_iterator_geo.isDone():
                    point_position = fn_iterator_geo.position(APIFn.kWorld)

                    point_positions.append(
                        [point_position.x, point_position.y, point_position.z]
                    )

                    fn_iterator_geo.next()

            # Iterate over NURBS curve CVs and get their positions
            elif dag_node_type == 'nurbsCurve':
                fn_iterator_curve = om.MItCurveCV(dag_path, component)

                while not fn_iterator_curve.isDone():
                    cv_position = fn_iterator_curve.position(APIFn.kWorld)

                    point_positions.append(
                        [cv_position.x, cv_position.y, cv_position.z]
                    )

                    fn_iterator_curve.next()

            else:
                logger.warning('# The following object of type: {0} is not '
                               'supported for calculating center positions: {1}'
                               .format(component.apiTypeStr(), component))

        # Calculate the center position by taking the average of all the
        # component positions
        center_position = [sum(x) / len(point_positions)
                           for x in zip(*point_positions)]

        if not center_position:
            logger.error('### No valid center position could be calculated!!!')
            pm.confirmDialog(
                title='Invalid objects given for center calculation!',
                message='Could not calculate a valid center calculation for the '
                        'given objects! Please check the Script Editor for '
                        'details!',
                icon='warning',
                b='Close'
            )
            return

        logger.debug('Center position calculated as: {0}'.format(center_position))

        if create_helper:
            # Create the helper object as a group node
            dag_modifier = om.MDagModifier()
            helper_object = dag_modifier.createNode('transform')
            dag_modifier.renameNode(helper_object, 'center')

            dag_modifier.doIt()

            # Set the translation value of the helper object
            fn_xform = om.MFnTransform(helper_object)
            fn_xform.setTranslation(om.MVector(*center_position), om.MSpace.kTransform)

            # Set the properties of the helper object
            fn_node = om.MFnDagNode(helper_object)
            display_handle_plug = fn_node.findPlug('displayHandle')
            display_handle_plug.setInt(1)

            # Set metadata on helper object
            add_metadata_to_node(
                helper_object,
                MetadataConstants().helper_attribute_name,
                True,
                fn_node
            )

        return center_position
