#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module node_setters: This module contains functions for setting various node
properties in Maya.
"""

import logging

import maya.OpenMaya as om
import maya.api.OpenMaya as om2
import pymel.core as pm

from rigTools.lib.api.globals import APIFnTransform, APIQuaternion, \
    APITransformationMatrix, APIVector, APIObject, APIFnDagNode, APIFn, \
    APIItDependencyGraph
from rigTools.lib.selections import get_current_ordered_selection
from rigTools.lib.undo import UndoManager


def get_child_transform(node):
    """
    This function retrieves the first found child transform of the given  **node**.

    :param node: ``APIObject`` The node to get the child transform for.

    :return: ``APIObject`` The child transform of the node.
    """
    fn_dagnode = APIFnDagNode(node)
    num_child_nodes = fn_dagnode.childCount()

    if num_child_nodes > 0:
        for idx in xrange(num_child_nodes):
            child_node = fn_dagnode.child(idx)
            if child_node.apiType() == APIFn.kTransform:
                return child_node


def walk_dag_for_node(node, api_type_filter, direction, name_contains=None):
    """
    This recursive function walks the dependency graph according to the
    **direction** given to find the node with the specified **api_type_filter**.

    :param node: (MObject): The node to start walking from in order to find the
            requested node.

    :param api_type_filter: (str|om.MFn.Type): Filter the nodes to find by the
        type of node that they are.

    :param direction: (om.MItDependencyGraph.kDirection: Direction to traverse the DG.

    :param name_contains:  (str): If specified, only the node that has a name
        that contains part of this string will have be returned.

    :return: (MObject): First node that is found that matches the filter.
        If no node matches, returns ``None``.
    """
    logger = logging.getLogger(__name__)


    def walk_dag_node(node_to_recurse, api_type_filter, direction, name_contains):
        """
        This function will recursively walk through the given **node** until the
        given filter matches.

        :param node_to_recurse: ``MObject`` The node to recurse through.

        :param api_type_filter: (str|om.MFn.Type): Filter the nodes to find by
            the type of node that they are.

        :param direction: (om.MItDependencyGraph.kDirection: Direction to
            traverse the DG.

        :param name_contains: (str): If specified, only the node that has a name
            that contains part of this string will have be returned.

        :return: (MObject): First node that is found that matches the filter.
            If no node matches, returns ``None``.
        """
        if node_to_recurse and node_to_recurse.apiType() == api_type_filter \
                or node_to_recurse.apiTypeStr() == api_type_filter:
            if name_contains:
                fn_dag_recurse_node = APIFnDagNode(node_to_recurse)
                if name_contains in fn_dag_recurse_node.name():
                    return node_to_recurse
                else:
                    # Otherwise keep recursing till a match is found
                    return walk_dag_for_node(node_to_recurse, api_type_filter,
                                             direction, name_contains)
            else:
                return node_to_recurse

        else:
            # Otherwise keep recursing till a match is found
            return walk_dag_for_node(node_to_recurse, api_type_filter, direction,
                                     name_contains)


    # Based on direction, search recursively through the children/parents of
    # the given node
    fn_dag_node = APIFnDagNode(node)

    if direction in (APIItDependencyGraph.kUpstream, 'upstream'):
        num_of_parents = fn_dag_node.parentCount()
        for par_idx in xrange(num_of_parents):
            recurse_node = fn_dag_node.parent(par_idx)

            result = walk_dag_node(
                node_to_recurse=recurse_node,
                api_type_filter=api_type_filter,
                direction=direction,
                name_contains=name_contains
            )

            if result:
                return result

    elif direction in (om.MItDependencyGraph.kDownstream, 'downstream'):
        num_of_children = fn_dag_node.childCount()
        for child_idx in xrange(num_of_children):
            recurse_node = fn_dag_node.child(child_idx)

            result = walk_dag_node(
                node_to_recurse=recurse_node,
                api_type_filter=api_type_filter,
                direction=direction,
                name_contains=name_contains
            )

            if result:
                return result

    else:
        logger.error('The direction specified: {0} to walk the dependency graph '
                     'was invalid!'.format(direction))
        return


def mirror_nodes(nodes, mirror_axis='x', delete_old_nodes=False):
    """
    This method mirrors the selected nodes across the specified mirror axis.

    :param delete_old_nodes: ``bool`` indicating if the original nodes should be
        removed after the mirror operation.

    :param nodes: ``list`` of ``nt.Transform`` objects to mirror.

    :param mirror_axis: ``str`` indicating the axis to mirror across.

    :return:
    """

    logger = logging.getLogger(__name__)

    if mirror_axis == 'x':
        negate_translate_x = -1
        negate_translate_y = 1
        negate_translate_z = 1

        negate_rotation_x  = 1
        negate_rotation_y  = -1
        negate_rotation_z  = -1

    elif mirror_axis == 'y':
        negate_translate_x = 1
        negate_translate_y = -1
        negate_translate_z = 1

        negate_rotation_x  = -1
        negate_rotation_y  = 1
        negate_rotation_z  = -1

    elif mirror_axis == 'z':
        negate_translate_x = 1
        negate_translate_y = 1
        negate_translate_z = -1

        negate_rotation_x  = -1
        negate_rotation_y  = -1
        negate_rotation_z  = 1

    else:
        logger.error('### An invalid mirror axis: {0} was specified!!!'
                     .format(mirror_axis))
        raise AttributeError

    for node in nodes:
        # Get the API object of the node from PyMEL bindings
        api_dag_path = node.__apimdagpath__()

        fn_dag_node = APIFnDagNode(api_dag_path)

        node_type = fn_dag_node.typeName()

        fn_transform = APIFnTransform(api_dag_path)

        # Get the world translations and rotations of the node
        node_translation = fn_transform.getTranslation(om.MSpace.kWorld)

        node_rotation = APIQuaternion()
        fn_transform.getRotation(node_rotation, om.MSpace.kWorld)

        # Normalize the quaternion to account for any non-uniform scaling of the
        # node
        node_rotation.normalizeIt()

        # Construct mirrored rotation and translation
        mirrored_translation = APIVector(
            node_translation.x * negate_translate_x,
            node_translation.y * negate_translate_y,
            node_translation.z * negate_translate_z,
        )

        mirrored_rotation = APIQuaternion(
            node_rotation.x * negate_rotation_x,
            node_rotation.y * negate_rotation_y,
            node_rotation.z * negate_rotation_z,
            node_rotation.w
        )

        # Construct new mirrored transformation matrix to factor out scaling
        mirrored_transform = APITransformationMatrix()

        # Set the rotation/translation components of the SRT matrix
        mirrored_transform.setRotation(
            mirrored_rotation.x,
            mirrored_rotation.y,
            mirrored_rotation.z,
            mirrored_rotation.w
        )
        mirrored_transform.setTranslation(mirrored_translation, om.MSpace.kWorld)

        # Create the new mirrored node and set its transformation
        fn_dag = om.MDagModifier()
        mirrored_obj = APIObject()

        fn_dag.createNode(node_type, mirrored_obj)

        # Commit changes made to the DAG
        fn_dag.doIt()

        fn_transform_mirrored_obj = APIFnTransform(mirrored_obj)
        fn_transform_mirrored_obj.set(mirrored_transform)


@UndoManager.repeatable
@UndoManager.undoable
def rename_nodes(
        nodes=None,
        filter_type=None,
        base_name='',
        prefix='',
        suffix='',
        separator='',
        number_from=0,
        renumber=True
):
    """
    This method renames the given nodes according to the parameters given.

    :param nodes: ``list`` of ``nt.Transform`` objects to be renamed.

    :param number_from: ``int`` determining where the re-numbering starts from.

    :param renumber: ``bool`` indicating if the nodes should be re-numbered
        in their new name.

    :param base_name: ``str`` that will act as the new base name for the
        renamed objects.

    :param prefix: ``str`` prefix to append to object name.

    :param suffix: ``str`` suffix to suffix to object name.

    :param separator: ``str`` used to separate elements.


    :return: ``None``
    """

    logger = logging.getLogger(__name__)

    if not nodes:
        nodes = get_current_ordered_selection(filter_type=filter_type)

        if not nodes:
            return

    for idx, node in enumerate(nodes):

        if not suffix:
            suffix = ''
        if not prefix:
            prefix = ''

        if not renumber:
            number = ''
        else:
            number = str(int(number_from) + idx)

        # Format new name for the node
        if separator == 'camelCase':
            new_name = '{prefix}{basename}{suffix}{number}'\
                .format(
                prefix=prefix,
                basename=base_name[0:1].upper()+base_name[1:],
                suffix=suffix[0:1].upper()+suffix[1:],
                number=number
            )

        else:
            new_name = separator.join(
                filter(None, [prefix, base_name, suffix, number])
            )

        pm.rename(node, new_name)

    logger.info('Successfully renamed objects!')


def toggle_local_axes(nodes, toggle=True, state=True):
    """
    This method toggles display of objects' local rotation axes.

    :param state:  ``bool`` indicating what the state should be set to.
        Ignored if ``toggle`` is set to ``True``.
    :param toggle: ``bool`` indicating if the state should be toggled or not.
    :param nodes: ``list`` of ``nt.Transform`` axes to toggle display for.
    """

    for node in nodes:

        if toggle:
            if pm.toggle(node, q=True, localAxis=True):
                pm.toggle(node, state=False, localAxis=True)
            else:
                pm.toggle(node, state=True, localAxis=True)

        else:
            pm.toggle(node, state=state, localAxis=True)


class NodeColor(object):
    """
    This class contains methods for handling node colours in Maya.
    """

    def __init__(self):
        """
        The constructor.
        """

        self.logger = logging.getLogger(__name__)

        # Bind default colour indexes in Maya to human-readable string values.
        self._color_mappings = {
            1 : 'Black',
            2 : 'Dark Grey',
            3 : 'Grey-ish',
            4 : 'Dark Red',
            5 : 'Dark Blue',
            6 : 'Blue',
            7 : 'Dark Green',
            8 : 'Dark Purple',
            9  : 'Purple',
            10 : 'Brown',
            11 : 'Dark Brown',
            12 : 'Dark Red-dish',
            13 : 'Red',
            14 : 'Green',
            15 : 'Dark Blue-ish',
            16 : 'White',
            17 : 'Yellow',
            18 : 'Light Blue',
            19 : 'Light Green',
            20 : 'Peach',
            21 : 'Skin',
            22 : 'Lighter Yellow',
            23 : 'Grass',
            24 : 'Browner',
            25 : 'Camo',
            26 : 'Dark Camo',
            27 : 'Golf Green',
            28 : 'Marine',
            29 : 'Sonic',
            30 : 'Florite',
            31 : 'Magenta'
        }


    @property
    def color_mappings(self):
        return self._color_mappings


    @color_mappings.setter
    def color_mappings(self, mappings):
        """
        Custom setter for color mappings. Can be used to remap default how colors
        will be assigned to nodes using this class.

        :param mappings: ``dict`` containing ``int`` index and ``str`` value
            colour mappings.
        """
        self._color_mappings = mappings


    def set_nodes_color(self, color, nodes=None):
        """
        This method sets the colour of an object(s) in Maya.

        :param color: ``list`` of ``int`` RGB colour values indicating colour
            to set the nodes to.

        :param nodes: ``MSelectionList`` of DAG objects to change the colours
            for in Maya. Defaults to using the active user selection.
        """

        def set_plugs(shape_dgnode):
            shape_dgnode.findPlug('overrideEnabled', True).setBool(True)

            if isinstance(color, int):
                shape_dgnode.findPlug('overrideColor', True).setInt(color)

            elif isinstance(color, list) or isinstance(color, tuple):
                shape_dgnode.findPlug('overrideRGBColors', True).setBool(True)
                shape_dgnode.findPlug('overrideColorR', True).setFloat(color[0])
                shape_dgnode.findPlug('overrideColorG', True).setFloat(color[1])
                shape_dgnode.findPlug('overrideColorB', True).setFloat(color[2])

        # Get user selection by default
        if not nodes:
            nodes = om2.MGlobal.getActiveSelectionList()

            if nodes.isEmpty():
                om2.MGlobal.displayWarning(
                    'Please select an object to change the colour for!'
                )
                return

        # Iterate over the selection
        fn_iterator = om2.MItSelectionList(nodes)

        while not fn_iterator.isDone():

            # Get the shape node
            dag_path = fn_iterator.getDagPath()

            if fn_iterator.getDependNode().apiTypeStr == 'kNurbsCurve':
                shape_dgnode = om2.MFnDagNode(dag_path)

                set_plugs(shape_dgnode)

            else:
                for i in range(dag_path.numberOfShapesDirectlyBelow()):
                    shape_dgnode = om2.MFnDagNode(dag_path.extendToShape(i))

                    set_plugs(shape_dgnode)

            fn_iterator.next()

        self.logger.info('Successfully changed the colours of the objects!')
