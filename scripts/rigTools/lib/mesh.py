#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module mesh: This module contains functions for dealing with meshes in Maya.
"""
from rigTools.lib.api.globals import APIFn, APIFnDagNode, APIObject
from rigTools.lib.ui import display_message


def get_mesh_from_node(node):
    """
    This function returns the mesh shape from the given **node**.

    :param node: (MObject): The node to retrieve the mesh shape for.

    :return: (MObject): The mesh shape dependency node that is currently selected.
            Returns ``None`` if no mesh shape is found.
    """
    # If transform is chosen, check for any child mesh shapes and only
    # allow for one child mesh shape
    if node.apiType() == APIFn.kTransform:
        fn_dag_node = APIFnDagNode(node)

        if fn_dag_node.childCount() > 0:
            childNode = fn_dag_node.child(0)

            if childNode.apiType() == APIFn.kMesh:
                node = childNode
            else:
                display_message(
                    'You need to select a transform that has a '
                    'single mesh shape!', message_type='warning')
                return
        else:
            display_message('Your transform must have at least 1 child mesh '
                           'shape!', message_type='warning')
            return

        return node

    elif node.apiType() == APIFn.kMesh:
        return node
    else:
        return


def get_mesh_from_selection_list(selection_list):
    """
    This function returns the current mesh from the given selection list.

    :param selection_list: (MSelectionList): The selection list to retrieve the
            mesh shapes for.

    :return: (MObject): The mesh dependency node that is currently selected.

    """
    if not selection_list.length() == 1:
        display_message('You need to select a single mesh object!',
                       message_type='warning')
        return
    else:
        # If transform is chosen, check for any child mesh shapes and only
        # allow for one child mesh shape
        node = APIObject()
        selection_list.getDependNode(0, node)

        return get_mesh_from_node(node)