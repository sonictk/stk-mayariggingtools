#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module callbacks: This module contains objects for dealing with customized
callbacks in Maya.
"""

import logging
import pymel.core as pm

from rigTools.lib.api.globals import APIEventMessage
from rigTools.lib.system import Singleton


class CallbackPublisher(object):
    """
    This class creates a global publisher that handles registration and managing
    callbacks for this plugin. It is intended to handle all management of
    any callback-related objects for this entire module.
    """
    __metaclass__ = Singleton

    def __init__(self):
        """
        The constructor.

        :return: ``None``
        """
        self.logger = logging.getLogger(__name__)
        self.__subscribers = {}


    def add_subscriber(self, callback_subscriber):
        """
        This method adds a subscriber to the registry.

        :param callback_subscriber: ``CallbackSubscriber`` subscriber object.
        """
        self.__subscribers[callback_subscriber] = callback_subscriber.callback


    def remove_subscriber(self, callback_subscriber):
        """
        This method removes a subscriber callback from the registry.

        :param callback_subscriber: ``CallbackSubscriber`` subscriber object.
        """
        del self.__subscribers[callback_subscriber]


class CallbackSubscriber(object):
    """
    This is the base class for a rig tools callback. It is intended to be used
    for registering callbacks within the context of the Rigging Tools plugin.
    """

    def __init__(self, callback_event, callback_fn):
        """
        Registers a callback with the given **callback type** and links it to
        the given **callback**.

        :param callback_event: ``str`` that is a valid **event** to register
            the callback for.

        :param callback_fn: ``fn`` callback that will be run when the callback
            is triggered.
        """
        self.logger = logging.getLogger(__name__)

        self._callback_type = callback_event
        self._callback_fn = callback_fn

        self._callback_id = APIEventMessage.addEventCallback(
            callback_event, callback_fn
        )
        CallbackPublisher().add_subscriber(self)


    @property
    def callback_id(self):
        """The callback ID for this registered subscriber in Maya."""
        return self._callback_id


    @property
    def callback_event(self):
        """The event that currently triggers this callback."""
        return self._callback_type


    @property
    def callback(self):
        """The callback that this subscriber is currently registered to."""
        return self._callback_fn


    @callback.setter
    def callback(self, callback_fn):
        """
        Set the subscriber to use a different callback instead when the event
        for this callback is triggered.

        :param callback_fn: ``fn`` object that will be called.
        """
        # Unregister the existing callback and register a new one
        APIEventMessage.removeCallback(self.callback_id)
        self._callback_id = APIEventMessage.addEventCallback(
            self.callback_event, callback_fn
        )
        self._callback_fn = callback_fn

        CallbackPublisher().add_subscriber(self)


    @staticmethod
    def get_callback_names():
        """
        This function shows the available callbacks that can be registered.

        :return: ``list`` of the available ``str`` names of the callbacks that
            can be registered.
        """
        return APIEventMessage.getEventNames()


    def remove_callback(self):
        """This method unregisters an existing callback."""
        try:
            APIEventMessage.removeCallback(self.callback_id)
        except RuntimeError as err:
            raise RuntimeError('Failed to remove callback!\n{0}'.format(err))


class SelectionChangedEvent(CallbackSubscriber):
    """
    This class registers a custom callback in Maya that sends an event whenever
    the user's selection changes.
    """

    def __init__(self):
        """
        The constructor.
        """

        self._event_type = 'SelectionChanged'

        super(SelectionChangedEvent, self).__init__(self._event_type, self.emit)
        self.logger = logging.getLogger(__name__)
        self.logger.debug('Registering custom callback: {0}...'
                          .format(self.__class__.__name__))


    def emit(self, event):
        """
        This is run whenever the user selection is changed.

        Returns the new selection.

        :return: ``list`` of ``nt.Transform`` objects that are currently
            selected.
        """
        return pm.ls(sl=True)