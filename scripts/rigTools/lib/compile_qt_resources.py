#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module compile_qt_resources: This module contains useful methods for converting
various types of Qt resource files to Python modules.
"""

import logging
import sys
import os
import pymel.core as pm

from pysideuic import compileUi


class ConvertUiToPy(object):
    """
    This class will open a file dialog allowing the user to choose a .ui file
    generated with Qt Designer to be converted to a .py file.
    """

    def __init__(self):
        """
        The constructor.

        :return: ``None``
        """

        self.logger = logging.getLogger(__name__)

        root_directory = pm.workspace(q=True, rootDirectory=True)

        # Get user choice of .ui file
        file_to_convert = pm.fileDialog2(
            dialogStyle=2,
            fileMode=1,
            startingDirectory=root_directory,
            fileFilter='Qt Designer (*.ui)',
            okCaption='Compile to *.py'
        )

        if not file_to_convert:
            return
        else:
            file_to_convert = file_to_convert[0]

        if not os.path.isfile(file_to_convert):
            pm.displayWarning('The file selected does not existing on the filesystem!')
            return

        output_file_path = self.convertUiToPyFile(file_to_convert)

        pm.displayInfo('Successfully compiled Qt Designer file to: {0}'
                       .format(output_file_path))


    def convertUiToPyFile(self, sourceFile, outputFile=None,
                          overwriteExisting=True, backupExisting=False ):
        """
        This method compiles the .ui files to .py files.

        :param sourceFile: ``str`` to path of source file

        :param outputFile: ``str`` to path of output file dest.

        :param overwriteExisting: ``bool`` determining if existing files
                            should be overwritten with output file

        :param backupExisting: ``bool`` determining if existing file should
                                be backed up before being overwritten

        :return: ``str`` to path of the output file.
        """

        self.logger.debug('Running file conversion for {0} to {1}...'
                     .format(sourceFile, outputFile))

        if outputFile is None:
            outputFile = sourceFile.rsplit('.')[0]+'.py'

            # check if file already exists and backup file if necessary
            if os.path.isfile(outputFile):
                self.logger.warning('File:{0} already exists...'.format(outputFile))

                if overwriteExisting:
                    self.logger.info('File: {0} will be overwritten...'.format(outputFile))

                    if backupExisting:
                        self.logger.debug('Backing up existing file before overwriting data...')
                        os.rename(outputFile, outputFile+'.bkup')
                        self.logger.debug('Renamed existing file to: {0}'.format(outputFile+'.bkup'))

                else:
                    self.logger.warning('overwriteExisting not specified, skipping file: {0}'
                                   .format(outputFile))
                    return None

        try: pyFile = open(outputFile, 'w')
        except IOError:
            self.logger.error('Unable to write to location: {0}\n{1}'
                         .format(outputFile, sys.exc_info()[0]))
            raise IOError

        self.logger.info('Attempting to convert: {0} to: \n{1} ...'
                     .format(sourceFile, outputFile))

        compileUi( sourceFile, pyFile )

        pyFile.close()

        self.logger.info('Conversion successful!\n')

        return outputFile