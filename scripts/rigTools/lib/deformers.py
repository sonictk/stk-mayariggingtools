#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" 
Module deformers: This module contains methods for working with deformer objects
inside of Maya.
"""
import logging
import os
import pymel.core as pm

from rigTools.lib.api.api_anim import APIFnSkinCluster
from rigTools.lib.api.datatypes import APIDoubleArray
from rigTools.lib.api.globals import APIGlobal, APIQuaternion, APIFnDagNode, \
    APIItDependencyGraph, APIFn, APIDagPathArray, APIDagPath, APISelectionList, \
    APIObject, APIFnSingleIndexedComponent, APIFnDGNode
from rigTools.lib.jsonutils import JSONUtils
from rigTools.lib.math import MathConstants
from rigTools.lib.metadata import add_metadata_to_node
from rigTools.lib.node import rename_nodes
from rigTools.lib.api.globals import get_api_object_from_name
from collections import namedtuple


def get_deformer_conventions():
    """
    This function gets the conventions for deformers from a JSON-formatted file.

    :return: ``dict`` of configuration data.
    """
    logger = logging.getLogger(__name__)

    config_path = os.path.join(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
        'prefs', 'deformer_conventions.json'
    )

    if not os.path.isfile(config_path):
        logger.error('### Could not find preferences file: {0}!!!'.format(config_path))
        return

    else:
        config_data = JSONUtils.read(config_path)
        return config_data


def get_skin_cluster(mesh):
    """
    This function finds the ``skinCluster`` for the given **mesh**.

    :param mesh: ``str`` The name of the mesh to find all available skinClusters for.

    :return: ``MObject`` The skinCluster that is attached to the mesh.
    """
    logger = logging.getLogger(__name__)

    dag_path = get_api_object_from_name(mesh, as_dag_path=True)
    fn_dag_node = APIFnDagNode(dag_path)
    in_mesh_plug = fn_dag_node.findPlug('inMesh')

    if in_mesh_plug.isConnected(): # TODO: Should not need to use an iterator for this
        # Walk the tree of nodes upstream until a skinCluster is found
        # since there is no guarantee that there aren't any intermediate nodes
        dg_iterator = APIItDependencyGraph(
            in_mesh_plug,
            APIFn.kInvalid,
            APIItDependencyGraph.kUpstream,
            APIItDependencyGraph.kDepthFirst,
            APIItDependencyGraph.kPlugLevel
        )
        dg_iterator.disablePruningOnFilter()

        while not dg_iterator.isDone():
            try:
                current_node = dg_iterator.currentItem()
                if current_node.apiType() == APIFn.kSkinClusterFilter:
                    # Once we find the skinCluster, stop iterating
                    return current_node

            except Exception as err:
                logger.error('Failed to find the influences for the mesh: {0}!'
                             '\n{1}'.format(mesh, err))
            finally:
                dg_iterator.next()


def get_skin_cluster_influences(mesh_name):
    """
    This function gets all the available **influences** that are currently on the
    given **mesh** object's skinCluster.

    :param mesh_name: ``str`` Name of the mesh to get the influences for.

    :return: ``MDagPathArray`` List of the influence paths that are currently
            attached to the mesh skinCluster.
    """
    logger = logging.getLogger(__name__)
    skin_cluster_node = get_skin_cluster(mesh_name)

    if skin_cluster_node:
        fn_skincluster = APIFnSkinCluster(skin_cluster_node)

        influences = APIDagPathArray()
        fn_skincluster.influenceObjects(influences)

        # Once we find the skinCluster influences, stop iterating
        return influences
    else:
        logger.error('No skinCluster could be found for the mesh: {0}!'.format(mesh_name))


def get_points_affected_by_deformer(node, mesh):
    """
    This function returns the points that are being affected by the specified
    deformer.

    :param node: ``MObject`` The deformer to find the points that are being
        affected by.

    :param mesh: ``MObject`` The mesh with the points to find.

    :return: ``namedtuple`` The ``MSelectionList``  of mesh components that are
            being affected, and a ``MDoubleArray`` of the weights that they are
            being influenced from the deformer by. The ``namedtuple`` hashes for
            accessing the results are 'points' and 'weights' respectively.
    """
    skincluster_node = get_skin_cluster(mesh)

    deformer_dag_path = APIDagPath()
    fn_dag_node = APIFnDagNode(node)
    fn_dag_node.getPath(deformer_dag_path)

    fn_skincluster = APIFnSkinCluster(skincluster_node)
    points_affected = APISelectionList()
    point_weights = APIDoubleArray()

    fn_skincluster.getPointsAffectedByInfluence(
        deformer_dag_path, points_affected, point_weights)

    result = namedtuple('PointsAffected', 'points weights')
    return result(points=points_affected, weights=point_weights)


def is_deformer_affecting_point(deformer, point_node, point_idx, mesh):
    """
    This function checks if the given **deformer** is influencing a given point.

    :param deformer:  (MObject): The deformer node to check.

    :param point_node: (MObject): The point node to check.

    :param point_idx: (int): The index of the component to check if is being affected.

    :param mesh: ``MObject`` The object representation of the mesh that the deformer
            and skinCluster are associated with.

    :return: (bool): ``True`` if the specified deformer is influencing the point
        given, ``False`` otherwise.
    """
    # Get the points that the deformer is currently affecting
    point_data = get_points_affected_by_deformer(deformer, mesh)
    points_affected = point_data.points
    num_of_pts_affected = point_data.weights.length()

    if num_of_pts_affected > 0:
        # For each influenced point, check if the vertices match
        mesh_dag_path = APIDagPath()
        component_node = APIObject()
        points_affected.getDagPath(0, mesh_dag_path, component_node)
        fn_component_affected = APIFnSingleIndexedComponent(component_node)

        for idx in xrange(num_of_pts_affected):
            component_idx = fn_component_affected.element(idx)

            if component_idx == point_idx:
                return True

        # If no match was found
        return False

    else:
        return False


def find_deformers_affecting_point(point_node, point_idx, mesh):
    """
    This function finds all the deformers that are currently driving a given
    **point**..

    :param point_node: (MObject): The node of the current point component object.

    :param point_idx: (int): The index of the component to find the deformers that
            are currently affecting it.

    :param mesh: ``MObject`` The object representation of the mesh that has
        deformers to be searched through for.

    :return:  (list): Returns a ``list`` of the ``MObject`` deformer objects
        that are affecting the point.
    """
    logger = logging.getLogger(__name__)

    mesh_name = APIFnDGNode(mesh).name()

    # Get the skinCluster to find the driving deformers for
    influences = get_skin_cluster_influences(mesh_name)
    if not influences:
        logger.info('No deformer influences were found for the point on mesh: '
                    '{0}!'.format(mesh_name))
        return

    driving_deformers = APISelectionList()

    # NOTE: Do this instead of iterating over MDagPathArray like a
    # normal Python list to avoid Maya crashing
    for influence_idx in xrange(influences.length()):
        influence = influences[influence_idx]

        # Find the points that each influence affects and see if it affects the
        # given one
        if is_deformer_affecting_point(
            deformer=influence,
            point_node=point_node,
            point_idx=point_idx,
            mesh=mesh
        ):
            driving_deformers.add(influence)

    return driving_deformers


class Deformers(object):
    """
    This class contains useful methods for working with and modifying deformer
    objects in Maya.
    """

    def __init__(self):
        """
        The constructor.
        """
        self.logger = logging.getLogger(__name__)

        # Define Maya specific enums for deformer metadata and indexes
        self.enum_sides = {
            'Center' : 0,
            'Left'   : 1,
            'Right'  : 2,
            None     : 3
        }

        self.enum_context_types = {
            None            : 0,
            'Root'          : 1,
            'Hip'           : 2,
            'Knee'          : 3,
            'Foot'          : 4,
            'Toe'           : 5,
            'Spine'         : 6,
            'Neck'          : 7,
            'Head'          : 8,
            'Collar'        : 9,
            'Shoulder'      : 10,
            'Elbow'         : 11,
            'Hand'          : 12,
            'Finger'        : 13,
            'Thumb'         : 14,
            'PropA'         : 15,
            'PropB'         : 16,
            'PropC'         : 17,
            'Other'         : 18,
            'Index Finger'  : 19,
            'Middle Finger' : 20,
            'Ring Finger'   : 21,
            'Pinky Finger'  : 22,
            'Extra Finger'  : 23,
            'Big Toe'       : 24,
            'Index Toe'     : 25,
            'Middle Toe'    : 26,
            'Ring Toe'      : 27,
            'Pinky Toe'     : 28,
            'Foot Thumb'    : 29
        }


    @staticmethod
    def toggle_label_display(deformers, toggle=True, state=True):
        """
        This method toggles display of the labels for the given deformer objects.

        :param deformers: ``list`` of deformer objects to toggle label display for.

        :param state:  ``bool`` indicating what the state should be set to.
            Ignored if ``toggle`` is set to ``True``.

        :param toggle: ``bool`` indicating if the state should be toggled or not.
        :return:
        """

        logger = logging.getLogger(__name__)

        for deformer in deformers:

            if deformer.hasAttr('drawLabel'):
                if toggle:
                    if deformer.getAttr('drawLabel') == 0:
                        deformer.setAttr('drawLabel', 1)
                    else:
                        deformer.setAttr('drawLabel', 0)
                else:
                    deformer.setAttr('drawLabel', state)

            else:
                logger.warning('# {0} does not have a label associated with it!'
                               .format(deformer))


    @staticmethod
    def rename_deformers(
        nodes=None,
        base_name='joint',
        deformer_type='joint',
        prefix='jnt',
        suffix='',
        separator='_',
        renumber=True,
        number_from=0
    ):
        """
        This method renames deformers.

        :param renumber: ``bool`` indicating if the deformers should also be
            re-numbered.

        :param number_from: ``int`` that the re-numbering will start from. Is
            ignored if ``renumber`` is set to ``False``.

        :param nodes: ``list`` of ``nt.Transform`` nodes to rename.

        :param separator: ``str`` that will be used to separate the prefix/base_name
            /suffix of the joint names.

        :param base_name: ``str`` that will act as the base name for the joints to
            be renamed as.

        :param deformer_type: ``str`` indicating the types of nodes that will be
            filtered for the current selection by.

        :param prefix: ``str`` that will be prefixed to the base_name of all joints.

        :param suffix: ``str`` that will be suffixed to the base_name of all joints.

        :return: ``None``
        """

        rename_nodes(
            nodes=nodes,
            filter_type=deformer_type,
            base_name=base_name,
            prefix=prefix,
            suffix=suffix,
            separator=separator,
            renumber=renumber,
            number_from=number_from
        )


    @staticmethod
    def orient_deformers(
            deformers,
            aim_vector=None,
            up_vector=None,
            world_up_vector=None,
            orient_to_world=False
    ):
        """
        This method orients the given deformers' local rotation axes while
        also zero-ing out any existing unwanted local rotation transformations.

        :param deformers: ``list`` of deformer objects to orient.

        :param aim_object: ``nt.Transform`` object to use as the reference
            object to calculate the up axis for.

        :param orient_to_world: ``bool`` indicating if the deformers' axes
            should be oriented to the world axes.

        :param up_axis: ``str`` that is the axis to use as the up-axis for the
            joints being oriented. Valid values are 'x', 'y' and 'z'. Can be
            negated by adding a minus '-' symbol in front of the axis value.

        :param aim_axis: ``str`` that is the axis to use as the aim-axis for the
            joints being oriented. Valid values are 'x', 'y' and 'z'. Can be
            negated by adding a minus '-' symbol in front of the axis value.
        """
        _math_constants = MathConstants()

        # todo: this isn't working, also, should be refactored to orient dfm method instead
        for deformer in deformers:

            # Combine any rotation and rotate axis values to joint orient values
            if deformer.type() == 'joint':
                joint_rotation = deformer.getRotation()
                joint_rotate_axis_rotation = deformer.getRotateAxis(objectSpace=True)
                joint_orientation = deformer.getOrientation().normalizeIt()\
                                        .asEulerRotation() * 180 / _math_constants.pi \
                                    + joint_rotation \
                                    + joint_rotate_axis_rotation

                pm.makeIdentity(deformer, rotate=True, apply=True)

                deformer.setAttr('jointOrientX', joint_orientation[0])
                deformer.setAttr('jointOrientY', joint_orientation[1])
                deformer.setAttr('jointOrientZ', joint_orientation[2])


    def relabel_deformers(
        self,
        deformers=None,
        deformer_type='joint',
        label_base_name='',
        side='',
        draw_labels=False
    ):
        """
        This method re-labels deformers.

        :param deformers: ``list`` of ``nt.Transform`` nodes to rename.

        :param label_base_name: ``str`` that will act as the base name for the
            deformers to be renamed as.

        :param deformer_type: ``str`` indicating the types of nodes that will be
            filtered for the current selection by.

        :param side: ``str`` that will be set as the side label of the deformer.

        :return: ``None``
        """
        # todo: make this work for any sort of deformer, not just joints
        logger = logging.getLogger(__name__)

        if not deformers:
            deformers = pm.ls(sl=True, typ=deformer_type)

        if side in self.enum_sides.keys():
            side = self.enum_sides[side]
        else:
            # No side information
            side = 3

        for deformer in deformers:
            # Set type to 'other' and set the label name
            pm.setAttr(deformer.name() + '.type', 18)
            pm.setAttr(deformer.name() + '.otherType', label_base_name)
            pm.setAttr(deformer.name() + '.side', side)

            if draw_labels:
                pm.setAttr(deformer.name() + '.drawLabel', 1)
            else:
                pm.setAttr(deformer.name() + '.drawLabel', 0)

        logger.debug('Successfully re-labeled the deformers!')
        APIGlobal().displayInfo('Successfully re-labeled the deformers!')


    def create_deformers(
            self,
            guide_objects,
            deformer_type='joint',
            side='',
            context_type='',
            draw_labels=False,
            aim_axis='y',
            up_axis='x',
            world_up_axis='y',
            base_name='jnt_',
            parent_in_sequence=False
    ):
        """
        This method creates deformer objects at the given locations.

        :param parent_in_sequence: ``bool`` indicating if deformers should be
            parented in sequence as they are being created.

        :param guide_objects: ``list`` of ``nt.Transform`` objects that the
            deformers will be created at their positions of.

        :param base_name: ``str`` that will be used to format the name of the
            deformers created.

        :param aim_axis: ``str`` that is the local aim axis of each deformer.

        :param up_axis: ``str`` that is the local secondary axis of each deformer.

        :param world_up_axis: ``str`` that is the reference up axis of how to
            orient the other two axes.

        :param deformer_type: ``str`` that is the context_type of deformer to
            be created.

        :param side: ``str`` that will be applied as metadata to the deformers
            of which side they belong to. Used in other utility functions such as
            mirroring skin weights, copying skin weights etc.

        :param context_type: ``str`` that will be applied as metadata to the
            deformers as to what context_type of deformer they are.

        :param draw_labels: ``bool`` indicating if the created deformers should
            have their labels visible.

        :return: ``list`` of the deformers that were created.
        """

        # Create deformer at each guide transform position
        deformers = []

        for idx, guide in enumerate(guide_objects):

            # Create the deformer at the guide object's position
            guide_xform = pm.datatypes.TransformationMatrix(
                guide.getMatrix(worldSpace=True)
            )

            guide_rotation = guide_xform.getRotationQuaternion()
            guide_translation = guide_xform.getTranslation(space='world')

            # Factor out scale
            deformer_xform = pm.datatypes.TransformationMatrix()
            deformer_xform.setRotationQuaternion(
                guide_rotation[0],
                guide_rotation[1],
                guide_rotation[2],
                guide_rotation[3],
            )
            deformer_xform.setTranslation(guide_translation, space='world')

            if deformer_type == 'locator':
                base_name += 'Shape'

            deformer = pm.createNode(deformer_type, n=base_name)

            # Get the transform node if necessary (e.g. locators)
            if deformer.getParent():
                deformer = deformer.getParent()

            deformer.setTransformation(deformer_xform)

            # Parent the deformer in sequence if necessary
            if parent_in_sequence and idx != 0:
                self.logger.debug('Parenting...{0} {1}'.format(deformer, deformers[idx-1]))
                pm.parent(deformer, deformers[idx-1])

            # Set deformer metadata
            if side:
                # Set Maya metadata if applicable
                if deformer.hasAttr('side') and side in self.enum_sides.keys():
                    deformer.setAttr('side', self.enum_sides[side])

                # Add custom metadata attribute as well
                add_metadata_to_node(deformer.__apimobject__(), 'rigSide', side)

            if draw_labels:
                deformer.setAttr('drawLabel', 1)

            deformers.append(deformer)

        return deformers


    def insert_deformers(self, start, end, number=1, deformer_type='joint'):
        """
        This method inserts deformers between the given ``start`` and ``end`` nodes.

        :param start: ``nt.Transform`` Node that will be the
            parent for the inserted deformers.

        :param end: ``nt.Transform`` Node that will be the end
            point for the inserted deformers.

        :param number: ``int`` that will be the number of deformers inserted.

        :param deformer_type: ``str`` that will be the type of the deformer to
            be inserted.
        :return:
        """

        logger = logging.getLogger(__name__)

        if not start.nodeType() == end.nodeType():
            logger.error('### Both deformers must be of the same type!!!')
            return
        else:
            # Extract and form the new basename for the deformers to be created
            basename = start.name()

        # Check if inserting within a chain or as floating deformers
        if end not in start.getChildren():
            logger.debug('Inserting {0} floating {1}s'.format(number, deformer_type))

            # Get positions of start and end point and find new positions to
            # create new deformers at
            pos_start = start.getTranslation(space='world')
            pos_end = end.getTranslation(space='world')

            # Get orientation of start and end point and interpolate to form
            # new orientations for deformers
            rot_start = APIQuaternion(start.getTransformation().getRotationQuaternion())
            rot_end = APIQuaternion(end.getTransformation().getRotationQuaternion())

            # Create the deformers
            for i in xrange(number):
                # Lerp the position and slerp rotation for the deformers
                pos_deformer = 1/number * pos_end + (1 - 1/number) * pos_start
                rot_deformer = APIQuaternion.slerp(rot_start, rot_end, 1/number)

                deformer = pm.createNode(deformer_type, n='{0}_inserted_{1}'.format(basename, number))

                # Account for if the deformer has a parent xform node
                if deformer.getParent():
                    deformer = deformer.getParent()

                deformer.setTranslation(pos_deformer)
                deformer.setRotationQuaternion(*rot_deformer)

        else:
            logger.debug('Inserting {0} {1}s'.format(number, deformer_type))
