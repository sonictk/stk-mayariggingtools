#!/usr/bin/env bash

# Generate Documentation bash script

echo "----------------------------------------------------------"
echo "Generating user documentation"
echo "----------------------------------------------------------"

if [ SPHINXBUILD == "" ]
  then
    export SPHINXBUILD=sphinx-build
fi

export BUILDDIR=$PWD

echo "Removing existing build directories..."
