@echo off

REM Navigate to the current working directory
cd "%~dp0"

echo. ----------------------------------------------------------
echo. Generating API documentation
echo. ---------------------------------------------------------- 

REM Remove existing documentation RST files previously built
if exist "%~dp0source\devGuide\api" del /s /q /f "%~dp0source\devGuide\api"
if errorlevel 1 exit /b 1

REM Generate new RST files
sphinx-apidoc --force --follow-links --no-toc -d 2 -o "%~dp0source/devGuide/api" ../scripts/rigTools/lib
sphinx-apidoc --force --follow-links --no-toc -d 2 -o "%~dp0source/devGuide/api" ../scripts/rigTools/rigging
if errorlevel 1 exit /b 1

echo. Successfully generated API documentation!