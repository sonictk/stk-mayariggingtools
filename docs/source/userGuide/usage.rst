..
    :copyright: Copyright (c) 2015 sonictk-Frostburn

.. _usage:

#####
Usage
#####


Getting Started
===============

After installation, you should be able to see the new tools show up in the Maya
UI by going to the **sonictk** menubar item.


Tools
=====

Below is a list of all the available **tools** currently available for use.

.. attention::

   The following information is outdated.