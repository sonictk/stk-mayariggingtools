.. _installation:

############
Installation
############

Requirements
============

You will require Maya 2013 or later to use this toolkit.

.. note::

    This toolkit was developed using Maya 2016 SP3.


Installation
============

Automatic Installation
----------------------

Run the ``setup.exe`` file and follow the on-screen instructions.

After the process finishes, you should now be able to use the Maya Rigging Tools.

You can verify that the installation was performed correctly by opening Maya and
opening the **Script Editor**. You should be able to view debug information from
the console this way, and the line:

``__main__ : Launching Rigging Tools (sonictk)...``

Would indicate that the plugin was loaded successfully.

.. _manual-installation:

Manual Installation
-------------------

.. attention::

    The following information needs to be updated.

If you have access to the git repository:

``git clone git@bitbucket.org:sonictk/stkmaritools.git``

You will need to build the installer from source as no binaries are provided.
The required dependencies are in ``requirements.txt`` and can be used by
``pip`` to install to a python ``virtualenv``.

.. tip::
    After creating a ``virtualenv``, you can use ``pip`` to automatically
    install the dependencies listed in the ``requirements.txt`` file, such as:

    ``pip install -r requirements.txt``

Alternatively, you can simply place the repository in a folder of your choice
set your ``MARI_SCRIPT_PATH`` environment variable to point to that directory.
See :ref:`setting-environment-variables` for more details.


.. _setting-environment-variables:

Setting the environment variables
=================================

.. attention::

    The following information needs to be updated.


.. tip::

    If you have existing scripts installed (like the Mari Extensions Pack)
    installed, you might want to also include the original path in your modified
    environment variable. For example:

    ``MARI_SCRIPT_PATH=C:\Programs\stkmaritools;C:\Users\<username>\Documents\Mari\Scripts``

Windows
-------

Run the following command from a command prompt that has Administrator privileges:

``setx MARI_SCRIPT_PATH <installation_dir>``

where ``<installation_dir>`` is the location of the ``stkmaritools`` directory.

.. tip::

    You can also set environment variables for a user profile using Control Panel.
    These changes will affect the environment for all applications run by that user.
    See your Windows documentation for more information.


OS X / Linux
------------

The procedure for setting the envrionment variable depends on what your default
shell is.

To get the name of your shell, launch a **Terminal** and type ``echo $SHELL``.
This should return the type of shell you are using.

If your shell is a ``csh`` or ``tcsh`` shell, add the following command to the
``.cshrc`` or ``.tcshrc`` file in your ``$HOME`` directory:

``setenv MARI_SCRIPT_PATH <installation_dir>``

where ``<installation_dir>`` is the location of the ``stkmaritools`` directory.

If your shell is a ``bash`` or ``ksh`` shell, add the following command to the
``.bashrc`` or ``.kshrc`` file in your ``$HOME`` directory:

``export MARI_SCRIPT_PATH <installation_dir>``

where ``<installation_dir>`` is the location of the ``stkmaritools`` directory.


Uninstallation
==============

You can run the uninstaller by using the shortcut in the Start menu to
remove the Maya Rigging Tools.


Troubleshooting
===============

Send a detailed description of your issue using the contact information as
described on the contact page, :ref:`about-the-author`.