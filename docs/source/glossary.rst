..
    :copyright: Copyright (c) 2015 sonictk-Frostburn

.. _glossary:


********
Glossary
********

This article contains a listing of all shorthand terms used in the documentation
and attempts to give a brief explanation of what each of them means.

.. glossary::
    :sorted:

    API
        Application Programming Interface. Refers to a simplified set of
        classes/functions/methods that an end-user can make use of to
        perform complex operations, while being abstracted from the
        underlying complexity of the system.

    Environment
        A structure where information about all documents under the root is
        saved, and used for cross-referencing.  The environment is pickled
        after the parsing stage, so that successive runs only need to read
        and parse new and changed documents.

    JSON
        `JavaScript Object Notation <http://json.org/>`_

         JSON (JavaScript Object Notation) is a lightweight data-interchange format.
         It is easy for humans to read and write. It is easy for machines to parse and generate.
         It is based on a subset of JavaScript. JSON is a text format that is completely language
         independent but uses conventions that are familiar to programmers of the
         C-family of languages.

    PEP-8
        `PEP 0008 - Style Guide for Python Code <https://www.python.org/dev/peps/pep-0008/>`_

        A standardized set of guidelines for coding in the Python programming
        language that have been generally accepted by the programming community
        as good rules to adhere to when formatting code.

    pylint
        `Pylint website <http://www.pylint.org/>`_

        ``pylint`` is a tool for finding bugs and style problems in Python source code.
        It finds problems that are typically caught by a compiler for less dynamic
        languages like C and C++. Because of the dynamic nature of Python, some
        warnings may be incorrect; however, spurious warnings should be fairly
        infrequent.

    Source directory
        The directory which, including its subdirectories, contains all
        source files for one project.

    ReST
        `ReST Sphinx standard <http://sphinx-doc.org/rest.html>`_

        ReStructuredText format. A human-readable markup language that is used for
        generating rich content from simplified markup. It is the language used to
        create this documentation.

    YAML
        `YAML Official webiste <http://yaml.org/>`_

        A recursive acronym that stands for **YAML Ain't Markup Language**. YAML is
        a human friendly data serialization standard for all programming languages. It
        offers several advantages over JSON such as better readability, less risk of
        malicious client-side behaviour and enhanced support for generic Python objects
        through PyYAML.