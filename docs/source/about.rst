.. _about:

############################
About the Maya Rigging Tools
############################

Description
===========

This is the documentation for the Maya Rigging Tools developed by sonictk.

These tools are a collection of useful utilties that add much-needed basic
functionality that is not present in Maya, such as control creation, metadata
management and a better object renamer, among many others. It also contains
several frameworks to allow for easy extension of the toolkit with additional
functionality.


Release Notes
=============

Please refer to the :ref:`release` for more details.


.. _about-the-author:

About the author
================

Siew Yi Liang

Please contact me via email at:

admin@sonictk.com