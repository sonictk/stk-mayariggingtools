# -*- coding: utf-8 -*-
#
# documentation build configuration file, created by
# sphinx-quickstart on Sat Feb 14 22:50:37 2015.
#
# This file is execfile()d with the current directory set to its
# containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.

import sys
import os

class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout

        logfile_path = os.path.join(
            os.path.dirname(
                os.path.dirname(
                    os.path.abspath(__file__))), 'build', 'buildLog.txt')
        
        if not os.path.isdir(os.path.dirname(logfile_path)):
            os.makedirs(os.path.dirname(logfile_path))

        self.log = open(logfile_path, 'a')

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)  

sys.stdout = Logger()


# Append sources dir to path
sourcesPath = \
    os.path.join(
        os.path.dirname(
            os.path.dirname(
                os.path.dirname(os.path.abspath(__file__)
                                )
            )
        ),
        'scripts'
    )

parent_module_path = os.path.join(
    os.path.dirname(
        os.path.dirname(
            os.path.dirname(os.path.abspath(__file__)))), 'scripts', 'rigTools')

sys.stdout.write('Appending {0} to PATH...'.format(sourcesPath))
sys.stdout.write('Appending {0} to PATH...'.format(parent_module_path))

sys.path.append(sourcesPath)
sys.path.append(parent_module_path)


from mock import Mock as MagicMock


class Mock(MagicMock):
    # noinspection PyUnresolvedReferences
    __all__ = [
        'QApplication',
        'pyqtSignal',
        'pyqtSlot',
        'QObject',
        'QAbstractItemModel',
        'QModelIndex',
        'QTabWidget',
        'QWebPage',
        'QTableView',
        'QWebView',
        'QAbstractTableModel',
        'Qt',
        'QWidget',
        'QPushButton',
        'QDoubleSpinBox',
        'QListWidget',
        'QDialog',
        'QSize',
        'QTableWidget',
        'QMainWindow',
        'QTreeWidget',
        'QAbstractItemDelegate',
        'QColor',
        'QGraphicsItemGroup',
        'QGraphicsItem',
        'QGraphicsPathItem',
        'QGraphicsTextItem',
        'QGraphicsRectItem',
        'QGraphicsScene',
        'QGraphicsView',
        'PyQt5',
        'PyQt4',
        'pymel.core',
        'maya.cmds',
        'shiboken',
        'maya.app.general.mayaMixin',
        'maya.api.OpenMayaAnim',
        'maya.api.OpenMaya',
        'maya.api.OpenMayaRender',
        'maya.api.OpenMayaUI'        
        'maya.OpenMayaAnim',
        'maya.OpenMaya',
        'maya.OpenMayaRender',
        'maya.OpenMayaUI'
    ]

    def __init__(self, *args, **kwargs):
        super(Mock, self).__init__()


    @classmethod
    def __getattr__(cls, name):
        if name in ('__file__', '__path__'):
            return os.devnull
        else:
            return Mock()


    def __getitem__(self, *args, **kwargs):
        return Mock()


MOCK_MODULES = [
    'pygtk', 
    'gtk', 
    'gobject', 
    'numpy', 
    'pandas',
    'PyQt5', 
    'PyQt4', 
    'maya.cmds',
    'pymel.core', 
    'shiboken',
    'maya.app.general.mayaMixin',
    'maya.api.OpenMayaAnim',
    'maya.api.OpenMaya',
    'maya.api.OpenMayaRender',
    'maya.api.OpenMayaUI'        
    'maya.OpenMayaAnim',
    'maya.OpenMaya',
    'maya.OpenMayaRender',
    'maya.OpenMayaUI'

]

sys.modules.update((mod_name, Mock) for mod_name in MOCK_MODULES)

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#sys.path.insert(0, os.path.abspath('.'))

# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    'sphinxarg.ext',
    'lowdown'
]

autodoc_mock_imports = [
    'pygtk',
    'gtk',
    'gobject',
    'numpy',
    'pandas',
    'PyQt5',
    'PyQt4',
    'pymel.core',
    'maya.cmds',
    'shiboken',
    'maya.app.general.mayaMixin',
    'maya.api.OpenMayaAnim',
    'maya.api.OpenMaya',
    'maya.api.OpenMayaRender',
    'maya.api.OpenMayaUI'        
    'maya.OpenMayaAnim',
    'maya.OpenMaya',
    'maya.OpenMayaRender',
    'maya.OpenMayaUI'
]

autodoc_default_flags = ['members', 'undoc-members']
autodoc_member_order = 'bysource'

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix of source filenames.
source_suffix = '.rst'

todo_include_todos = True

# The encoding of source files.
#source_encoding = 'utf-8-sig'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = u'Maya Rigging Tools (sonictk)'
copyright = u'2015, Siew Yi Liang'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = '1.0'
# The full version, including alpha/beta/rc tags.
release = '1.0'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#language = None

# Document both the class and the __init__ method's docstrings
autoclass_content = 'both'

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#today = ''
# Else, today_fmt is used as the format for a strftime call.
#today_fmt = '%B %d, %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = ['_build']

# The reST default role (used for this markup: `text`) to use for all
# documents.
#default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
#add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
add_module_names = False

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
#show_authors = False

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# A list of ignored prefixes for module index sorting.
#modindex_common_prefix = []

# If true, keep warnings as "system message" paragraphs in the built documents.
#keep_warnings = False


# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
# html_theme = 'default'

# Sphinx RTD theme
# on_rtd is whether we are on readthedocs.org, this line of code grabbed from docs.readthedocs.org
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'

if not on_rtd:  # only import and set the theme if we're building docs locally
    try:
        import sphinx_rtd_theme
        html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

    except:
        html_theme_path = ['_themes', ]

    html_theme = 'sphinx_rtd_theme'

# otherwise, readthedocs.org uses their theme by default, so no need to specify it

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#html_theme_options = {}

# Add any paths that contain custom themes here, relative to this directory.
#html_theme_path = []

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
#html_title = None

# A shorter title for the navigation bar.  Default is the same as html_title.
#html_short_title = None

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
html_logo = './_resources/logo.png'

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
html_favicon = './_static/favicon.ico'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# Add any extra paths that contain custom files (such as robots.txt or
# .htaccess) here, relative to this directory. These files are copied
# directly to the root of the documentation.
#html_extra_path = []

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
html_last_updated_fmt = '%b %d, %Y'

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
#html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
#html_sidebars = {}

# Additional templates that should be rendered to pages, maps page names to
# template names.
#html_additional_pages = {}

# If false, no module index is generated.
html_domain_indices = True

# If false, no index is generated.
html_use_index = True

# If true, the index is split into individual pages for each letter.
html_split_index = True

# If true, links to the reST sources are added to the pages.
html_show_sourcelink = False

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
html_show_sphinx = False

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
html_show_copyright = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# This is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = None

# Output file base name for HTML help builder.
htmlhelp_basename = 'stk-MayaRiggingTools'

# Allow for customizations of theme
html_style = 'css/styleoverrides.css'

# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'stk-MayaRiggingTools', u'Maya Rigging Tools Documentation',
     [u'Siew Yi Liang'], 1)
]

# If true, show URL addresses after external links.
#man_show_urls = False
