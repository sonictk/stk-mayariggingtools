.. Maya Rigging Tools documentation master file, created by
   sphinx-quickstart on Sat Feb 14 22:50:37 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Maya Rigging Tools (sonictk) Documentation
==========================================

This is the documentation for the Maya Rigging Tools by Siew Yi Liang.

If you're seeing this page after running the installation, you should take
a look at the :ref:`userGuide` section.

If you're a developer looking to extend this application, please refer
to the :ref:`developerGuide` instead.


.. _userGuide:

User Guide
==========

.. toctree::
   :maxdepth: 2

   about
   userGuide/installation
   userGuide/usage


.. _developerGuide:

Developer Guide
===============

.. toctree::
   :maxdepth: 2

   devGuide/developerGuide


Release Notes
=============

.. toctree::
   :maxdepth: 2

   release


Glossary
========

.. toctree::
   :maxdepth: 1

   glossary

License
=======

.. toctree::
   :maxdepth: 1

   license


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`