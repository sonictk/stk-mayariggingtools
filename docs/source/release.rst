.. _release:

Release Notes
=============

.. release:: 1.0
    :date: 2015-09-10

    .. change:: new
        :tags: documentation

        Added this documentation.