#################
Developer's Guide 
#################

This is the developer's guide for the Maya Rigging Tools.

.. toctree::
    :maxdepth: 2

    getting-started
    codingStyleGuide


.. _api-reference:

API Reference
=============

This is the API reference for the modules used in the Mari Tools.


Maya Rigging Tools Libraries
----------------------------

Libraries
^^^^^^^^^

.. toctree::
   :maxdepth: 2
   :glob:

   api/lib*

