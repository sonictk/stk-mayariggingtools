lib.api package
===============

Submodules
----------

lib.api.api_anim module
-----------------------

.. automodule:: lib.api.api_anim
    :members:
    :undoc-members:
    :show-inheritance:

lib.api.datatypes module
------------------------

.. automodule:: lib.api.datatypes
    :members:
    :undoc-members:
    :show-inheritance:

lib.api.globals module
----------------------

.. automodule:: lib.api.globals
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: lib.api
    :members:
    :undoc-members:
    :show-inheritance:
