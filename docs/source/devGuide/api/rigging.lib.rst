rigging.lib package
===================

Submodules
----------

rigging.lib.curve_controls module
---------------------------------

.. automodule:: rigging.lib.curve_controls
    :members:
    :undoc-members:
    :show-inheritance:

rigging.lib.shapes module
-------------------------

.. automodule:: rigging.lib.shapes
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: rigging.lib
    :members:
    :undoc-members:
    :show-inheritance:
