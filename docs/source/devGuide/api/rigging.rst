rigging package
===============

Subpackages
-----------

.. toctree::

    rigging.lib
    rigging.ui

Submodules
----------

rigging.blendshape_symmetrizer module
-------------------------------------

.. automodule:: rigging.blendshape_symmetrizer
    :members:
    :undoc-members:
    :show-inheritance:

rigging.change_node_color module
--------------------------------

.. automodule:: rigging.change_node_color
    :members:
    :undoc-members:
    :show-inheritance:

rigging.control_shape_creator module
------------------------------------

.. automodule:: rigging.control_shape_creator
    :members:
    :undoc-members:
    :show-inheritance:

rigging.create_null_groups module
---------------------------------

.. automodule:: rigging.create_null_groups
    :members:
    :undoc-members:
    :show-inheritance:

rigging.deformer_creator module
-------------------------------

.. automodule:: rigging.deformer_creator
    :members:
    :undoc-members:
    :show-inheritance:

rigging.deformer_orienter module
--------------------------------

.. automodule:: rigging.deformer_orienter
    :members:
    :undoc-members:
    :show-inheritance:

rigging.group_freeze module
---------------------------

.. automodule:: rigging.group_freeze
    :members:
    :undoc-members:
    :show-inheritance:

rigging.helpers module
----------------------

.. automodule:: rigging.helpers
    :members:
    :undoc-members:
    :show-inheritance:

rigging.rename_nodes module
---------------------------

.. automodule:: rigging.rename_nodes
    :members:
    :undoc-members:
    :show-inheritance:

rigging.tension_deformer module
-------------------------------

.. automodule:: rigging.tension_deformer
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: rigging
    :members:
    :undoc-members:
    :show-inheritance:
