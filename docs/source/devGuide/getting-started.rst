..
    :copyright: Copyright (c) 2015 sonictk-Frostburn

.. _getting-started:


***************
Getting Started
***************

In order to work on the Maya Rigging Tools, you will need a
working knowledge of Python. If you are already familiar with Python,
please take a moment to review the :ref:`codingStyleGuide`.

First, please follow the instructions for :ref:`manual-installation` in
order to get a working copy of the source.


Making your first plugin using the Maya Rigging Tools
=====================================================


Registering the plugin
======================


What's next?
============

Have a look at the :ref:`api-reference` to see what other plugins are available,
what other libraries are available, and to see where best you could help to
contribute to the toolkit!