@echo off
REM 
REM Generate documentation batch script 
REM

echo. ----------------------------------------------------------
echo. Generating user documentation
echo. ---------------------------------------------------------- 

if "%SPHINXBUILD%" == "" (
    set SPHINXBUILD=sphinx-build
)

set BUILDDIR=%~dp0

echo. Removing existing build directories...
if exist "%~dp0build" rmdir "%~dp0build" /S /Q
if errorlevel 1 exit /b 1

for %%A in ("%~dp0\..") do set "SOURCEDIR=%%~fA"

if not exist "%BUILDDIR%source/_static" mkdir "%BUILDDIR%source/_static"

set ALLSPHINXOPTS=-d "%BUILDDIR%doctrees" %SPHINXOPTS% "%BUILDDIR%source"
set I18NSPHINXOPTS=%SPHINXOPTS% "%BUILDDIR%source"

echo. reading sources from: %SOURCEDIR%
echo. building to: %BUILDDIR%build/html
    
%SPHINXBUILD% -b html -d "%BUILDDIR%build\doctrees" "%BUILDDIR%source" "%BUILDDIR%build\html"
if errorlevel 1 exit /b 1
echo.
echo.Build finished. The HTML pages are in %BUILDDIR%html.
echo Reached end of documentation generation script!

goto end

:end
