#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Module measureNode: This module is a Maya Plugin that calculates the 
midpoint and the distance between two nodes.
"""

import maya.OpenMayaMPx as om_mpx
import maya.OpenMaya as om
 
class MeasureNode(om_mpx.MPxNode):
    kPluginNodeId = om.MTypeId(0x00124f00)
 
    pos1 = om.MObject()
    pos2 = om.MObject()
    midpoint = om.MObject()
    distance = om.MObject()
 
    def __init__(self):
        super(MeasureNode, self).__init__(self)


    def compute(self, plug, data):
        p1 = data.inputValue(MeasureNode.pos1).asDouble3()
        p2 = data.inputValue(MeasureNode.pos2).asDouble3()
 
        mid = ((p1[0]+p2[0])/2, (p1[1]+p2[1])/2, (p1[2]+p2[2])/2)
        d = ((p1[0]-p2[0])**2+(p1[1]-p2[1])**2+(p1[2]-p2[2])**2)**.5
 
        aOutput = data.outputValue(MeasureNode.midpoint)
        aOutput.set3Double(mid[0], mid[1], mid[2])
 
        bOutput = data.outputValue(MeasureNode.distance)
        bOutput.setFloat(d)
        data.setClean(plug)


def creator():
    return om_mpx.asMPxPtr(MeasureNode()) 


def initialize():
    nAttr = om.MFnNumericAttribute()
    MeasureNode.midpoint = nAttr.create("midpoint","mid", om.MFnNumericData.k3Double,0.0)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    MeasureNode.addAttribute(MeasureNode.midpoint)
 
    MeasureNode.distance = nAttr.create("distance","dist", om.MFnNumericData.kFloat,0.0)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    MeasureNode.addAttribute(MeasureNode.distance)
 
    MeasureNode.pos1 = nAttr.create("pos1","in1", om.MFnNumericData.k3Double,0.0)
    MeasureNode.addAttribute(MeasureNode.pos1)
    MeasureNode.attributeAffects(MeasureNode.pos1, MeasureNode.midpoint)
    MeasureNode.attributeAffects(MeasureNode.pos1, MeasureNode.distance)
 
    MeasureNode.pos2 = nAttr.create("pos2","in2", om.MFnNumericData.k3Double,0.0)
    MeasureNode.addAttribute(MeasureNode.pos2)
    MeasureNode.attributeAffects(MeasureNode.pos2, MeasureNode.midpoint)
    MeasureNode.attributeAffects(MeasureNode.pos2, MeasureNode.distance)


def initializePlugin(obj):
    plugin = om_mpx.MFnPlugin(obj, 'measureNode', '1.0.0', 'sonictk')
    try:
        plugin.registerNode('measureNode', MeasureNode.kPluginNodeId, creator, initialize)
    except:
        raise RuntimeError, 'Initialization of plugin failed!'


def uninitializePlugin(obj):
    plugin = om_mpx.MFnPlugin(obj)
    try:
        plugin.deregisterNode(MeasureNode.kPluginNodeId)
    except:
        raise RuntimeError, 'Unloading the plugin failed!'