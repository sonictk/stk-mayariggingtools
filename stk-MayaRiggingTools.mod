+ MAYAVERSION:2016 PLATFORM:win64 stk-MayaRiggingTools 1.0 C:/Programs/stk-MayaRiggingTools
MAYA_PLUG_IN_PATH+:=plug-ins/bin

+ MAYAVERSION:2016 PLATFORM:mac stk-MayaRiggingTools 1.0 /Users/sonictk/Git/maya/stk-mayariggingtools
MAYA_PLUG_IN_PATH+:=plug-ins/bin

+ MAYAVERSION:2016 PLATFORM:linux stk-MayaRiggingTools 1.0 /home/Git/maya/stk-mayariggingtools
MAYA_PLUG_IN_PATH+:=plug-ins/bin
